//
//	AboutUS.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class AboutUS : NSObject, NSCoding{

	var aboutId : String!
	var aboutText : String!
	var contactAddress : String!
	var contactEmail : String!
	var contactLat : String!
	var contactLong : String!
	var createdBy : String!
	var createdOn : String!
	var phone : String!
	var status : String!
	var updatedBy : String!
	var updatedOn : String!
	var uploadPic : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		aboutId = json["about_id"].stringValue
		aboutText = json["about_text"].stringValue
		contactAddress = json["contact_address"].stringValue
		contactEmail = json["contact_email"].stringValue
		contactLat = json["contact_lat"].stringValue
		contactLong = json["contact_long"].stringValue
		createdBy = json["created_by"].stringValue
		createdOn = json["created_on"].stringValue
		phone = json["phone"].stringValue
		status = json["status"].stringValue
		updatedBy = json["updated_by"].stringValue
		updatedOn = json["updated_on"].stringValue
		uploadPic = json["upload_pic"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if aboutId != nil{
			dictionary["about_id"] = aboutId
		}
		if aboutText != nil{
			dictionary["about_text"] = aboutText
		}
		if contactAddress != nil{
			dictionary["contact_address"] = contactAddress
		}
		if contactEmail != nil{
			dictionary["contact_email"] = contactEmail
		}
		if contactLat != nil{
			dictionary["contact_lat"] = contactLat
		}
		if contactLong != nil{
			dictionary["contact_long"] = contactLong
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if createdOn != nil{
			dictionary["created_on"] = createdOn
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if status != nil{
			dictionary["status"] = status
		}
		if updatedBy != nil{
			dictionary["updated_by"] = updatedBy
		}
		if updatedOn != nil{
			dictionary["updated_on"] = updatedOn
		}
		if uploadPic != nil{
			dictionary["upload_pic"] = uploadPic
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aboutId = aDecoder.decodeObject(forKey: "about_id") as? String
         aboutText = aDecoder.decodeObject(forKey: "about_text") as? String
         contactAddress = aDecoder.decodeObject(forKey: "contact_address") as? String
         contactEmail = aDecoder.decodeObject(forKey: "contact_email") as? String
         contactLat = aDecoder.decodeObject(forKey: "contact_lat") as? String
         contactLong = aDecoder.decodeObject(forKey: "contact_long") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
         updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String
         uploadPic = aDecoder.decodeObject(forKey: "upload_pic") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if aboutId != nil{
			aCoder.encode(aboutId, forKey: "about_id")
		}
		if aboutText != nil{
			aCoder.encode(aboutText, forKey: "about_text")
		}
		if contactAddress != nil{
			aCoder.encode(contactAddress, forKey: "contact_address")
		}
		if contactEmail != nil{
			aCoder.encode(contactEmail, forKey: "contact_email")
		}
		if contactLat != nil{
			aCoder.encode(contactLat, forKey: "contact_lat")
		}
		if contactLong != nil{
			aCoder.encode(contactLong, forKey: "contact_long")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if createdOn != nil{
			aCoder.encode(createdOn, forKey: "created_on")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updated_by")
		}
		if updatedOn != nil{
			aCoder.encode(updatedOn, forKey: "updated_on")
		}
		if uploadPic != nil{
			aCoder.encode(uploadPic, forKey: "upload_pic")
		}

	}

}
