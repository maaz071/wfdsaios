//
//	InvoiceDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class InvoiceDetail : NSObject, NSCoding{

	var amount : String!
	var item : [Item]!
	var name : String!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		amount = json["amount"].stringValue
		item = [Item]()
		let itemArray = json["item"].arrayValue
		for itemJson in itemArray{
			let value = Item(fromJson: itemJson)
			item.append(value)
		}
		name = json["name"].stringValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if amount != nil{
			dictionary["amount"] = amount
		}
		if item != nil{
			var dictionaryElements = [[String:Any]]()
			for itemElement in item {
				dictionaryElements.append(itemElement.toDictionary())
			}
			dictionary["item"] = dictionaryElements
		}
		if name != nil{
			dictionary["name"] = name
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         amount = aDecoder.decodeObject(forKey: "amount") as? String
         item = aDecoder.decodeObject(forKey: "item") as? [Item]
         name = aDecoder.decodeObject(forKey: "name") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if amount != nil{
			aCoder.encode(amount, forKey: "amount")
		}
		if item != nil{
			aCoder.encode(item, forKey: "item")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}
