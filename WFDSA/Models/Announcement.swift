//
//	Announcement.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Announcement : NSObject, NSCoding{

	var announceFor : String!
	var announcementId : String!
	var announcementMessage : String!
	var createdBy : String!
	var createdOn : String!
	var date : String!
	var iconClass : String!
	var status : String!
	var title : String!
	var updatedBy : String!
	var updatedOn : String!
	var uploadImage : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		announceFor = json["announce_for"].stringValue
		announcementId = json["announcement_id"].stringValue
		announcementMessage = json["announcement_message"].stringValue
		createdBy = json["created_by"].stringValue
		createdOn = json["created_on"].stringValue
		date = json["date"].stringValue
		iconClass = json["icon_class"].stringValue
		status = json["status"].stringValue
		title = json["title"].stringValue
		updatedBy = json["updated_by"].stringValue
		updatedOn = json["updated_on"].stringValue
		uploadImage = json["upload_image"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if announceFor != nil{
			dictionary["announce_for"] = announceFor
		}
		if announcementId != nil{
			dictionary["announcement_id"] = announcementId
		}
		if announcementMessage != nil{
			dictionary["announcement_message"] = announcementMessage
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if createdOn != nil{
			dictionary["created_on"] = createdOn
		}
		if date != nil{
			dictionary["date"] = date
		}
		if iconClass != nil{
			dictionary["icon_class"] = iconClass
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
		if updatedBy != nil{
			dictionary["updated_by"] = updatedBy
		}
		if updatedOn != nil{
			dictionary["updated_on"] = updatedOn
		}
		if uploadImage != nil{
			dictionary["upload_image"] = uploadImage
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         announceFor = aDecoder.decodeObject(forKey: "announce_for") as? String
         announcementId = aDecoder.decodeObject(forKey: "announcement_id") as? String
         announcementMessage = aDecoder.decodeObject(forKey: "announcement_message") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
         date = aDecoder.decodeObject(forKey: "date") as? String
         iconClass = aDecoder.decodeObject(forKey: "icon_class") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
         updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String
         uploadImage = aDecoder.decodeObject(forKey: "upload_image") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if announceFor != nil{
			aCoder.encode(announceFor, forKey: "announce_for")
		}
		if announcementId != nil{
			aCoder.encode(announcementId, forKey: "announcement_id")
		}
		if announcementMessage != nil{
			aCoder.encode(announcementMessage, forKey: "announcement_message")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if createdOn != nil{
			aCoder.encode(createdOn, forKey: "created_on")
		}
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if iconClass != nil{
			aCoder.encode(iconClass, forKey: "icon_class")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updated_by")
		}
		if updatedOn != nil{
			aCoder.encode(updatedOn, forKey: "updated_on")
		}
		if uploadImage != nil{
			aCoder.encode(uploadImage, forKey: "upload_image")
		}

	}

}
