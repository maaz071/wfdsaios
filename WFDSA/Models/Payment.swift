//
//	Payment.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Payment : NSObject, NSCoding{

	var createdBy : String!
	var createdOn : String!
	var eventId : String!
	var invoiceId : String!
	var memberId : String!
	var nonMemberId : String!
	var paymentAmount : String!
	var paymentDate : String!
	var paymentId : String!
	var paymentStatus : String!
	var status : String!
	var title : String!
	var updatedBy : String!
	var updatedOn : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		createdBy = json["created_by"].stringValue
		createdOn = json["created_on"].stringValue
		eventId = json["event_id"].stringValue
		invoiceId = json["invoice_id"].stringValue
		memberId = json["member_id"].stringValue
		nonMemberId = json["non_member_id"].stringValue
		paymentAmount = json["payment_amount"].stringValue
		paymentDate = json["payment_date"].stringValue
		paymentId = json["payment_id"].stringValue
		paymentStatus = json["payment_status"].stringValue
		status = json["status"].stringValue
		title = json["title"].stringValue
		updatedBy = json["updated_by"].stringValue
		updatedOn = json["updated_on"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if createdOn != nil{
			dictionary["created_on"] = createdOn
		}
		if eventId != nil{
			dictionary["event_id"] = eventId
		}
		if invoiceId != nil{
			dictionary["invoice_id"] = invoiceId
		}
		if memberId != nil{
			dictionary["member_id"] = memberId
		}
		if nonMemberId != nil{
			dictionary["non_member_id"] = nonMemberId
		}
		if paymentAmount != nil{
			dictionary["payment_amount"] = paymentAmount
		}
		if paymentDate != nil{
			dictionary["payment_date"] = paymentDate
		}
		if paymentId != nil{
			dictionary["payment_id"] = paymentId
		}
		if paymentStatus != nil{
			dictionary["payment_status"] = paymentStatus
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
		if updatedBy != nil{
			dictionary["updated_by"] = updatedBy
		}
		if updatedOn != nil{
			dictionary["updated_on"] = updatedOn
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
         eventId = aDecoder.decodeObject(forKey: "event_id") as? String
         invoiceId = aDecoder.decodeObject(forKey: "invoice_id") as? String
         memberId = aDecoder.decodeObject(forKey: "member_id") as? String
         nonMemberId = aDecoder.decodeObject(forKey: "non_member_id") as? String
         paymentAmount = aDecoder.decodeObject(forKey: "payment_amount") as? String
         paymentDate = aDecoder.decodeObject(forKey: "payment_date") as? String
         paymentId = aDecoder.decodeObject(forKey: "payment_id") as? String
         paymentStatus = aDecoder.decodeObject(forKey: "payment_status") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
         updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if createdOn != nil{
			aCoder.encode(createdOn, forKey: "created_on")
		}
		if eventId != nil{
			aCoder.encode(eventId, forKey: "event_id")
		}
		if invoiceId != nil{
			aCoder.encode(invoiceId, forKey: "invoice_id")
		}
		if memberId != nil{
			aCoder.encode(memberId, forKey: "member_id")
		}
		if nonMemberId != nil{
			aCoder.encode(nonMemberId, forKey: "non_member_id")
		}
		if paymentAmount != nil{
			aCoder.encode(paymentAmount, forKey: "payment_amount")
		}
		if paymentDate != nil{
			aCoder.encode(paymentDate, forKey: "payment_date")
		}
		if paymentId != nil{
			aCoder.encode(paymentId, forKey: "payment_id")
		}
		if paymentStatus != nil{
			aCoder.encode(paymentStatus, forKey: "payment_status")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updated_by")
		}
		if updatedOn != nil{
			aCoder.encode(updatedOn, forKey: "updated_on")
		}

	}

}
