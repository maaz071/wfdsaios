//
//    Poll.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


class Poll : NSObject, NSCoding{
    
    var id : [String]!
    var pollData : PollData!
    var pollAnswer : [String]!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        id = [String]()
        let idArray = json["id"].arrayValue
        for idJson in idArray{
            id.append(idJson.stringValue)
        }
        let pollDataJson = json["0"]
        if !pollDataJson.isEmpty{
            pollData = PollData(fromJson: pollDataJson)
        }
        pollAnswer = [String]()
        let pollAnswerArray = json["poll_answer"].arrayValue
        for pollAnswerJson in pollAnswerArray{
            pollAnswer.append(pollAnswerJson.stringValue)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if pollData != nil{
            dictionary["0"] = pollData.toDictionary()
        }
        if pollAnswer != nil{
            dictionary["poll_answer"] = pollAnswer
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? [String]
        pollData = aDecoder.decodeObject(forKey: "0") as? PollData
        pollAnswer = aDecoder.decodeObject(forKey: "poll_answer") as? [String]
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if pollData != nil{
            aCoder.encode(pollData, forKey: "0")
        }
        if pollAnswer != nil{
            aCoder.encode(pollAnswer, forKey: "poll_answer")
        }
        
    }
    
}
