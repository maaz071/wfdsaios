//
//	ContactUS.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class ContactUS : NSObject, NSCoding{

	var address : String!
	var contactNo : String!
	var contactUsId : String!
	var createdBy : String!
	var createdOn : String!
	var email : String!
	var lat : String!
	var lng : String!
	var status : String!
	var updatedBy : String!
	var updatedOn : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		address = json["address"].stringValue
		contactNo = json["contact_no"].stringValue
		contactUsId = json["contact_us_id"].stringValue
		createdBy = json["created_by"].stringValue
		createdOn = json["created_on"].stringValue
		email = json["email"].stringValue
		lat = json["lat"].stringValue
		lng = json["lng"].stringValue
		status = json["status"].stringValue
		updatedBy = json["updated_by"].stringValue
		updatedOn = json["updated_on"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if address != nil{
			dictionary["address"] = address
		}
		if contactNo != nil{
			dictionary["contact_no"] = contactNo
		}
		if contactUsId != nil{
			dictionary["contact_us_id"] = contactUsId
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if createdOn != nil{
			dictionary["created_on"] = createdOn
		}
		if email != nil{
			dictionary["email"] = email
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if lng != nil{
			dictionary["lng"] = lng
		}
		if status != nil{
			dictionary["status"] = status
		}
		if updatedBy != nil{
			dictionary["updated_by"] = updatedBy
		}
		if updatedOn != nil{
			dictionary["updated_on"] = updatedOn
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         contactNo = aDecoder.decodeObject(forKey: "contact_no") as? String
         contactUsId = aDecoder.decodeObject(forKey: "contact_us_id") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         lat = aDecoder.decodeObject(forKey: "lat") as? String
         lng = aDecoder.decodeObject(forKey: "lng") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
         updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if contactNo != nil{
			aCoder.encode(contactNo, forKey: "contact_no")
		}
		if contactUsId != nil{
			aCoder.encode(contactUsId, forKey: "contact_us_id")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if createdOn != nil{
			aCoder.encode(createdOn, forKey: "created_on")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lng != nil{
			aCoder.encode(lng, forKey: "lng")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updated_by")
		}
		if updatedOn != nil{
			aCoder.encode(updatedOn, forKey: "updated_on")
		}

	}

}
