//
//    RoleMember.swift
//
//    Create by Ali Abdul Jabbar on 31/7/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class RoleMember : NSObject, NSCoding{
    
    var address : String!
    var cell : String!
    var company : String!
    var designation : String!
    var email : String!
    var fax : String!
    var flagPic : String!
    var memberId : String!
    var memberName : String!
    var memberRoleId : String!
    var memberSelectRoleId : String!
    var name : String!
    var sortOrder : String!
    var telephone : String!
    var title : String!
    var uploadImage : String!
    var wfdsaTitle : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        cell = json["cell"].stringValue
        company = json["company"].stringValue
        designation = json["designation"].stringValue
        email = json["email"].stringValue
        fax = json["fax"].stringValue
        flagPic = json["flag_pic"].stringValue
        memberId = json["member_id"].stringValue
        memberName = json["member_name"].stringValue
        memberRoleId = json["member_role_id"].stringValue
        memberSelectRoleId = json["member_select_role_id"].stringValue
        name = json["name"].stringValue
        sortOrder = json["sort_order"].stringValue
        telephone = json["telephone"].stringValue
        title = json["title"].stringValue
        uploadImage = json["upload_image"].stringValue
        wfdsaTitle = json["wfdsa_title"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if cell != nil{
            dictionary["cell"] = cell
        }
        if company != nil{
            dictionary["company"] = company
        }
        if designation != nil{
            dictionary["designation"] = designation
        }
        if email != nil{
            dictionary["email"] = email
        }
        if fax != nil{
            dictionary["fax"] = fax
        }
        if flagPic != nil{
            dictionary["flag_pic"] = flagPic
        }
        if memberId != nil{
            dictionary["member_id"] = memberId
        }
        if memberName != nil{
            dictionary["member_name"] = memberName
        }
        if memberRoleId != nil{
            dictionary["member_role_id"] = memberRoleId
        }
        if memberSelectRoleId != nil{
            dictionary["member_select_role_id"] = memberSelectRoleId
        }
        if name != nil{
            dictionary["name"] = name
        }
        if sortOrder != nil{
            dictionary["sort_order"] = sortOrder
        }
        if telephone != nil{
            dictionary["telephone"] = telephone
        }
        if title != nil{
            dictionary["title"] = title
        }
        if uploadImage != nil{
            dictionary["upload_image"] = uploadImage
        }
        if wfdsaTitle != nil{
            dictionary["wfdsa_title"] = wfdsaTitle
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        cell = aDecoder.decodeObject(forKey: "cell") as? String
        company = aDecoder.decodeObject(forKey: "company") as? String
        designation = aDecoder.decodeObject(forKey: "designation") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        fax = aDecoder.decodeObject(forKey: "fax") as? String
        flagPic = aDecoder.decodeObject(forKey: "flag_pic") as? String
        memberId = aDecoder.decodeObject(forKey: "member_id") as? String
        memberName = aDecoder.decodeObject(forKey: "member_name") as? String
        memberRoleId = aDecoder.decodeObject(forKey: "member_role_id") as? String
        memberSelectRoleId = aDecoder.decodeObject(forKey: "member_select_role_id") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        sortOrder = aDecoder.decodeObject(forKey: "sort_order") as? String
        telephone = aDecoder.decodeObject(forKey: "telephone") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        uploadImage = aDecoder.decodeObject(forKey: "upload_image") as? String
        wfdsaTitle = aDecoder.decodeObject(forKey: "wfdsa_title") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if cell != nil{
            aCoder.encode(cell, forKey: "cell")
        }
        if company != nil{
            aCoder.encode(company, forKey: "company")
        }
        if designation != nil{
            aCoder.encode(designation, forKey: "designation")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if fax != nil{
            aCoder.encode(fax, forKey: "fax")
        }
        if flagPic != nil{
            aCoder.encode(flagPic, forKey: "flag_pic")
        }
        if memberId != nil{
            aCoder.encode(memberId, forKey: "member_id")
        }
        if memberName != nil{
            aCoder.encode(memberName, forKey: "member_name")
        }
        if memberRoleId != nil{
            aCoder.encode(memberRoleId, forKey: "member_role_id")
        }
        if memberSelectRoleId != nil{
            aCoder.encode(memberSelectRoleId, forKey: "member_select_role_id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if sortOrder != nil{
            aCoder.encode(sortOrder, forKey: "sort_order")
        }
        if telephone != nil{
            aCoder.encode(telephone, forKey: "telephone")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if uploadImage != nil{
            aCoder.encode(uploadImage, forKey: "upload_image")
        }
        if wfdsaTitle != nil{
            aCoder.encode(wfdsaTitle, forKey: "wfdsa_title")
        }
        
    }
    
}
