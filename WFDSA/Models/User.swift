//
//  User.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/28/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import SwiftyJSON

class User: Object, NSCoding {
    
    @objc dynamic var address: String!
    @objc dynamic var contactNo: String!
    @objc dynamic var token: String!
    @objc dynamic var appUser: String!
    @objc dynamic var biography: String!
    @objc dynamic var cell: String!
    @objc dynamic var company: String!
    @objc dynamic var country: String!
    @objc dynamic var createdBy: String!
    @objc dynamic var createdOn: String!
    @objc dynamic var designation: String!
    @objc dynamic var deviceToken: String!
    @objc dynamic var email: String!
    @objc dynamic var fax: String!
    @objc dynamic var firstName: String!
    @objc dynamic var lastName: String!
    @objc dynamic var id: String!
    @objc dynamic var password: String!
    @objc dynamic var role: String!
    @objc dynamic var signinType: String!
    @objc dynamic var status: String!
    @objc dynamic var telephone: String!
    @objc dynamic var title: String!
    @objc dynamic var updatedBy: String!
    @objc dynamic var updatedOn: String!
    @objc dynamic var uploadImage: String!
    @objc dynamic var venue: String!
    @objc dynamic var website: String!
    @objc dynamic var wfdsaTitle: String!
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value : value,schema:schema)
        //fatalError("init(value:schema:) has not been implemented")
    }
    
    required init() {
        super.init()
        //fatalError("init() has not been implemented")
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm:realm , schema:schema)
        //fatalError("init(realm:schema:) has not been implemented")
    }
    

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        super.init()
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        contactNo = json["contact_no"].stringValue
        token = json["token"].stringValue
        appUser = json["app_user"].stringValue
        biography = json["biography"].stringValue
        cell = json["cell"].stringValue
        company = json["company"].stringValue
        country = json["country"].stringValue
        createdBy = json["created_by"].stringValue
        createdOn = json["created_on"].stringValue
        designation = json["designation"].stringValue
        deviceToken = json["device_token"].stringValue
        email = json["email"].stringValue
        fax = json["fax"].stringValue
        firstName = json["first_name"].stringValue
        lastName = json["last_name"].stringValue
        id = json["id"].stringValue
        password = json["password"].stringValue
        role = json["role"].stringValue
        signinType = json["signin_type"].stringValue
        status = json["status"].stringValue
        telephone = json["telephone"].stringValue
        title = json["title"].stringValue
        updatedBy = json["updated_by"].stringValue
        updatedOn = json["updated_on"].stringValue
        uploadImage = json["upload_image"].stringValue
        venue = json["venue"].stringValue
        website = json["website"].stringValue
        wfdsaTitle = json["wfdsa_title"].stringValue
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    class func fromDictionary(dictionary: [String:Any]) -> User    {
        let this = User()
        if let addressValue = dictionary["address"] as? String{
            this.address = addressValue
        }
        if let contactNoValue = dictionary["contact_no"] as? String{
            this.contactNo = contactNoValue
        }
        if let tokenValue = dictionary["token"] as? String{
            this.token = tokenValue
        }
        if let appUserValue = dictionary["app_user"] as? String{
            this.appUser = appUserValue
        }
        if let biographyValue = dictionary["biography"] as? String{
            this.biography = biographyValue
        }
        if let cellValue = dictionary["cell"] as? String{
            this.cell = cellValue
        }
        if let companyValue = dictionary["company"] as? String{
            this.company = companyValue
        }
        if let countryValue = dictionary["country"] as? String{
            this.country = countryValue
        }
        if let createdByValue = dictionary["created_by"] as? String{
            this.createdBy = createdByValue
        }
        if let createdOnValue = dictionary["created_on"] as? String{
            this.createdOn = createdOnValue
        }
        if let designationValue = dictionary["designation"] as? String{
            this.designation = designationValue
        }
        if let deviceTokenValue = dictionary["device_token"] as? String{
            this.deviceToken = deviceTokenValue
        }
        if let emailValue = dictionary["email"] as? String{
            this.email = emailValue
        }
        if let faxValue = dictionary["fax"] as? String{
            this.fax = faxValue
        }
        if let firstNameValue = dictionary["first_name"] as? String{
            this.firstName = firstNameValue
        }
        if let lastNameValue = dictionary["last_name"] as? String{
            this.lastName = lastNameValue
        }
        if let idValue = dictionary["id"] as? String{
            this.id = idValue
        }
        if let passwordValue = dictionary["password"] as? String{
            this.password = passwordValue
        }
        if let roleValue = dictionary["role"] as? String{
            this.role = roleValue
        }
        if let signinTypeValue = dictionary["signin_type"] as? String{
            this.signinType = signinTypeValue
        }
        if let statusValue = dictionary["status"] as? String{
            this.status = statusValue
        }
        if let telephoneValue = dictionary["telephone"] as? String{
            this.telephone = telephoneValue
        }
        if let titleValue = dictionary["title"] as? String{
            this.title = titleValue
        }
        if let updatedByValue = dictionary["updated_by"] as? String{
            this.updatedBy = updatedByValue
        }
        if let updatedOnValue = dictionary["updated_on"] as? String{
            this.updatedOn = updatedOnValue
        }
        if let uploadImageValue = dictionary["upload_image"] as? String{
            this.uploadImage = uploadImageValue
        }
        if let venueValue = dictionary["venue"] as? String{
            this.venue = venueValue
        }
        if let websiteValue = dictionary["website"] as? String{
            this.website = websiteValue
        }
        if let wfdsaTitleValue = dictionary["wfdsa_title"] as? String{
            this.wfdsaTitle = wfdsaTitleValue
        }
        return this
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if contactNo != nil{
            dictionary["contact_no"] = contactNo
        }
        if token != nil{
            dictionary["token"] = token
        }
        if appUser != nil{
            dictionary["app_user"] = appUser
        }
        if biography != nil{
            dictionary["biography"] = biography
        }
        if cell != nil{
            dictionary["cell"] = cell
        }
        if company != nil{
            dictionary["company"] = company
        }
        if country != nil{
            dictionary["country"] = country
        }
        if createdBy != nil{
            dictionary["created_by"] = createdBy
        }
        if createdOn != nil{
            dictionary["created_on"] = createdOn
        }
        if designation != nil{
            dictionary["designation"] = designation
        }
        if deviceToken != nil{
            dictionary["device_token"] = deviceToken
        }
        if email != nil{
            dictionary["email"] = email
        }
        if fax != nil{
            dictionary["fax"] = fax
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if id != nil{
            dictionary["id"] = id
        }
        if password != nil{
            dictionary["password"] = password
        }
        if role != nil{
            dictionary["role"] = role
        }
        if signinType != nil{
            dictionary["signin_type"] = signinType
        }
        if status != nil{
            dictionary["status"] = status
        }
        if telephone != nil{
            dictionary["telephone"] = telephone
        }
        if title != nil{
            dictionary["title"] = title
        }
        if updatedBy != nil{
            dictionary["updated_by"] = updatedBy
        }
        if updatedOn != nil{
            dictionary["updated_on"] = updatedOn
        }
        if uploadImage != nil{
            dictionary["upload_image"] = uploadImage
        }
        if venue != nil{
            dictionary["venue"] = venue
        }
        if website != nil{
            dictionary["website"] = website
        }
        if wfdsaTitle != nil{
            dictionary["wfdsa_title"] = wfdsaTitle
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        super.init()
        address = aDecoder.decodeObject(forKey: "address") as? String
        contactNo = aDecoder.decodeObject(forKey: "contact_no") as? String
        token = aDecoder.decodeObject(forKey: "token") as? String
        appUser = aDecoder.decodeObject(forKey: "app_user") as? String
        biography = aDecoder.decodeObject(forKey: "biography") as? String
        cell = aDecoder.decodeObject(forKey: "cell") as? String
        company = aDecoder.decodeObject(forKey: "company") as? String
        country = aDecoder.decodeObject(forKey: "country") as? String
        createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
        createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
        designation = aDecoder.decodeObject(forKey: "designation") as? String
        deviceToken = aDecoder.decodeObject(forKey: "device_token") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        fax = aDecoder.decodeObject(forKey: "fax") as? String
        firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        password = aDecoder.decodeObject(forKey: "password") as? String
        role = aDecoder.decodeObject(forKey: "role") as? String
        signinType = aDecoder.decodeObject(forKey: "signin_type") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        telephone = aDecoder.decodeObject(forKey: "telephone") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
        updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String
        uploadImage = aDecoder.decodeObject(forKey: "upload_image") as? String
        venue = aDecoder.decodeObject(forKey: "venue") as? String
        website = aDecoder.decodeObject(forKey: "website") as? String
        wfdsaTitle = aDecoder.decodeObject(forKey: "wfdsa_title") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if contactNo != nil{
            aCoder.encode(contactNo, forKey: "contact_no")
        }
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
        if appUser != nil{
            aCoder.encode(appUser, forKey: "app_user")
        }
        if biography != nil{
            aCoder.encode(biography, forKey: "biography")
        }
        if cell != nil{
            aCoder.encode(cell, forKey: "cell")
        }
        if company != nil{
            aCoder.encode(company, forKey: "company")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if createdBy != nil{
            aCoder.encode(createdBy, forKey: "created_by")
        }
        if createdOn != nil{
            aCoder.encode(createdOn, forKey: "created_on")
        }
        if designation != nil{
            aCoder.encode(designation, forKey: "designation")
        }
        if deviceToken != nil{
            aCoder.encode(deviceToken, forKey: "device_token")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if fax != nil{
            aCoder.encode(fax, forKey: "fax")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if password != nil{
            aCoder.encode(password, forKey: "password")
        }
        if role != nil{
            aCoder.encode(role, forKey: "role")
        }
        if signinType != nil{
            aCoder.encode(signinType, forKey: "signin_type")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if telephone != nil{
            aCoder.encode(telephone, forKey: "telephone")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if updatedBy != nil{
            aCoder.encode(updatedBy, forKey: "updated_by")
        }
        if updatedOn != nil{
            aCoder.encode(updatedOn, forKey: "updated_on")
        }
        if uploadImage != nil{
            aCoder.encode(uploadImage, forKey: "upload_image")
        }
        if venue != nil{
            aCoder.encode(venue, forKey: "venue")
        }
        if website != nil{
            aCoder.encode(website, forKey: "website")
        }
        if wfdsaTitle != nil{
            aCoder.encode(wfdsaTitle, forKey: "wfdsa_title")
        }
        
    }
}
