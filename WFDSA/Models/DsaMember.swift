//
//    DsaMember.swift
//
//    Create by Umer Abdul Jababr on 29/7/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


class DsaMember : NSObject, NSCoding{
    
    var address : String!
    var companyLogo : String!
    var companyName : String!
    var country : String!
    var countryId : String!
    var createdBy : String!
    var createdOn : String!
    var dsaId : String!
    var email : String!
    var fax : String!
    var flagPic : String!
    var memberName : String!
    var memberPhoto : String!
    var phone : String!
    var region : String!
    var regionId : String!
    var status : String!
    var updatedBy : String!
    var updatedOn : String!
    var website : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        companyLogo = json["company_logo"].stringValue
        companyName = json["company_name"].stringValue
        country = json["country"].stringValue
        countryId = json["country_id"].stringValue
        createdBy = json["created_by"].stringValue
        createdOn = json["created_on"].stringValue
        dsaId = json["dsa_id"].stringValue
        email = json["email"].stringValue
        fax = json["fax"].stringValue
        flagPic = json["flag_pic"].stringValue
        memberName = json["member_name"].stringValue
        memberPhoto = json["member_photo"].stringValue
        phone = json["phone"].stringValue
        region = json["region"].stringValue
        regionId = json["region_id"].stringValue
        status = json["status"].stringValue
        updatedBy = json["updated_by"].stringValue
        updatedOn = json["updated_on"].stringValue
        website = json["website"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if companyLogo != nil{
            dictionary["company_logo"] = companyLogo
        }
        if companyName != nil{
            dictionary["company_name"] = companyName
        }
        if country != nil{
            dictionary["country"] = country
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if createdBy != nil{
            dictionary["created_by"] = createdBy
        }
        if createdOn != nil{
            dictionary["created_on"] = createdOn
        }
        if dsaId != nil{
            dictionary["dsa_id"] = dsaId
        }
        if email != nil{
            dictionary["email"] = email
        }
        if fax != nil{
            dictionary["fax"] = fax
        }
        if flagPic != nil{
            dictionary["flag_pic"] = flagPic
        }
        if memberName != nil{
            dictionary["member_name"] = memberName
        }
        if memberPhoto != nil{
            dictionary["member_photo"] = memberPhoto
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if region != nil{
            dictionary["region"] = region
        }
        if regionId != nil{
            dictionary["region_id"] = regionId
        }
        if status != nil{
            dictionary["status"] = status
        }
        if updatedBy != nil{
            dictionary["updated_by"] = updatedBy
        }
        if updatedOn != nil{
            dictionary["updated_on"] = updatedOn
        }
        if website != nil{
            dictionary["website"] = website
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        companyLogo = aDecoder.decodeObject(forKey: "company_logo") as? String
        companyName = aDecoder.decodeObject(forKey: "company_name") as? String
        country = aDecoder.decodeObject(forKey: "country") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
        createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
        dsaId = aDecoder.decodeObject(forKey: "dsa_id") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        fax = aDecoder.decodeObject(forKey: "fax") as? String
        flagPic = aDecoder.decodeObject(forKey: "flag_pic") as? String
        memberName = aDecoder.decodeObject(forKey: "member_name") as? String
        memberPhoto = aDecoder.decodeObject(forKey: "member_photo") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        region = aDecoder.decodeObject(forKey: "region") as? String
        regionId = aDecoder.decodeObject(forKey: "region_id") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
        updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String
        website = aDecoder.decodeObject(forKey: "website") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if companyLogo != nil{
            aCoder.encode(companyLogo, forKey: "company_logo")
        }
        if companyName != nil{
            aCoder.encode(companyName, forKey: "company_name")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if createdBy != nil{
            aCoder.encode(createdBy, forKey: "created_by")
        }
        if createdOn != nil{
            aCoder.encode(createdOn, forKey: "created_on")
        }
        if dsaId != nil{
            aCoder.encode(dsaId, forKey: "dsa_id")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if fax != nil{
            aCoder.encode(fax, forKey: "fax")
        }
        if flagPic != nil{
            aCoder.encode(flagPic, forKey: "flag_pic")
        }
        if memberName != nil{
            aCoder.encode(memberName, forKey: "member_name")
        }
        if memberPhoto != nil{
            aCoder.encode(memberPhoto, forKey: "member_photo")
        }
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        if region != nil{
            aCoder.encode(region, forKey: "region")
        }
        if regionId != nil{
            aCoder.encode(regionId, forKey: "region_id")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if updatedBy != nil{
            aCoder.encode(updatedBy, forKey: "updated_by")
        }
        if updatedOn != nil{
            aCoder.encode(updatedOn, forKey: "updated_on")
        }
        if website != nil{
            aCoder.encode(website, forKey: "website")
        }
        
    }
    
}
