//
//	Comment.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Comment : NSObject, NSCoding{

	var date : String!
	var imageURL : String!
	var message : String!
	var name : String!
	var post : String!
	var time : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		date = json["date"].stringValue
		imageURL = json["imageURL"].stringValue
		message = json["message"].stringValue
		name = json["name"].stringValue
		post = json["post"].stringValue
		time = json["time"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if date != nil{
			dictionary["date"] = date
		}
		if imageURL != nil{
			dictionary["imageURL"] = imageURL
		}
		if message != nil{
			dictionary["message"] = message
		}
		if name != nil{
			dictionary["name"] = name
		}
		if post != nil{
			dictionary["post"] = post
		}
		if time != nil{
			dictionary["time"] = time
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         date = aDecoder.decodeObject(forKey: "date") as? String
         imageURL = aDecoder.decodeObject(forKey: "imageURL") as? String
         message = aDecoder.decodeObject(forKey: "message") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         post = aDecoder.decodeObject(forKey: "post") as? String
         time = aDecoder.decodeObject(forKey: "time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if imageURL != nil{
			aCoder.encode(imageURL, forKey: "imageURL")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if post != nil{
			aCoder.encode(post, forKey: "post")
		}
		if time != nil{
			aCoder.encode(time, forKey: "time")
		}

	}

}
