//
//	EventDetail.swift
//
//	Create by Umer Abdul Jababr on 29/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class EventDetail : NSObject, NSCoding{

    var agenda : String!
    var cityId : String!
    var countryId : String!
    var createdBy : String!
    var createdOn : String!
    var endDate : String!
    var endTime : String!
    var eventId : String!
    var eventUserType : String!
    var fee : String!
    var isAnswered : String!
    var isCheckedIn : Int!
    var isLike : Int!
    var isRegistered : Int!
    var latLng : String!
    var latitude : String!
    var location : String!
    var longitude : String!
    var noSeats : String!
    var permission : String!
    var photoUploaded : Int!
    var place : String!
    var speaker : String!
    var startDate : String!
    var startTime : String!
    var status : String!
    var title : String!
    var totalLikes : String!
    var totalRegistered : String!
    var updatedBy : String!
    var updatedOn : String!
    var uploadImage : String!
    var venue : String!
    var venueAddress : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        agenda = json["agenda"].stringValue
        cityId = json["city_id"].stringValue
        countryId = json["country_id"].stringValue
        createdBy = json["created_by"].stringValue
        createdOn = json["created_on"].stringValue
        endDate = json["end_date"].stringValue
        endTime = json["end_time"].stringValue
        eventId = json["event_id"].stringValue
        eventUserType = json["event_user_type"].stringValue
        fee = json["fee"].stringValue
        isAnswered = json["is_answered"].stringValue
        isCheckedIn = json["is_checked_in"].intValue
        isLike = json["is_like"].intValue
        isRegistered = json["is_registered"].intValue
        latLng = json["latLng"].stringValue
        latitude = json["latitude"].stringValue
        location = json["location"].stringValue
        longitude = json["longitude"].stringValue
        noSeats = json["no_seats"].stringValue
        permission = json["permission"].stringValue
        photoUploaded = json["photo_uploaded"].intValue
        place = json["place"].stringValue
        speaker = json["speaker"].stringValue
        startDate = json["start_date"].stringValue
        startTime = json["start_time"].stringValue
        status = json["status"].stringValue
        title = json["title"].stringValue
        totalLikes = json["total_likes"].stringValue
        totalRegistered = json["total_registered"].stringValue
        updatedBy = json["updated_by"].stringValue
        updatedOn = json["updated_on"].stringValue
        uploadImage = json["upload_image"].stringValue
        venue = json["venue"].stringValue
        venueAddress = json["venue_address"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if agenda != nil{
            dictionary["agenda"] = agenda
        }
        if cityId != nil{
            dictionary["city_id"] = cityId
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if createdBy != nil{
            dictionary["created_by"] = createdBy
        }
        if createdOn != nil{
            dictionary["created_on"] = createdOn
        }
        if endDate != nil{
            dictionary["end_date"] = endDate
        }
        if endTime != nil{
            dictionary["end_time"] = endTime
        }
        if eventId != nil{
            dictionary["event_id"] = eventId
        }
        if eventUserType != nil{
            dictionary["event_user_type"] = eventUserType
        }
        if fee != nil{
            dictionary["fee"] = fee
        }
        if isAnswered != nil{
            dictionary["is_answered"] = isAnswered
        }
        if isCheckedIn != nil{
            dictionary["is_checked_in"] = isCheckedIn
        }
        if isLike != nil{
            dictionary["is_like"] = isLike
        }
        if isRegistered != nil{
            dictionary["is_registered"] = isRegistered
        }
        if latLng != nil{
            dictionary["latLng"] = latLng
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if location != nil{
            dictionary["location"] = location
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if noSeats != nil{
            dictionary["no_seats"] = noSeats
        }
        if permission != nil{
            dictionary["permission"] = permission
        }
        if photoUploaded != nil{
            dictionary["photo_uploaded"] = photoUploaded
        }
        if place != nil{
            dictionary["place"] = place
        }
        if speaker != nil{
            dictionary["speaker"] = speaker
        }
        if startDate != nil{
            dictionary["start_date"] = startDate
        }
        if startTime != nil{
            dictionary["start_time"] = startTime
        }
        if status != nil{
            dictionary["status"] = status
        }
        if title != nil{
            dictionary["title"] = title
        }
        if totalLikes != nil{
            dictionary["total_likes"] = totalLikes
        }
        if totalRegistered != nil{
            dictionary["total_registered"] = totalRegistered
        }
        if updatedBy != nil{
            dictionary["updated_by"] = updatedBy
        }
        if updatedOn != nil{
            dictionary["updated_on"] = updatedOn
        }
        if uploadImage != nil{
            dictionary["upload_image"] = uploadImage
        }
        if venue != nil{
            dictionary["venue"] = venue
        }
        if venueAddress != nil{
            dictionary["venue_address"] = venueAddress
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        agenda = aDecoder.decodeObject(forKey: "agenda") as? String
        cityId = aDecoder.decodeObject(forKey: "city_id") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
        createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
        endDate = aDecoder.decodeObject(forKey: "end_date") as? String
        endTime = aDecoder.decodeObject(forKey: "end_time") as? String
        eventId = aDecoder.decodeObject(forKey: "event_id") as? String
        eventUserType = aDecoder.decodeObject(forKey: "event_user_type") as? String
        fee = aDecoder.decodeObject(forKey: "fee") as? String
        isAnswered = aDecoder.decodeObject(forKey: "is_answered") as? String
        isCheckedIn = aDecoder.decodeObject(forKey: "is_checked_in") as? Int
        isLike = aDecoder.decodeObject(forKey: "is_like") as? Int
        isRegistered = aDecoder.decodeObject(forKey: "is_registered") as? Int
        latLng = aDecoder.decodeObject(forKey: "latLng") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        location = aDecoder.decodeObject(forKey: "location") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        noSeats = aDecoder.decodeObject(forKey: "no_seats") as? String
        permission = aDecoder.decodeObject(forKey: "permission") as? String
        photoUploaded = aDecoder.decodeObject(forKey: "photo_uploaded") as? Int
        place = aDecoder.decodeObject(forKey: "place") as? String
        speaker = aDecoder.decodeObject(forKey: "speaker") as? String
        startDate = aDecoder.decodeObject(forKey: "start_date") as? String
        startTime = aDecoder.decodeObject(forKey: "start_time") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        totalLikes = aDecoder.decodeObject(forKey: "total_likes") as? String
        totalRegistered = aDecoder.decodeObject(forKey: "total_registered") as? String
        updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
        updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String
        uploadImage = aDecoder.decodeObject(forKey: "upload_image") as? String
        venue = aDecoder.decodeObject(forKey: "venue") as? String
        venueAddress = aDecoder.decodeObject(forKey: "venue_address") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if agenda != nil{
            aCoder.encode(agenda, forKey: "agenda")
        }
        if cityId != nil{
            aCoder.encode(cityId, forKey: "city_id")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if createdBy != nil{
            aCoder.encode(createdBy, forKey: "created_by")
        }
        if createdOn != nil{
            aCoder.encode(createdOn, forKey: "created_on")
        }
        if endDate != nil{
            aCoder.encode(endDate, forKey: "end_date")
        }
        if endTime != nil{
            aCoder.encode(endTime, forKey: "end_time")
        }
        if eventId != nil{
            aCoder.encode(eventId, forKey: "event_id")
        }
        if eventUserType != nil{
            aCoder.encode(eventUserType, forKey: "event_user_type")
        }
        if fee != nil{
            aCoder.encode(fee, forKey: "fee")
        }
        if isAnswered != nil{
            aCoder.encode(isAnswered, forKey: "is_answered")
        }
        if isCheckedIn != nil{
            aCoder.encode(isCheckedIn, forKey: "is_checked_in")
        }
        if isLike != nil{
            aCoder.encode(isLike, forKey: "is_like")
        }
        if isRegistered != nil{
            aCoder.encode(isRegistered, forKey: "is_registered")
        }
        if latLng != nil{
            aCoder.encode(latLng, forKey: "latLng")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if location != nil{
            aCoder.encode(location, forKey: "location")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if noSeats != nil{
            aCoder.encode(noSeats, forKey: "no_seats")
        }
        if permission != nil{
            aCoder.encode(permission, forKey: "permission")
        }
        if photoUploaded != nil{
            aCoder.encode(photoUploaded, forKey: "photo_uploaded")
        }
        if place != nil{
            aCoder.encode(place, forKey: "place")
        }
        if speaker != nil{
            aCoder.encode(speaker, forKey: "speaker")
        }
        if startDate != nil{
            aCoder.encode(startDate, forKey: "start_date")
        }
        if startTime != nil{
            aCoder.encode(startTime, forKey: "start_time")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if totalLikes != nil{
            aCoder.encode(totalLikes, forKey: "total_likes")
        }
        if totalRegistered != nil{
            aCoder.encode(totalRegistered, forKey: "total_registered")
        }
        if updatedBy != nil{
            aCoder.encode(updatedBy, forKey: "updated_by")
        }
        if updatedOn != nil{
            aCoder.encode(updatedOn, forKey: "updated_on")
        }
        if uploadImage != nil{
            aCoder.encode(uploadImage, forKey: "upload_image")
        }
        if venue != nil{
            aCoder.encode(venue, forKey: "venue")
        }
        if venueAddress != nil{
            aCoder.encode(venueAddress, forKey: "venue_address")
        }
        
    }
    
}
