//
//	AlbumObject.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class AlbumObject : NSObject, NSCoding{

	var albumId : String!
	var createdAt : String!
	var galleryId : String!
	var status : String!
	var type : String!
	var uploadLink : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		albumId = json["album_id"].stringValue
		createdAt = json["created_at"].stringValue
		galleryId = json["gallery_id"].stringValue
		status = json["status"].stringValue
		type = json["type"].stringValue
		uploadLink = json["upload_link"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if albumId != nil{
			dictionary["album_id"] = albumId
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if galleryId != nil{
			dictionary["gallery_id"] = galleryId
		}
		if status != nil{
			dictionary["status"] = status
		}
		if type != nil{
			dictionary["type"] = type
		}
		if uploadLink != nil{
			dictionary["upload_link"] = uploadLink
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         albumId = aDecoder.decodeObject(forKey: "album_id") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         galleryId = aDecoder.decodeObject(forKey: "gallery_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String
         uploadLink = aDecoder.decodeObject(forKey: "upload_link") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if albumId != nil{
			aCoder.encode(albumId, forKey: "album_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if galleryId != nil{
			aCoder.encode(galleryId, forKey: "gallery_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if uploadLink != nil{
			aCoder.encode(uploadLink, forKey: "upload_link")
		}

	}

}
