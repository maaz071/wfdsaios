//
//	UserGuide.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserGuide : NSObject, NSCoding{

	var createdBy : String!
	var createdOn : String!
	var descriptionField : String!
	var guideId : String!
	var status : String!
	var title : String!
	var updatedBy : String!
	var updatedOn : String!
	var uploadVideo : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		createdBy = json["created_by"].stringValue
		createdOn = json["created_on"].stringValue
		descriptionField = json["description"].stringValue
		guideId = json["guide_id"].stringValue
		status = json["status"].stringValue
		title = json["title"].stringValue
		updatedBy = json["updated_by"].stringValue
		updatedOn = json["updated_on"].stringValue
		uploadVideo = json["upload_video"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if createdOn != nil{
			dictionary["created_on"] = createdOn
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if guideId != nil{
			dictionary["guide_id"] = guideId
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
		if updatedBy != nil{
			dictionary["updated_by"] = updatedBy
		}
		if updatedOn != nil{
			dictionary["updated_on"] = updatedOn
		}
		if uploadVideo != nil{
			dictionary["upload_video"] = uploadVideo
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         guideId = aDecoder.decodeObject(forKey: "guide_id") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
         updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String
         uploadVideo = aDecoder.decodeObject(forKey: "upload_video") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if createdOn != nil{
			aCoder.encode(createdOn, forKey: "created_on")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if guideId != nil{
			aCoder.encode(guideId, forKey: "guide_id")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updated_by")
		}
		if updatedOn != nil{
			aCoder.encode(updatedOn, forKey: "updated_on")
		}
		if uploadVideo != nil{
			aCoder.encode(uploadVideo, forKey: "upload_video")
		}

	}

}
