//
//    ResourcesCategory.swift
//
//    Create by Umer Abdul Jababr on 29/7/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


class ResourcesCategory : NSObject, NSCoding{
    var categoriesId : String!
    var createdBy : String!
    var createdOn : String!
    var name : String!
    var parentId : String!
    var resources : [Resource]!
    var status : String!
    var updatedBy : String!
    var updatedOn : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        categoriesId = json["categories_id"].stringValue
        createdBy = json["created_by"].stringValue
        createdOn = json["created_on"].stringValue
        name = json["name"].stringValue
        parentId = json["parent_id"].stringValue
        resources = [Resource]()
        let resourcesArray = json["resources"].arrayValue
        for resourcesJson in resourcesArray{
            let value = Resource(fromJson: resourcesJson)
            resources.append(value)
        }
        status = json["status"].stringValue
        updatedBy = json["updated_by"].stringValue
        updatedOn = json["updated_on"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if categoriesId != nil{
            dictionary["categories_id"] = categoriesId
        }
        if createdBy != nil{
            dictionary["created_by"] = createdBy
        }
        if createdOn != nil{
            dictionary["created_on"] = createdOn
        }
        if name != nil{
            dictionary["name"] = name
        }
        if parentId != nil{
            dictionary["parent_id"] = parentId
        }
        if resources != nil{
            var dictionaryElements = [[String:Any]]()
            for resourcesElement in resources {
                dictionaryElements.append(resourcesElement.toDictionary())
            }
            dictionary["resources"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        if updatedBy != nil{
            dictionary["updated_by"] = updatedBy
        }
        if updatedOn != nil{
            dictionary["updated_on"] = updatedOn
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        categoriesId = aDecoder.decodeObject(forKey: "categories_id") as? String
        createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
        createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        parentId = aDecoder.decodeObject(forKey: "parent_id") as? String
        resources = aDecoder.decodeObject(forKey: "resources") as? [Resource]
        status = aDecoder.decodeObject(forKey: "status") as? String
        updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
        updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if categoriesId != nil{
            aCoder.encode(categoriesId, forKey: "categories_id")
        }
        if createdBy != nil{
            aCoder.encode(createdBy, forKey: "created_by")
        }
        if createdOn != nil{
            aCoder.encode(createdOn, forKey: "created_on")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if parentId != nil{
            aCoder.encode(parentId, forKey: "parent_id")
        }
        if resources != nil{
            aCoder.encode(resources, forKey: "resources")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if updatedBy != nil{
            aCoder.encode(updatedBy, forKey: "updated_by")
        }
        if updatedOn != nil{
            aCoder.encode(updatedOn, forKey: "updated_on")
        }
        
    }
    
}
