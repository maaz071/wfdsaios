//
//	Album.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Album : NSObject, NSCoding{

	var albumCover : String!
	var albumId : String!
	var albumPermission : String!
	var albumTitle : String!
	var albumType : String!
	var createdAt : String!
	var status : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		albumCover = json["album_cover"].stringValue
		albumId = json["album_id"].stringValue
		albumPermission = json["album_permission"].stringValue
		albumTitle = json["album_title"].stringValue
		albumType = json["album_type"].stringValue
		createdAt = json["created_at"].stringValue
		status = json["status"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if albumCover != nil{
			dictionary["album_cover"] = albumCover
		}
		if albumId != nil{
			dictionary["album_id"] = albumId
		}
		if albumPermission != nil{
			dictionary["album_permission"] = albumPermission
		}
		if albumTitle != nil{
			dictionary["album_title"] = albumTitle
		}
		if albumType != nil{
			dictionary["album_type"] = albumType
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         albumCover = aDecoder.decodeObject(forKey: "album_cover") as? String
         albumId = aDecoder.decodeObject(forKey: "album_id") as? String
         albumPermission = aDecoder.decodeObject(forKey: "album_permission") as? String
         albumTitle = aDecoder.decodeObject(forKey: "album_title") as? String
         albumType = aDecoder.decodeObject(forKey: "album_type") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if albumCover != nil{
			aCoder.encode(albumCover, forKey: "album_cover")
		}
		if albumId != nil{
			aCoder.encode(albumId, forKey: "album_id")
		}
		if albumPermission != nil{
			aCoder.encode(albumPermission, forKey: "album_permission")
		}
		if albumTitle != nil{
			aCoder.encode(albumTitle, forKey: "album_title")
		}
		if albumType != nil{
			aCoder.encode(albumType, forKey: "album_type")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
