//
//    EventMember.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


class EventMember : NSObject, NSCoding{
    
    var firstName : String!
    var lastName : String!
    var memberId : String!
    var uploadImage : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        firstName = json["first_name"].stringValue
        lastName = json["last_name"].stringValue
        memberId = json["member_id"].stringValue
        uploadImage = json["upload_image"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if memberId != nil{
            dictionary["member_id"] = memberId
        }
        if uploadImage != nil{
            dictionary["upload_image"] = uploadImage
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        memberId = aDecoder.decodeObject(forKey: "member_id") as? String
        uploadImage = aDecoder.decodeObject(forKey: "upload_image") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if memberId != nil{
            aCoder.encode(memberId, forKey: "member_id")
        }
        if uploadImage != nil{
            aCoder.encode(uploadImage, forKey: "upload_image")
        }
        
    }
    
}
