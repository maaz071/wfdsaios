//
//	Item.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Item : NSObject, NSCoding{

	var amount : String!
	var name : String!
    var quantity : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		amount = json["amount"].stringValue
		name = json["name"].stringValue
        quantity = json["qty"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if amount != nil{
			dictionary["amount"] = amount
		}
		if name != nil{
			dictionary["name"] = name
		}
        if quantity != nil{
            dictionary["qty"] = quantity
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         amount = aDecoder.decodeObject(forKey: "amount") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         quantity = aDecoder.decodeObject(forKey: "qty") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if amount != nil{
			aCoder.encode(amount, forKey: "amount")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
        if quantity != nil{
            aCoder.encode(quantity, forKey: "qty")
        }
	}

}
