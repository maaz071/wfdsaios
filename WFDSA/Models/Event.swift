//
//	Event.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Event : NSObject, NSCoding{

	var agenda : String!
	var cityId : String!
	var countryId : String!
	var createdBy : String!
	var createdOn : String!
	var endDate : String!
	var eventId : String!
	var eventUserType : String!
	var fee : String!
	var latLng : String!
	var location : String!
	var noSeats : String!
	var permission : String!
	var place : String!
	var speaker : String!
	var startDate : String!
	var status : String!
	var title : String!
	var updatedBy : String!
	var updatedOn : String!
	var uploadImage : String!
	var venue : String!
	var venueAddress : String!
    var galleryImages : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		agenda = json["agenda"].stringValue
		cityId = json["city_id"].stringValue
		countryId = json["country_id"].stringValue
		createdBy = json["created_by"].stringValue
		createdOn = json["created_on"].stringValue
		endDate = json["end_date"].stringValue
		eventId = json["event_id"].stringValue
		eventUserType = json["event_user_type"].stringValue
		fee = json["fee"].stringValue
		latLng = json["latLng"].stringValue
		location = json["location"].stringValue
		noSeats = json["no_seats"].stringValue
		permission = json["permission"].stringValue
		place = json["place"].stringValue
		speaker = json["speaker"].stringValue
		startDate = json["start_date"].stringValue
		status = json["status"].stringValue
		title = json["title"].stringValue
		updatedBy = json["updated_by"].stringValue
		updatedOn = json["updated_on"].stringValue
		uploadImage = json["upload_image"].stringValue
		venue = json["venue"].stringValue
		venueAddress = json["venue_address"].stringValue
        galleryImages = json["gallery_images"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if agenda != nil{
			dictionary["agenda"] = agenda
		}
		if cityId != nil{
			dictionary["city_id"] = cityId
		}
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if createdOn != nil{
			dictionary["created_on"] = createdOn
		}
		if endDate != nil{
			dictionary["end_date"] = endDate
		}
		if eventId != nil{
			dictionary["event_id"] = eventId
		}
		if eventUserType != nil{
			dictionary["event_user_type"] = eventUserType
		}
		if fee != nil{
			dictionary["fee"] = fee
		}
		if latLng != nil{
			dictionary["latLng"] = latLng
		}
		if location != nil{
			dictionary["location"] = location
		}
		if noSeats != nil{
			dictionary["no_seats"] = noSeats
		}
		if permission != nil{
			dictionary["permission"] = permission
		}
		if place != nil{
			dictionary["place"] = place
		}
		if speaker != nil{
			dictionary["speaker"] = speaker
		}
		if startDate != nil{
			dictionary["start_date"] = startDate
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
		if updatedBy != nil{
			dictionary["updated_by"] = updatedBy
		}
		if updatedOn != nil{
			dictionary["updated_on"] = updatedOn
		}
		if uploadImage != nil{
			dictionary["upload_image"] = uploadImage
		}
		if venue != nil{
			dictionary["venue"] = venue
		}
		if venueAddress != nil{
			dictionary["venue_address"] = venueAddress
		}
        if galleryImages != nil{
            dictionary["gallery_images"] = galleryImages
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         agenda = aDecoder.decodeObject(forKey: "agenda") as? String
         cityId = aDecoder.decodeObject(forKey: "city_id") as? String
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         createdBy = aDecoder.decodeObject(forKey: "created_by") as? String
         createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
         endDate = aDecoder.decodeObject(forKey: "end_date") as? String
         eventId = aDecoder.decodeObject(forKey: "event_id") as? String
         eventUserType = aDecoder.decodeObject(forKey: "event_user_type") as? String
         fee = aDecoder.decodeObject(forKey: "fee") as? String
         latLng = aDecoder.decodeObject(forKey: "latLng") as? String
         location = aDecoder.decodeObject(forKey: "location") as? String
         noSeats = aDecoder.decodeObject(forKey: "no_seats") as? String
         permission = aDecoder.decodeObject(forKey: "permission") as? String
         place = aDecoder.decodeObject(forKey: "place") as? String
         speaker = aDecoder.decodeObject(forKey: "speaker") as? String
         startDate = aDecoder.decodeObject(forKey: "start_date") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         updatedBy = aDecoder.decodeObject(forKey: "updated_by") as? String
         updatedOn = aDecoder.decodeObject(forKey: "updated_on") as? String
         uploadImage = aDecoder.decodeObject(forKey: "upload_image") as? String
         venue = aDecoder.decodeObject(forKey: "venue") as? String
         venueAddress = aDecoder.decodeObject(forKey: "venue_address") as? String
        galleryImages = aDecoder.decodeObject(forKey: "gallery_images") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if agenda != nil{
			aCoder.encode(agenda, forKey: "agenda")
		}
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if createdBy != nil{
			aCoder.encode(createdBy, forKey: "created_by")
		}
		if createdOn != nil{
			aCoder.encode(createdOn, forKey: "created_on")
		}
		if endDate != nil{
			aCoder.encode(endDate, forKey: "end_date")
		}
		if eventId != nil{
			aCoder.encode(eventId, forKey: "event_id")
		}
		if eventUserType != nil{
			aCoder.encode(eventUserType, forKey: "event_user_type")
		}
		if fee != nil{
			aCoder.encode(fee, forKey: "fee")
		}
		if latLng != nil{
			aCoder.encode(latLng, forKey: "latLng")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if noSeats != nil{
			aCoder.encode(noSeats, forKey: "no_seats")
		}
		if permission != nil{
			aCoder.encode(permission, forKey: "permission")
		}
		if place != nil{
			aCoder.encode(place, forKey: "place")
		}
		if speaker != nil{
			aCoder.encode(speaker, forKey: "speaker")
		}
		if startDate != nil{
			aCoder.encode(startDate, forKey: "start_date")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if updatedBy != nil{
			aCoder.encode(updatedBy, forKey: "updated_by")
		}
		if updatedOn != nil{
			aCoder.encode(updatedOn, forKey: "updated_on")
		}
		if uploadImage != nil{
			aCoder.encode(uploadImage, forKey: "upload_image")
		}
		if venue != nil{
			aCoder.encode(venue, forKey: "venue")
		}
		if venueAddress != nil{
			aCoder.encode(venueAddress, forKey: "venue_address")
		}
        if galleryImages != nil{
            aCoder.encode(galleryImages, forKey: "gallery_images")
        }
	}

}
