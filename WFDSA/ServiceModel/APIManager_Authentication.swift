
import UIKit
import Alamofire
import SwiftyJSON


extension APIManager{
    
    func authenticateUserWith(parameters: [String:String],
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.authenticateUserWith(parameters: parameters, success: success, failure: failure)
        
    }
    
    func passwordConfirmation(parameters: [String:String],
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.passwordConfirmation(parameters: parameters, success: success, failure: failure)
    }

    func authenticateSocialUser(parameters: [String:String],
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.authenticateSocialUser(parameters: parameters, success: success, failure: failure)
    }

    
    
    
    func userSignupWith(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.userSignupWith(parameters: parameters, success: success, failure: failure)
    }
    
    
    
    func forgotUserPassword(parameters: Parameters,
                            success:@escaping DefaultArrayResultAPISuccessClosure,
                            failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.forgotUserPassword(parameters: parameters, success: success, failure: failure)
    }    
    
    
    func updateDevicetoken(parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        
        authenticationManagerAPI.updateDevicetoken(parameters: parameters, success: success, failure: failure)
    }
    
    
    
    
    
    func updateUserInfo(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.updateUserInfo(parameters: parameters, success: success, failure: failure)
    }
    
    func uploadGalleryImages(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.uploadGalleryImages(parameters: parameters, success: success, failure: failure)
    }
    
    func paymentComplete(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.paymentComplete(parameters: parameters, success: success, failure: failure)
        
    }
    
    
    func userResendCode(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.userResendCode(parameters: parameters, success: success, failure: failure)
    }

    func logout(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        authenticationManagerAPI.logout(parameters: parameters, success: success, failure: failure)
    }
    
    
}
