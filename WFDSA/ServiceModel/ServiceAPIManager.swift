//
//  ServiceAPIManager.swift
//  Template
//
//  Created by alijabbar on 4/20/17.
//  Copyright © 2017 INGIC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ServiceAPIManager: APIManagerBase {
    
    func getSingleEvent(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.SingleEvent.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getRecentThreeResources(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.RecentResources.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getResourcesCategories(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.ResourcesCategories.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getResourcesByCategories(parameters: Parameters,
                                success:@escaping DefaultArrayResultAPISuccessClosure,
                                failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.ResourcesByCategories.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getLatestAnnouncement(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.LatestAnnouncement.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getAllAnnouncement(parameters: Parameters,
                               success:@escaping DefaultArrayResultAPISuccessClosure,
                               failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.AllAnnouncement.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getAllEvents(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.AllEvents.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getEventGallery(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.GetEventGallery.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getAlbumGallery(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.AlbumGallery.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getEventMembers(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.GetEventMembers.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getEventDetails(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.GetEventDetails.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func checkIn(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.CheckIn.rawValue)!
        self.postRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getPolls(parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.GetPolls.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func addPollAnswer(parameters: Parameters,
                  success:@escaping DefaultArrayResultAPISuccessClosure,
                  failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.AddPollAnswer.rawValue)!
        self.postRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getPhotoAlbums(parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.PhotoAlbums.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getVideoAlbums(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.VideoAlbums.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getLeaderShipMembers(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.LeaderShipMembers.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getCommitteeMembers(parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.CommitteeMembers.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getDsaMembers(parameters: Parameters,
                             success:@escaping DefaultArrayResultAPISuccessClosure,
                             failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.DsaMembers.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getRoleMembers(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.RoleMembers.rawValue, params: parameters as! [String : String])!
        self.postRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func addLike(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.AddLikes.rawValue, params: parameters as! [String : String])!
        self.postRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getUserGuides(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.UserGuides.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getMyInvoices(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){

        let route: URL = POSTURLforRoute(route: Route.MyInvoices.rawValue)!

        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getInvoicesDetails(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.InvoicesDetails.rawValue, params: parameters as! [String : String])!
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getMyPayments(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.MyPayments.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func registerForEvent(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.RegisterEvent.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func registerWithAmountForEvent(parameters: Parameters,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.RegisterEventAmount.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func invoicePayment(parameters: Parameters,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.InvoicePayment.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getAboutUs(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.AboutUs.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getContactUs(parameters: Parameters,
                    success:@escaping DefaultArrayResultAPISuccessClosure,
                    failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.ContactUs.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func submitContactUs(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.SubmitContactUs.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getServiceTracker(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: NSURL = URLforRoute(route: Route.ServiceTracker.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func getServicesDetail(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.ServiceDetail.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getContactDetail(parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.Contact.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func addToCart(parameters: Parameters,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        
        
        let route = POSTURLforRoute(route: Route.AddToCart.rawValue)!
        self.postRequestWithMultipart(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func editCart(parameters: Parameters,
                   success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        
        let route = POSTURLforRoute(route: Route.EditCart.rawValue)!
        self.postRequestWithMultipart(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func getCart(parameters: Parameters,
                   success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: NSURL = URLforRoute(route: Route.GetCart.rawValue, params: parameters as! [String : String])!
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func getCartCount(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: NSURL = URLforRoute(route: Route.GetCartCount.rawValue, params: parameters as! [String : String])!
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func deleteCart(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        var route = POSTURLforRoute(route: Route.DeleteCart.rawValue)!
        
        let cartId = parameters["cartId"] as! String
        route.appendPathComponent(cartId)
        
        print(route)
    
        self.deleteRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
}

extension APIManager {
    
    func getSingleEvent(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getSingleEvent(parameters: parameters, success: success, failure: failure)
    }
    
    func getRecentThreeResources(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getRecentThreeResources(parameters: parameters, success: success, failure: failure)
    }
    
    func getResourcesCategories(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getResourcesCategories(parameters: parameters, success: success, failure: failure)
    }
    
    func getResourcesByCategories(parameters: Parameters,
                                success:@escaping DefaultArrayResultAPISuccessClosure,
                                failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getResourcesByCategories(parameters: parameters, success: success, failure: failure)
    }
    
    func getLatestAnnouncement(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getLatestAnnouncement(parameters: parameters, success: success, failure: failure)
    }
    
    func getAllAnnouncement(parameters: Parameters,
                               success:@escaping DefaultArrayResultAPISuccessClosure,
                               failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getAllAnnouncement(parameters: parameters, success: success, failure: failure)
    }
    
    func getAllEvents(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getAllEvents(parameters: parameters, success: success, failure: failure)
    }
    
    func getEventGallery(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getEventGallery(parameters: parameters, success: success, failure: failure)
    }
    
    func getAlbumGallery(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getAlbumGallery(parameters: parameters, success: success, failure: failure)
    }

    func getEventMembers(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getEventMembers(parameters: parameters, success: success, failure: failure)
    }
    
    func getEventDetails(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getEventDetails(parameters: parameters, success: success, failure: failure)
    }
    
    func checkIn(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.checkIn(parameters: parameters, success: success, failure: failure)
    }
    
    func getPolls(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getPolls(parameters: parameters, success: success, failure: failure)
    }
    
    func addPollAnswer(parameters: Parameters,
                  success:@escaping DefaultArrayResultAPISuccessClosure,
                  failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.addPollAnswer(parameters: parameters, success: success, failure: failure)
    }
    
    
    func getUserGuides(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getUserGuides(parameters: parameters, success: success, failure: failure)
    }
    
    func registerForEvent(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.registerForEvent(parameters: parameters, success: success, failure: failure)
    }
    
    func registerWithAmountForEvent(parameters: Parameters,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.registerWithAmountForEvent(parameters: parameters, success: success, failure: failure)
    }
    
    func invoicePayment(parameters: Parameters,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.invoicePayment(parameters: parameters, success: success, failure: failure)
    }

    
    func getMyInvoices(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getMyInvoices(parameters: parameters, success: success, failure: failure)
    }
    
    func getInvoicesDetails(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getInvoicesDetails(parameters: parameters, success: success, failure: failure)
    }
    
    func getMyPayments(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getMyPayments(parameters: parameters, success: success, failure: failure)
    }
    
    func getAboutUs(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getAboutUs(parameters: parameters, success: success, failure: failure)
    }
    
    func getContactUs(parameters: Parameters,
                    success:@escaping DefaultArrayResultAPISuccessClosure,
                    failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getContactUs(parameters: parameters, success: success, failure: failure)
    }
    
    func submitContactUs(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.submitContactUs(parameters: parameters, success: success, failure: failure)
    }
    
    func getPhotoAlbums(parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getPhotoAlbums(parameters: parameters, success: success, failure: failure)
    }
    
    func getVideoAlbums(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getVideoAlbums(parameters: parameters, success: success, failure: failure)
    }

    func getLeaderShipMembers(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getLeaderShipMembers(parameters: parameters, success: success, failure: failure)
    }
    
    func getCommitteeMembers(parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getCommitteeMembers(parameters: parameters, success: success, failure: failure)
    }
    
    func getDsaMembers(parameters: Parameters,
                             success:@escaping DefaultArrayResultAPISuccessClosure,
                             failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getDsaMembers(parameters: parameters, success: success, failure: failure)
    }
    
    func getRoleMembers(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getRoleMembers(parameters: parameters, success: success, failure: failure)
    }
    
    func addLike(parameters: Parameters,
                       success:@escaping DefaultArrayResultAPISuccessClosure,
                       failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.addLike(parameters: parameters, success: success, failure: failure)
    }
    
    func getServiceTracker(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getServiceTracker(parameters: parameters, success: success, failure: failure)
    }
    
    func getServicesDetail(parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getServicesDetail(parameters: parameters, success: success, failure: failure)
    }
    
    func getContactDetail(parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getContactDetail(parameters: parameters, success: success, failure: failure)
    }
    
    func addToCart(parameters: Parameters,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.addToCart(parameters: parameters, success: success, failure: failure)
    }
    
    func editCart(parameters: Parameters,
                   success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.editCart(parameters: parameters, success: success, failure: failure)
    }
    
    func getCart(parameters: Parameters,
                   success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getCart(parameters: parameters, success: success, failure: failure)
    }
    
    func getCartCount(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getCartCount(parameters: parameters, success: success, failure: failure)
    }
    
    func deleteCart(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.deleteCart(parameters: parameters, success: success, failure: failure)
    }
    
}
