//
//  LocationAPIManager.swift
//  Template
//
//  Created by alijabbar on 4/19/17.
//  Copyright © 2017 INGIC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LocationAPIManager: APIManagerBase {
    
    func getLocations(parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.GetLocations.rawValue, params: parameters as! [String : String])!
        
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func addLocation(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        
        self.postRequestWith(route: URL(string: Constants.BaseURL+Route.AddLocations.rawValue)!, parameters: parameters, success: success, failure: failure)
    }
    
    func editLocation(parameters: Parameters,
                     success:@escaping DefaultArrayResultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure){
        
        self.postRequestWith(route: URL(string: Constants.BaseURL+Route.EditLocations.rawValue)!, parameters: parameters, success: success, failure: failure)
    }

}

extension APIManager {
    func getLocations(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        self.locationManager.getLocations(parameters: parameters, success: success, failure: failure)
    }
    
    func addLocation(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        
        
        self.locationManager.addLocation(parameters: parameters, success: success, failure: failure)
    }
    
    func editLocation(parameters: Parameters,
                     success:@escaping DefaultArrayResultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure){
        
        
        
        self.locationManager.editLocation(parameters: parameters, success: success, failure: failure)
    }

}
