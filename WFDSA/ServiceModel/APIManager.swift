
import UIKit
import Alamofire

typealias DefaultAPIFailureClosure = (NSError) -> Void
typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void
typealias DefaultArrayResultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void


protocol APIErrorHandler {
    func handleErrorFromResponse(response: Dictionary<String,AnyObject>)
    func handleErrorFromERror(error:NSError)
}


class APIManager: NSObject {
    
    static let sharedInstance = APIManager()
    
    var serverToken: String? {
        get{
            return AppStateManager.sharedInstance.loggedInUser.token
        }
        
    }
    
    let authenticationManagerAPI = AuthenticationAPIManager()
    let locationManager = LocationAPIManager()
    let serviceManager = ServiceAPIManager()
    
    override init() {
        super.init()
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 3
        configuration.timeoutIntervalForResource = 60
        let _ = Alamofire.SessionManager(configuration: configuration)
    }
    
}
