

import UIKit
import Alamofire
import SwiftyJSON
class AuthenticationAPIManager: APIManagerBase {


    func authenticateUserWith(parameters:[String:String],
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
    
        let route: URL = POSTURLforRoute(route: Route.Login.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func passwordConfirmation(parameters:[String:String],
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.PasswordConfirmation.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }

    
    func authenticateSocialUser(parameters:[String:String],
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.SocialLogin.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
        
    }
    
    func userSignupWith(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.Register.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func updateDevicetoken(parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.DeviceToken.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func updateUserInfo(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.UpdateProfile.rawValue)!
        
        self.postRequestWithMultipart(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func uploadGalleryImages(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.UploadGalleryImages.rawValue)!
        self.postJSONRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func forgotUserPassword(parameters: Parameters,
                            success:@escaping DefaultArrayResultAPISuccessClosure,
                            failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.ForgotPassword.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func paymentComplete(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.Checkout.rawValue)!
        self.postRequestWithMultipart(route: route, parameters: parameters, success: success, failure: failure)
        
    }
    
    func userResendCode(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = URLforRoute(route: Route.ResendCode.rawValue, params: parameters as! [String : String])! as URL
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func logout(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.Logout.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
}
