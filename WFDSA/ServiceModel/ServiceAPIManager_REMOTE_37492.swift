//
//  ServiceAPIManager.swift
//  Template
//
//  Created by alijabbar on 4/20/17.
//  Copyright © 2017 INGIC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ServiceAPIManager: APIManagerBase {
    
    func getSingleEvent(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.SingleEvent.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getRecentThreeResources(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.RecentResources.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getLatestAnnouncement(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.LatestAnnouncement.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getAllEvents(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.AllEvents.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getEventDetails(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.GetEventDetails.rawValue, params: parameters as! [String : String])!
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
    }
    
    func getUserGuides(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        
        let route: URL = POSTURLforRoute(route: Route.UserGuides.rawValue)!
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure)
    }
    
    func getServiceTracker(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: NSURL = URLforRoute(route: Route.ServiceTracker.rawValue, params: parameters as! [String : String])!
        
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func getServicesDetail(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.ServiceDetail.rawValue, params: parameters as! [String : String])!
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)

    }
    
    func getContactDetail(parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        let route: NSURL = URLforRoute(route: Route.Contact.rawValue, params: parameters as! [String : String])!
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func addToCart(parameters: Parameters,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        
        
        let route = POSTURLforRoute(route: Route.AddToCart.rawValue)!
        self.postRequestWithMultipart(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func editCart(parameters: Parameters,
                   success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        
        let route = POSTURLforRoute(route: Route.EditCart.rawValue)!
        self.postRequestWithMultipart(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func getCart(parameters: Parameters,
                   success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: NSURL = URLforRoute(route: Route.GetCart.rawValue, params: parameters as! [String : String])!
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func getCartCount(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        
        let route: NSURL = URLforRoute(route: Route.GetCartCount.rawValue, params: parameters as! [String : String])!
        
        self.getRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
    func deleteCart(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        var route = POSTURLforRoute(route: Route.DeleteCart.rawValue)!
        
        let cartId = parameters["cartId"] as! String
        route.appendPathComponent(cartId)
        
        print(route)
    
        self.deleteRequestWith(route: route as URL, parameters: parameters, success: success, failure: failure)
        
    }
    
}

extension APIManager {
    
    func getSingleEvent(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getSingleEvent(parameters: parameters, success: success, failure: failure)
    }
    
    func getRecentThreeResources(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getRecentThreeResources(parameters: parameters, success: success, failure: failure)
    }
    
    func getLatestAnnouncement(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getLatestAnnouncement(parameters: parameters, success: success, failure: failure)
    }
    
    func getAllEvents(parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getAllEvents(parameters: parameters, success: success, failure: failure)
    }
    
    func getEventDetails(parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getEventDetails(parameters: parameters, success: success, failure: failure)
    }
    
    func getUserGuides(parameters: Parameters,
                                 success:@escaping DefaultArrayResultAPISuccessClosure,
                                 failure:@escaping DefaultAPIFailureClosure){
        self.serviceManager.getUserGuides(parameters: parameters, success: success, failure: failure)
    }


    
    func getServiceTracker(parameters: Parameters,
                      success:@escaping DefaultArrayResultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getServiceTracker(parameters: parameters, success: success, failure: failure)
    }
    
    func getServicesDetail(parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getServicesDetail(parameters: parameters, success: success, failure: failure)
    }
    
    func getContactDetail(parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getContactDetail(parameters: parameters, success: success, failure: failure)
    }
    
    func addToCart(parameters: Parameters,
                          success:@escaping DefaultArrayResultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.addToCart(parameters: parameters, success: success, failure: failure)
    }
    
    func editCart(parameters: Parameters,
                   success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.editCart(parameters: parameters, success: success, failure: failure)
    }
    
    func getCart(parameters: Parameters,
                   success:@escaping DefaultArrayResultAPISuccessClosure,
                   failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getCart(parameters: parameters, success: success, failure: failure)
    }
    
    func getCartCount(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.getCartCount(parameters: parameters, success: success, failure: failure)
    }
    
    func deleteCart(parameters: Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure){
        
        self.serviceManager.deleteCart(parameters: parameters, success: success, failure: failure)
    }
    
}
