//
//  Utility.m
//  techplanner-customer-ios
//
//  Created by Muhammad Adil on 10/04/2018.
//  Copyright © 2017 Muhammad Adil. All rights reserved.
//

#import "Utility.h"
#import "WFDSA-Bridging-Header.h"

@interface Utility ()

@end

@implementation Utility

static Utility *utility = nil;

+ (Utility *)sharedUtility {
	static Utility *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedInstance = [Utility new];
	});
	return sharedInstance;
}

/*
+ (NSString *)mimeTypeForFileAtPath:(NSString *)path {
	if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
		return nil;
	}
	
	CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
	CFStringRef mimeType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
	CFRelease(UTI);
	if (!mimeType) {
		return @"application/octet-stream";
	}
	
	return (__bridge NSString *)mimeType;
}
*/

+ (ScreenSize)screenSize {
	CGFloat height = CGRectGetHeight([UIScreen mainScreen].bounds);
	if (height == 480.0f)
		return ScreenSize3_5Inch;
	else if (height == 568.0f)
		return ScreenSize4_0Inch;
	else if (height == 667.0f)
		return ScreenSize4_7Inch;
	else if (height == 736.0f)
		return ScreenSize5_5Inch;
	else if (height == 1024.0f)
		return ScreenSize9_7Inch;
	else if (height == 1366.0f)
		return ScreenSize12_9Inch;
	
	return ScreenSizeUnknown;
}

+ (CGFloat)scaleFactor {
    return (CGRectGetWidth([UIScreen mainScreen].bounds) / 375);
}

#pragma mark - Validation
+ (BOOL)validateEmailWithString:(NSString *)candidate {
	NSString *emailRegex =
	@"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
	@"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
	@"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
	@"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
	@"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
	@"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
	@"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
	
	return [emailTest evaluateWithObject:candidate];
}

+ (BOOL)validatePhoneNumberWithString:(NSString *)candidate {
	NSString *phoneRegex = @"^(\\+)[0-9]{6,14}$";
	NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
	return [test evaluateWithObject:candidate];
}

+ (NSString *)getRandomPINString:(NSInteger)length {
	NSMutableString *returnString = [NSMutableString stringWithCapacity:length];
	NSString *numbers = @"0123456789";
	
	[returnString appendFormat:@"%C", [numbers characterAtIndex:(arc4random() % ([numbers length] - 1)) + 1]];
	
	for (NSInteger i = 1; i < length; i++) {
		[returnString appendFormat:@"%C", [numbers characterAtIndex:arc4random() % [numbers length]]];
	}
	
	return returnString;
}

/*
+ (BOOL)isLightColor:(UIColor *)color {
    
    NSScanner *scanner = [NSScanner scannerWithString:[[UIColor hexFromUIColor:color] stringByReplacingOccurrencesOfString:@"#" withString:@""]];
    uint hex;
    if ([scanner scanHexInt:&hex]) {
        // Parsing successful. We have a big int representing the 0xBD8F60 value
        int r = (hex >> 16) & 0xFF; // get the first byte
        int g = (hex >>  8) & 0xFF; // get the middle byte
        int b = (hex      ) & 0xFF; // get the last byte
        
        int lightness = ((r*299)+(g*587)+(b*114))/1000; //get lightness value
        
        if (lightness < 127) { //127 = middle of possible lightness value
            return NO;
        }
        else return YES;
    }
    
    NSLog(@"Error detecting light color");
    return YES;
}
*/

+ (UIImage *)imageWithView:(UIView *)view {
	UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, [UIScreen mainScreen].scale);
	[view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
	UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return snapshotImage;
}

+ (NSArray *)findID:(NSString *)fID withVarName:(NSString *)varName inArray:(NSArray *)array {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", varName, fID];
    return [array filteredArrayUsingPredicate:predicate];
}

+ (UIImage *)imageForNavigationBar {
	CGRect rect = CGRectMake(0, 0, 1, 1);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(context, [[[UIColor redColor] colorWithAlphaComponent:0.7] CGColor]);
	CGContextFillRect(context, rect);
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return image;
}

+ (BOOL)isDeviceLanguageDirectionRTL {
	return ([NSLocale characterDirectionForLanguage:[[NSLocale preferredLanguages] objectAtIndex:0]] == NSLocaleLanguageDirectionRightToLeft);
}

+ (void)cacheLanguage:(NSString *)language {
	[[NSUserDefaults standardUserDefaults] setObject:language forKey:@"languageCode"];
}

+ (CAGradientLayer *)gradientViewWithFrame:(CGRect)frame withStartColor:(UIColor *)startColor withEndColor:(UIColor *)endColor andRotation:(CGFloat)angle {
	CAGradientLayer *gradient = [CAGradientLayer layer];
	gradient.frame = (CGRect){CGPointZero, frame.size};
	gradient.colors = [NSArray arrayWithObjects:(id)[startColor CGColor], (id)[endColor CGColor], nil];
	
	CGFloat x = angle / 360;
	float a = pow(sinf((2 * M_PI * ((x + 0.75) / 2))), 2);
	float b = pow(sinf((2 * M_PI * ((x + 0.00) / 2))), 2);
	float c = pow(sinf((2 * M_PI * ((x + 0.25) / 2))), 2);
	float d = pow(sinf((2 * M_PI * ((x + 0.50) / 2))), 2);
	
	[gradient setStartPoint:CGPointMake(a, b)];
	[gradient setEndPoint:CGPointMake(c, d)];
	
	return gradient;
}

+ (NSString *)getDeviceName {
	return [[UIDevice currentDevice] name];
}

+ (NSData *)compressedImage:(UIImage *)image {
	CGFloat compression = 0.9f;
	CGFloat maxCompression = 0.1f;
	NSInteger maxFileSize = 256 * 1024;
	
	NSData *imageData = UIImageJPEGRepresentation(image, compression);
	
	while ([imageData length] > maxFileSize && compression > maxCompression) {
		compression -= 0.1;
		imageData = UIImageJPEGRepresentation(image, compression);
	}
	
	return imageData;
}

+ (NSDictionary *)callingCodeMap {
	return @{@"AD" : @"+376",
			 @"AE" : @"+971",
			 @"AF" : @"+93",
			 @"AG" : @"+1268",
			 @"AI" : @"+1264",
			 @"AL" : @"+355",
			 @"AM" : @"+374",
			 @"AN" : @"+599",
			 @"AO" : @"+244",
			 @"AQ" : @"+672",
			 @"AR" : @"+54",
			 @"AS" : @"+1684",
			 @"AT" : @"+43",
			 @"AU" : @"+61",
			 @"AW" : @"+297",
			 @"AZ" : @"+994",
			 @"BA" : @"+387",
			 @"BB" : @"+1246",
			 @"BD" : @"+880",
			 @"BE" : @"+32",
			 @"BF" : @"+226",
			 @"BG" : @"+359",
			 @"BH" : @"+973",
			 @"BI" : @"+257",
			 @"BJ" : @"+229",
			 @"BL" : @"+590",
			 @"BM" : @"+1441",
			 @"BN" : @"+673",
			 @"BO" : @"+591",
			 @"BR" : @"+55",
			 @"BS" : @"+1242",
			 @"BT" : @"+975",
			 @"BW" : @"+267",
			 @"BY" : @"+375",
			 @"BZ" : @"+501",
			 @"CA" : @"+1",
			 @"CC" : @"+61",
			 @"CD" : @"+243",
			 @"CF" : @"+236",
			 @"CG" : @"+242",
			 @"CH" : @"+41",
			 @"CI" : @"+225",
			 @"CK" : @"+682",
			 @"CL" : @"+56",
			 @"CM" : @"+237",
			 @"CN" : @"+86",
			 @"CO" : @"+57",
			 @"CR" : @"+506",
			 @"CU" : @"+53",
			 @"CV" : @"+238",
			 @"CX" : @"+61",
			 @"CY" : @"+357",
			 @"CZ" : @"+420",
			 @"DE" : @"+49",
			 @"DJ" : @"+253",
			 @"DK" : @"+45",
			 @"DM" : @"+1767",
			 @"DO" : @"+1809",
			 @"DZ" : @"+213",
			 @"EC" : @"+593",
			 @"EE" : @"+372",
			 @"EG" : @"+20",
			 @"ER" : @"+291",
			 @"ES" : @"+34",
			 @"ET" : @"+251",
			 @"FI" : @"+358",
			 @"FJ" : @"+679",
			 @"FK" : @"+500",
			 @"FM" : @"+691",
			 @"FO" : @"+298",
			 @"FR" : @"+33",
			 @"GA" : @"+241",
			 @"GB" : @"+44",
			 @"GD" : @"+1473",
			 @"GE" : @"+995",
			 @"GH" : @"+233",
			 @"GI" : @"+350",
			 @"GL" : @"+299",
			 @"GM" : @"+220",
			 @"GN" : @"+224",
			 @"GQ" : @"+240",
			 @"GR" : @"+30",
			 @"GT" : @"+502",
			 @"GU" : @"+1671",
			 @"GW" : @"+245",
			 @"GY" : @"+592",
			 @"HK" : @"+852",
			 @"HN" : @"+504",
			 @"HR" : @"+385",
			 @"HT" : @"+509",
			 @"HU" : @"+36",
			 @"ID" : @"+62",
			 @"IE" : @"+353",
			 @"IL" : @"+972",
			 @"IM" : @"+44",
			 @"IN" : @"+91",
			 @"IQ" : @"+964",
			 @"IR" : @"+98",
			 @"IS" : @"+354",
			 @"IT" : @"+39",
			 @"JM" : @"+1876",
			 @"JO" : @"+962",
			 @"JP" : @"+81",
			 @"KE" : @"+254",
			 @"KG" : @"+996",
			 @"KH" : @"+855",
			 @"KI" : @"+686",
			 @"KM" : @"+269",
			 @"KN" : @"+1869",
			 @"KP" : @"+850",
			 @"KR" : @"+82",
			 @"KW" : @"+965",
			 @"KY" : @"+1345",
			 @"KZ" : @"+7",
			 @"LA" : @"+856",
			 @"LB" : @"+961",
			 @"LC" : @"+1758",
			 @"LI" : @"+423",
			 @"LK" : @"+94",
			 @"LR" : @"+231",
			 @"LS" : @"+266",
			 @"LT" : @"+370",
			 @"LU" : @"+352",
			 @"LV" : @"+371",
			 @"LY" : @"+218",
			 @"MA" : @"+212",
			 @"MC" : @"+377",
			 @"MD" : @"+373",
			 @"ME" : @"+382",
			 @"MF" : @"+1599",
			 @"MG" : @"+261",
			 @"MH" : @"+692",
			 @"MK" : @"+389",
			 @"ML" : @"+223",
			 @"MM" : @"+95",
			 @"MN" : @"+976",
			 @"MO" : @"+853",
			 @"MP" : @"+1670",
			 @"MR" : @"+222",
			 @"MS" : @"+1664",
			 @"MT" : @"+356",
			 @"MU" : @"+230",
			 @"MV" : @"+960",
			 @"MW" : @"+265",
			 @"MX" : @"+52",
			 @"MY" : @"+60",
			 @"MZ" : @"+258",
			 @"NA" : @"+264",
			 @"NC" : @"+687",
			 @"NE" : @"+227",
			 @"NG" : @"+234",
			 @"NI" : @"+505",
			 @"NL" : @"+31",
			 @"NO" : @"+47",
			 @"NP" : @"+977",
			 @"NR" : @"+674",
			 @"NU" : @"+683",
			 @"NZ" : @"+64",
			 @"OM" : @"+968",
			 @"PA" : @"+507",
			 @"PE" : @"+51",
			 @"PF" : @"+689",
			 @"PG" : @"+675",
			 @"PH" : @"+63",
			 @"PK" : @"+92",
			 @"PL" : @"+48",
			 @"PM" : @"+508",
			 @"PN" : @"+870",
			 @"PR" : @"+1",
			 @"PT" : @"+351",
			 @"PW" : @"+680",
			 @"PY" : @"+595",
			 @"QA" : @"+974",
			 @"RO" : @"+40",
			 @"RS" : @"+381",
			 @"RU" : @"+7",
			 @"RW" : @"+250",
			 @"SA" : @"+966",
			 @"SB" : @"+677",
			 @"SC" : @"+248",
			 @"SD" : @"+249",
			 @"SE" : @"+46",
			 @"SG" : @"+65",
			 @"SH" : @"+290",
			 @"SI" : @"+386",
			 @"SK" : @"+421",
			 @"SL" : @"+232",
			 @"SM" : @"+378",
			 @"SN" : @"+221",
			 @"SO" : @"+252",
			 @"SR" : @"+597",
			 @"ST" : @"+239",
			 @"SV" : @"+503",
			 @"SY" : @"+963",
			 @"SZ" : @"+268",
			 @"TC" : @"+1649",
			 @"TD" : @"+235",
			 @"TG" : @"+228",
			 @"TH" : @"+66",
			 @"TJ" : @"+992",
			 @"TK" : @"+690",
			 @"TL" : @"+670",
			 @"TM" : @"+993",
			 @"TN" : @"+216",
			 @"TO" : @"+676",
			 @"TR" : @"+90",
			 @"TT" : @"+1868",
			 @"TV" : @"+688",
			 @"TW" : @"+886",
			 @"TZ" : @"+255",
			 @"UA" : @"+380",
			 @"UG" : @"+256",
			 @"US" : @"+1",
			 @"UY" : @"+598",
			 @"UZ" : @"+998",
			 @"VA" : @"+39",
			 @"VC" : @"+1784",
			 @"VE" : @"+58",
			 @"VG" : @"+1284",
			 @"VI" : @"+1340",
			 @"VN" : @"+84",
			 @"VU" : @"+678",
			 @"WF" : @"+681",
			 @"WS" : @"+685",
			 @"YE" : @"+967",
			 @"YT" : @"+262",
			 @"ZA" : @"+27",
			 @"ZM" : @"+260",
			 @"ZW" : @"+263"};
}

+ (NSString *)formatPhoneNumber:(NSString *)text InRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [text stringByReplacingCharactersInRange:range withString:string];
    NSArray *components = [newString componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
    NSString *decimalString = [components componentsJoinedByString:@""];
    
    NSUInteger length = decimalString.length;
    BOOL hasLeadingOne = length > 0 && [decimalString characterAtIndex:0] == '1';
    
    if (length == 0 || (length > 10 && !hasLeadingOne) || (length > 11)) {
        return decimalString;
    }
    
    NSUInteger index = 0;
    NSMutableString *formattedString = [NSMutableString string];
    
    if (length - index > 3) {
        NSString *areaCode = [decimalString substringWithRange:NSMakeRange(index, 3)];
        [formattedString appendFormat:@"(%@) ",areaCode];
        index += 3;
    }
    
    if (length - index > 3) {
        NSString *prefix = [decimalString substringWithRange:NSMakeRange(index, 3)];
        [formattedString appendFormat:@"%@-",prefix];
        index += 3;
    }
    
    NSString *remainder = [decimalString substringFromIndex:index];
    [formattedString appendString:remainder];
    
    return formattedString;
}

+ (NSString *)version {
	return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (NSString *)build {
	return [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
}

+ (NSString *)versionBuild {
	NSString *version = [self version];
	NSString *build = [self build];
	
	NSString *versionBuild = [NSString stringWithFormat:@"v%@", version];
	
	if (![version isEqualToString: build]) {
		versionBuild = [NSString stringWithFormat:@"%@(%@)", versionBuild, build];
	}
	
	return versionBuild;
}

@end
