//
//  FontManager.m
//  techplanner-customer-ios
//
//  Created by Muhammad Adil on 10/04/2018.
//  Copyright © 2017 Muhammad Adil. All rights reserved.
//

#import "FontManager.h"
#import "Utility.h"

@interface FontManager ()

@end

@implementation FontManager

+ (instancetype)sharedManager {
	static FontManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [FontManager new];
    });
    return sharedInstance;
}

#pragma mark - Fonts
+ (void)logFonts {
	for (NSString *family in [UIFont familyNames]) {
		NSLog(@"%@", family);
		
		for (NSString *name in [UIFont fontNamesForFamilyName:@"MullerRegular"]) {
			NSLog(@"  %@", name);
		}
	}
}

+ (UIFont *)getThinFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_THIN size:size];
}

+ (UIFont *)getAdjustedThinFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getThinFontOfSize:adjustedSize];
}

+ (UIFont *)getThinItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_THIN_ITALIC size:size];
}

+ (UIFont *)getAdjustedThinItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getThinItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getExtraLightFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_EXTRA_LIGHT size:size];
}

+ (UIFont *)getAdjustedExtraLightFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getExtraLightFontOfSize:adjustedSize];
}

+ (UIFont *)getExtraLightItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_EXTRA_LIGHT_ITALIC size:size];
}

+ (UIFont *)getAdjustedExtraLightItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getExtraLightItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getLightFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_LIGHT size:size];
}

+ (UIFont *)getAdjustedLightFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getLightFontOfSize:adjustedSize];
}

+ (UIFont *)getLightItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_LIGHT_ITALIC size:size];
}

+ (UIFont *)getAdjustedLightItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getLightItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getRegularFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_REGULAR size:size];
}

+ (UIFont *)getAdjustedRegularFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getRegularFontOfSize:adjustedSize];
}

+ (UIFont *)getRegularItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_ITALIC size:size];
}

+ (UIFont *)getAdjustedRegularItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getRegularItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getMediumFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_MEDIUM size:size];
}

+ (UIFont *)getAdjustedMediumFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getMediumFontOfSize:adjustedSize];
}

+ (UIFont *)getMediumItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_MEDIUM_ITALIC size:size];
}

+ (UIFont *)getAdjustedMediumItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getMediumItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getSemiBoldFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_SEMIBOLD size:size];
}

+ (UIFont *)getAdjustedSemiBoldFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getSemiBoldFontOfSize:adjustedSize];
}

+ (UIFont *)getSemiBoldItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_SEMIBOLD_ITALIC size:size];
}

+ (UIFont *)getAdjustedSemiBoldItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getSemiBoldItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getBoldFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_BOLD size:size];
}

+ (UIFont *)getAdjustedBoldFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getBoldFontOfSize:adjustedSize];
}

+ (UIFont *)getBoldItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_BOLD_ITALIC size:size];
}

+ (UIFont *)getAdjustedBoldItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getBoldItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getExtraBoldFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_BOLD size:size];
}

+ (UIFont *)getAdjustedExtraBoldFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getBoldFontOfSize:adjustedSize];
}

+ (UIFont *)getExtraBoldItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_BOLD_ITALIC size:size];
}

+ (UIFont *)getAdjustedExtraBoldItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getBoldItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getBlackFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_BLACK size:size];
}

+ (UIFont *)getAdjustedBlackFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getBlackFontOfSize:adjustedSize];
}

+ (UIFont *)getBlackItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_BLACK_ITALIC size:size];
}

+ (UIFont *)getAdjustedBlackItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getBlackItalicFontOfSize:adjustedSize];
}

+ (UIFont *)getHeavyFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_HEAVY size:size];
}

+ (UIFont *)getAdjustedHeavyFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getHeavyFontOfSize:adjustedSize];
}

+ (UIFont *)getHeavyItalicFontOfSize:(float)size {
	return [UIFont fontWithName:FONT_HEAVY_ITALIC size:size];
}

+ (UIFont *)getAdjustedHeavyItalicFontOfSize:(float)size {
	CGFloat multiplier = 0;
	switch ([Utility screenSize]) {
		case ScreenSize3_5Inch:
		case ScreenSize4_0Inch:
			multiplier = 0.71f;
			break;
		case ScreenSize4_7Inch:
			multiplier = 0.905f;
			break;
		case ScreenSize5_5Inch:
			multiplier = 1.0f;
			break;
		case ScreenSize5_8Inch:
			multiplier = 0.905f;
			break;
		default:
			multiplier = 1.0f;
			break;
	}
	CGFloat adjustedSize = multiplier * size;
	return [FontManager getHeavyItalicFontOfSize:adjustedSize];
}
    
+ (float)getAdjustedFontSizeFromSize:(float)size {
    CGFloat multiplier = 0;
    switch ([Utility screenSize]) {
        case ScreenSize3_5Inch:
        case ScreenSize4_0Inch:
        multiplier = 0.71f;
        break;
        case ScreenSize4_7Inch:
        multiplier = 0.905f;
        break;
        case ScreenSize5_5Inch:
        multiplier = 1.0f;
        break;
        case ScreenSize5_8Inch:
        multiplier = 0.905f;
        break;
        default:
        multiplier = 1.0f;
        break;
    }
    CGFloat adjustedSize = multiplier * size;
    return adjustedSize;
}

@end
