//
//  FontManager.h
//
//  Created by Muhammad Adil on 10/04/2018.
//  Copyright © 2016 Muhammad Adil. All rights reserved.
//

#define FONT_THIN @""
#define FONT_THIN_ITALIC @""
#define FONT_LIGHT @"Gotham"
#define FONT_LIGHT_ITALIC @"Gotham-Italic"
#define FONT_EXTRA_LIGHT @""
#define FONT_EXTRA_LIGHT_ITALIC @""
#define FONT_REGULAR @"Gotham-Book"
#define FONT_ITALIC @"Gotham-BookItalic"
#define FONT_MEDIUM @"Gotham-Medium"
#define FONT_MEDIUM_ITALIC @"GothamMedium-Italic"
#define FONT_SEMIBOLD @"Gotham-Medium"
#define FONT_SEMIBOLD_ITALIC @"GothamMedium-Italic"
#define FONT_BOLD @"GothamBold"
#define FONT_BOLD_ITALIC @"GothamBold-Italic"
#define FONT_EXTRA_BOLD @"GothamBlack"
#define FONT_EXTRA_BOLD_ITALIC @"GothamBlack-Italic"
#define FONT_BLACK @""
#define FONT_BLACK_ITALIC @""
#define FONT_HEAVY @""
#define FONT_HEAVY_ITALIC @""

#import <UIKit/UIKit.h>

@interface FontManager : NSObject

+ (instancetype)sharedManager;

//Fonts
+ (void)logFonts;

+ (UIFont *)getThinFontOfSize:(float)size;
+ (UIFont *)getAdjustedThinFontOfSize:(float)size;
+ (UIFont *)getThinItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedThinItalicFontOfSize:(float)size;

+ (UIFont *)getExtraLightFontOfSize:(float)size;
+ (UIFont *)getAdjustedExtraLightFontOfSize:(float)size;
+ (UIFont *)getExtraLightItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedExtraLightItalicFontOfSize:(float)size;

+ (UIFont *)getLightFontOfSize:(float)size;
+ (UIFont *)getAdjustedLightFontOfSize:(float)size;
+ (UIFont *)getLightItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedLightItalicFontOfSize:(float)size;

+ (UIFont *)getRegularFontOfSize:(float)size;
+ (UIFont *)getAdjustedRegularFontOfSize:(float)size;
+ (UIFont *)getRegularItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedRegularItalicFontOfSize:(float)size;

+ (UIFont *)getMediumFontOfSize:(float)size;
+ (UIFont *)getAdjustedMediumFontOfSize:(float)size;
+ (UIFont *)getMediumItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedMediumItalicFontOfSize:(float)size;

+ (UIFont *)getSemiBoldFontOfSize:(float)size;
+ (UIFont *)getAdjustedSemiBoldFontOfSize:(float)size;
+ (UIFont *)getSemiBoldItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedSemiBoldItalicFontOfSize:(float)size;

+ (UIFont *)getBoldFontOfSize:(float)size;
+ (UIFont *)getAdjustedBoldFontOfSize:(float)size;
+ (UIFont *)getBoldItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedBoldItalicFontOfSize:(float)size;

+ (UIFont *)getExtraBoldFontOfSize:(float)size;
+ (UIFont *)getAdjustedExtraBoldFontOfSize:(float)size;
+ (UIFont *)getExtraBoldItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedExtraBoldItalicFontOfSize:(float)size;

+ (UIFont *)getBlackFontOfSize:(float)size;
+ (UIFont *)getAdjustedBlackFontOfSize:(float)size;
+ (UIFont *)getBlackItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedBlackItalicFontOfSize:(float)size;

+ (UIFont *)getHeavyFontOfSize:(float)size;
+ (UIFont *)getAdjustedHeavyFontOfSize:(float)size;
+ (UIFont *)getHeavyItalicFontOfSize:(float)size;
+ (UIFont *)getAdjustedHeavyItalicFontOfSize:(float)size;
    
+ (float)getAdjustedFontSizeFromSize:(float)size;

@end
