//
//  Utility.h
//  techplanner-customer-ios
//
//  Created by Muhammad Adil on 10/04/2018.
//  Copyright © 2017 Muhammad Adil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 Uses for device screen sizes

 - ScreenSizeUnknown: Unkown Size
 - ScreenSize3_5Inch: 3.5" Screen Size
 - ScreenSize4_0Inch: 4.0" Screen Size
 - ScreenSize4_7Inch: 4.7" Screen Size
 - ScreenSize5_5Inch: 5.5" Screen Size
 - ScreenSize5_8Inch: 5.8" Screen Size
 - ScreenSize9_7Inch: 9.7" Screen Size
 - ScreenSize12_9Inch: 12.9" Screen Size
 */
typedef NS_ENUM(NSInteger, ScreenSize) {
	ScreenSizeUnknown,
	ScreenSize3_5Inch,
	ScreenSize4_0Inch,
	ScreenSize4_7Inch,
	ScreenSize5_5Inch,
	ScreenSize5_8Inch,
	ScreenSize9_7Inch,
	ScreenSize12_9Inch
};

@interface Utility : NSObject

+ (Utility *)sharedUtility;

//+ (NSString *)mimeTypeForFileAtPath:(NSString *)path;

+ (CGFloat)scaleFactor;
+ (ScreenSize)screenSize;

+ (BOOL)validateEmailWithString:(NSString *)email;
+ (BOOL)validatePhoneNumberWithString:(NSString *)candidate;
+ (NSString *)getRandomPINString:(NSInteger)length;

+ (NSString *)formatPhoneNumber:(NSString *)text InRange:(NSRange)range replacementString:(NSString *)string;

//+ (BOOL)isLightColor:(UIColor *)color;
+ (UIImage *)imageWithView:(UIView *)view;

+ (NSArray *)findID:(NSString *)fID withVarName:(NSString *)varName inArray:(NSArray *)array;

+ (UIImage *)imageForNavigationBar;
+ (BOOL)isDeviceLanguageDirectionRTL;
+ (void)cacheLanguage:(NSString *)language;

+ (CAGradientLayer *)gradientViewWithFrame:(CGRect)frame withStartColor:(UIColor *)startColor withEndColor:(UIColor *)endColor andRotation:(CGFloat)angle;
+ (NSString *)getDeviceName;
+ (NSData *)compressedImage:(UIImage *)image;
+ (NSDictionary *)callingCodeMap;
+ (NSString *)version;
+ (NSString *)build;
+ (NSString *)versionBuild;

@end
