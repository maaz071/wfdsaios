





import UIKit
import RealmSwift

class AppStateManager: NSObject {

    static let sharedInstance = AppStateManager()
    var loggedInUser: User!
    var realm: Realm!

    override init() {
       
        super.init()
        
        if(!(realm != nil)){
            realm = try! Realm()
        }
        
        loggedInUser = realm.objects(User.self).first
    }
    
    func isUserLoggedIn() -> Bool {
        
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    

    func logOutUser() {
        
        if self.isUserLoggedIn() {
            if(!(realm != nil)){
                realm = try! Realm()
            }
            try! realm.write {
                realm.delete(loggedInUser)
            }
            
            self.loggedInUser = nil
            //self.loggedInGuestUser = nil
            //self.isGuestUser = false
        }else {
            if(!(realm != nil)){
                realm = try! Realm()
            }
            try! realm.write {
                realm.delete(loggedInUser)
            }
            self.loggedInUser = nil
        }
        if(!(realm != nil)){
            realm = try! Realm()
        }
    }
  
}
