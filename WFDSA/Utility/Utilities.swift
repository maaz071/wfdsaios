//
//  Utility.swift
//  NewTemplate
//
//  Created by Ali Abdul Jabbar on 2/19/18.
//  Copyright © 2018 ZOTION. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

struct HelperMethods {

    static func addGradient(colors:[CGColor], toview:UIView) {
        let gradient:CAGradientLayer = CAGradientLayer()
        gradient.frame.size = toview.frame.size
        gradient.colors = [colors[0], colors[1]]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        toview.layer.addSublayer(gradient)
    }
    
    static func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }

}


