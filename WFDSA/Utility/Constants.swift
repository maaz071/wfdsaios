//
//  Constants.swift
//  NewTemplate
//
//  Created by Umer on 04/09/2017.
//  Copyright © 2017 ZOTION. All rights reserved.
//

import Foundation
import UIKit
import AnimatedTextInput

let IN_APP_REMOVE_ADS_ID = "com.zotion.NewTemplate.inapp.removeAds"

let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
let defaults = UserDefaults(suiteName: "group.zotion.NewTemplate")!


let SELECTED_THEME_KEY = "SELECTEDTHEMEKEY"
let SELECTED_FONT_KEY = "SELECTEDFONTSKEY"

let themeUpdateNotificationKey = "UpdateThemeNotificationKey"
let showReviewNotificationKey = "ShowReviewPopup"
let updateDataWithSelectedCategoryNotificationKey = "updateDataWithSelectedCategoryNotificationKey"
let updateDataNotificationKey = "updateDataNotificationKey"

let RemindersSelectedRepeatKey = "RemindersSelectedRepeatKey"
let RemindersSelectedStartTimeKey = "RemindersSelectedStartTimeKey"
let RemindersSelectedEndTimeKey = "RemindersSelectedEndTimeKey"
let RemindersDailyReminderSwitchKey = "RemindersDailyReminderSwitchKey"

let AppHasLaunchedBefore = "AppHasLaunchedBefore"


struct AppColors {
    static let appThemeColor = #colorLiteral(red: 0.3607843137, green: 0.337254902, blue: 0.4392156863, alpha: 1)
    static let appThemeSecondaryColor = UIColor(hexString: "AAAAAA")
}

struct DefaultValues {
    static let RemindersDefaultRepeatValue = 3.0
    static let RemindersDailyReminderSwitchValue = true
    static let numberOfItemsInHome = 500
    static let numberOfThemeColorsItemsInARow = 4
    static let TitleViewHeight = 60
    static let sampleFontThemeText = "I watched the storm, so beautiful yet terrific."
    static let samplePreviewFontThemeText = "Almost before we knew it, we had left the ground."
	
    static var SelectedStartTimeForReminders:Date {
        get {
            let key = defaults.object(forKey: RemindersSelectedStartTimeKey)
			guard let startDate = key as? Date else {
				return defaultTimeForReminderFor(start: true)
			}
			return startDate
        }
        set {
            defaults.set(newValue, forKey: RemindersSelectedStartTimeKey)
            defaults.synchronize()
        }
    }
    
    static var SelectedEndTimeForReminders:Date {
        get {
            let key = defaults.object(forKey: RemindersSelectedEndTimeKey)
			guard let endDate = key as? Date else {
				return defaultTimeForReminderFor(start: false)
			}
			return endDate
        }
        set {
            defaults.set(newValue, forKey: RemindersSelectedEndTimeKey)
            defaults.synchronize()
        }
    }
	
	
	static func defaultTimeForReminderFor(start:Bool) -> Date {
		let today = Date()
		let calendar = Calendar(identifier: .gregorian)
		var dateComponents = calendar.dateComponents([.calendar, .day], from: today)
		dateComponents.hour = start ? 10 : 21
		dateComponents.minute = 0
		dateComponents.second = 0
		let date = calendar.date(from: dateComponents)
		return date ?? Date()
	}
	
	static var SelectedRemindersRepeat:Double {
		get {
			let key = defaults.double(forKey: RemindersSelectedRepeatKey)
			return (key != 0.0) ? key : RemindersDefaultRepeatValue
		}
		set {
			defaults.set(newValue, forKey: RemindersSelectedRepeatKey)
			defaults.synchronize()
		}
	}
	
	static var selectedRemindersSwitch: Bool {
		get {
			 return defaults.bool(forKey: RemindersDailyReminderSwitchKey)
		}
		set {
			defaults.set(newValue, forKey: RemindersDailyReminderSwitchKey)
			defaults.synchronize()
		}
	}
}

struct AppTextInputStyle: AnimatedTextInputStyle {
    var lineHeight: CGFloat
    var yPlaceholderPositionOffset: CGFloat
    var textAttributes: [String : Any]?
    let lineActiveColor = AppColors.appThemeColor
    let lineInactiveColor = AppColors.appThemeSecondaryColor
    let placeholderInactiveColor = AppColors.appThemeSecondaryColor
    let activeColor = AppColors.appThemeColor
    let inactiveColor = AppColors.appThemeSecondaryColor
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 14*Utility.scaleFactor())
    let textInputFontColor = UIColor.black
    let placeholderMinFontSize: CGFloat = 10*Utility.scaleFactor()
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 10*Utility.scaleFactor())
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 15*Utility.scaleFactor()
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 10*Utility.scaleFactor()
    let yHintPositionOffset: CGFloat = 0
}

struct Ads {
    static let numberOfIterationToShowIntertitial = 7
#if DEBUG
    static let isDevelopmentOn = true
#else
    static let isDevelopmentOn = false
#endif
    
    static let testBannerID = "ca-app-pub-3940256099942544/2934735716"
    static let productionBannerID = isDevelopmentOn ? testBannerID : "ca-app-pub-8459509244834954/4263640652"
    static let testInterstitialId = "ca-app-pub-3940256099942544/4411468910"
    static let productionInterstitialId = isDevelopmentOn ? testInterstitialId : "ca-app-pub-8459509244834954/9742842824"
    
}


struct AppStore {
    static let appstoreID = "1353374535"
    static let appStoreAppLink = "https://itunes.apple.com/us/app/factpedia-daily-random-facts/id1353374535?ls=1&mt=8"
    static let appStoreAppPromotionMessage = "Via Daily Facts:\(appStoreAppLink)"
}

struct CoreData {
    static let persistentContainer = SharedCoreData.default.persistentContainer
    static let viewContext = SharedCoreData.default.persistentContainer.viewContext
}

struct CoreSpotlight {
    static let domainIdentifier = "com.zotion.NewTemplate.facts"
}


struct Constants{
    
    
    static let kFONT_WIDTH_FACTOR           = UIScreen.main.bounds.width / 414 //resize font according to screen size
    
    static let kWINDOW_FRAME                = UIScreen.main.bounds
    static let kSCREEN_SIZE                 = UIScreen.main.bounds.size
    static let kWINDOW_WIDTH                = UIScreen.main.bounds.size.width
    static let kWINDOW_HIEGHT               = UIScreen.main.bounds.size.height
    
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    
    static let USER_DEFAULTS               = UserDefaults.standard
    
//    static let SINGLETON                   = Singleton.sharedInstance
    
    
    static let NAV_BAR_COLOR               = UIColor(red: 108.0/255.0, green: 211.0/255.0, blue: 234.0/255.0, alpha: 1.0)
    
    static let EXPRESS_COLOR               = UIColor(red: 221.0/255.0, green: 111.0/255.0, blue: 112.0/255.0, alpha: 1.0)
    
    static let SHOP_COLOR               = UIColor(red: 117.0/255.0, green: 208.0/255.0, blue: 98.0/255.0, alpha: 1.0)
    
    static let GAME_COLOR               = UIColor(red: 253.0/255.0, green: 152.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    
    
    static let SEGMENT_BG_COLOR               = UIColor(red: 102.0/255.0, green: 211.0/255.0, blue: 234.0/255.0, alpha: 1.0)
    
    
    static let THEME_COLOR_LIGHT               = UIColor(red: 102.0/255.0, green: 211.0/255.0, blue: 234.0/255.0, alpha: 0.3)
    
    static let PLACEHOLDER_USER                        = #imageLiteral(resourceName: "ic_profile_pic")
    
    static let VALIDATION_FIELD_LENGTH                 = 300
    static let VALIDATION_PASSWORD_LENGTH              = 6
    static let VALIDATION_FIELD_LENGTH_QUANTITY        = 6
    static let VALIDATION_FIELD_LENGTH_PRICE           = 9
    
    static let VALIDATION_USERNAME_MIN                 = "Username must not be more than 15 characters and must not contain any space character."
    static let VALIDATION_VALID_NAME                   = "Please provide a valid name."
    static let VALIDATION_VALID_EMAIL                  = "Please provide a valid Email Address."
    static let VALIDATION_VALID_AGE                    = "Kindly provide a valid age."
    static let VALIDATION_NUMERIC_PHONE                = "Phone number must be numeric with at least 11 digits."
    static let VALIDATION_NUMERIC_CELL                 = "Cell number must be numeric with at least 11 digits."
    
    static let VALIDATION_PASSWORD_MIN                 = "Password should contain atlest 4 characters."
    static let VALIDATION_PASSWORD_MATCH               = "New password and confirm password does not match."
    static let FORGET_PASSWORD                         = "A password has been sent to your email Address!"
    static let PASSWORD_UPDATED                        = "Your Password has been updated"
    
    static let VALIDATION_MAX_FIELD_LENGTH             = "Field must not be more than 15 characters."
    static let VALIDATION_MAX_DESCRIPTION_LENGTH       = "Description must not be more than 300 characters."
    static let VALIDATION_ALL_FIELDS                   = "Kindly fill all the fields."
    static let VALIDATION_VALID_URL                    = "Please provide a valid URL."
    static let VALIDATION_VALID_FILE_NAME              = "Please provide a file name."
    
    static let VALIDATION_IMAGE                        = "Please provide an image."
    static let VALIDATION_TERMS                        = "Kindly select the terms and Conditions."
    
    static let PROFILE_UPDATED                         = "Your Profile has been updated."
    
    static let MESSAGE_LOGOUT                          = "Are you sure you want to logout?"
    
    static let notAvailable                            = "Not Available"
    static let noNetworkConnection                     = "Not Available"
    
    static let BaseURL = "http://www.wfdsamobileportal.com/"
    //"http://35.160.175.165/portfolio/enigma/api/"
    
    static let ImagesBaseURL = "https://www.enigma.ae/mobileapp/enigma/public/images/profile_picture/"
    //"http://35.160.175.165/portfolio/enigma/public/images/profile_picture/"
    
    static let CartImageBaseURL = "https://www.enigma.ae/mobileapp/enigma/public/images/"
    //"http://35.160.175.165/portfolio/enigma/public/images/"
    
    //static let BaseURL = "http://10.1.18.153/enigma/api/"
    
    //static let ImagesBaseURL = "http://10.1.18.153/enigma/public/images/profile_picture/"
}


