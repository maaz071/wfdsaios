//
//  FaceBookUserProfile.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/27/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKCoreKit

class FaceBookUserProfile{
    
    
    
    static var first_Name = ""
    static var last_Name = ""
    static var userImageUrl = ""
    static var userEmail = ""
    static var userFbId = ""
    static var getUserProfileInfoCallBack: ((_ firtsName: String,_ lastName: String,_ email: String,_ imageUrl: String,_ userId: String) -> Void)?
    
    static func getUserFacebookProfile(from:UIViewController){
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        fbLoginManager.logOut()
        
        fbLoginManager.logIn(withReadPermissions: ["email"], from: from) { (result, error) -> Void in
            
            print(error?.localizedDescription ?? "")
            
            if (error == nil){
                print(result?.token ?? "")
                if(result?.token != nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.fetchProfile()
                    }
                }
                else{
                    print("try again")
                }
            }
        }
        
    }
    
    
    static func fetchProfile(){
        print("fetch profile")
        
        if (FBSDKAccessToken.current()) != nil {
            let parameters = ["fields":"email,first_name,last_name, picture.type(large)"]
            
            FBSDKGraphRequest(graphPath: "me", parameters: parameters).start {
                (connection, result  , error) in
                if error != nil {
                    print(error!)
                    return
                }
                let data:[String:AnyObject] = result as! [String : AnyObject]
                print(data)
                
                if let email = data["email"] as? String
                {
                    
                    userEmail = email
                    print(email)
                    
                    
                }
                
                if let firstName = data["first_name"] as? String
                {
                    
                    first_Name = firstName
                    print(first_Name)
                    
                    
                }
                if let lastName = data["last_name"] as? String
                {
                    
                    last_Name = lastName
                    print(last_Name)
                    
                    
                }
                let info = result as! NSDictionary
                if let imageURL = ((info.value(forKey: "picture") as AnyObject).value(forKey:"data") as AnyObject).value(forKey:"url") as? String
                {
                    userImageUrl = imageURL
                    print(imageURL)
                    //Download image from imageURL
                    
                }
                
                if let userId = data["id"] as? String
                {
                    
                    userFbId = userId
                    print(userFbId)
                    
                    
                }
                
                
                getUserProfileInfoCallBack?(first_Name,last_Name,userEmail,userImageUrl,userFbId)
            }
            
            
        }
        
    }
}
