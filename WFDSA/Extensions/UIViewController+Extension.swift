//
//  UIViewController+Extension.swift
//  WFDSA
//
//  Created by Umer Jabbar on 27/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID : String {
        
        //if your storyboard name is same as ControllerName uncomment it
        //return "\(self)"
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
