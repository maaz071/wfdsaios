//
//  MyPaymentTableViewCell.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/31/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class MyPaymentTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var payButton: UIButton!
    weak var delegate:MyInvoicesPaymentViewCellDelegate?
    
    var index = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.borderView.layer.cornerRadius = 3
        self.borderView.layer.borderColor = UIColor.black.cgColor
        self.borderView.layer.borderWidth = 0
    }
    
    open class func height() -> CGFloat {
        return CGFloat(60) * Utility.scaleFactor()
    }
    
    func populateDataFrom(payment:Payment) {
        self.nameLabel.text = payment.title.capitalized
        if payment.paymentStatus == "1" {
            self.payButton.setTitle("Paid", for: .normal)
            self.payButton.isEnabled = false
        }else {
            self.payButton.setTitle("Pay Now\n $\(payment.paymentAmount ?? "")", for: .normal)
            self.payButton.titleLabel?.numberOfLines = 2
            self.payButton.isEnabled = true
        }
    }
    
    @IBAction func payButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapOnPayButton(index: self.index)
    }
}
