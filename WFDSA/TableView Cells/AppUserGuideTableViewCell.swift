//
//  AppUserGuideTableViewCell.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/29/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit


class AppUserGuideTableViewCell: BaseTableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.borderView.layer.cornerRadius = 3
        self.borderView.layer.borderColor = UIColor.black.cgColor
        self.borderView.layer.borderWidth = 1
    }

    open class func height() -> CGFloat {
        return CGFloat(85) * Utility.scaleFactor()
    }
    
    func populateDataFromUserGuide(userGuide:UserGuide) {
        self.nameLabel.text = userGuide.title
    }
}
