//
//  AlbumTableViewCell.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 18/09/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class AlbumTableViewCell: BaseTableViewCell {
    @IBOutlet weak var albumCoverImageView: UIImageView!
    @IBOutlet weak var albumTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateCellWith(_ album:Album) {
        if let url = URL(string: (album.albumCover.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")) {
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            self.albumCoverImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
        self.albumTitleLabel.text = album.albumTitle.uppercased()
    }
    
}
