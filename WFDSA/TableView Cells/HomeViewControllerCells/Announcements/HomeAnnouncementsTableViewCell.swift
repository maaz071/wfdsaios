//
//  HomeAnnouncementsTableViewCell.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class HomeAnnouncementsTableViewCell: BaseTableViewCell{
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var foregroundView: UIView!
    
    @IBOutlet weak var viewAllButton: UIButton!
    
    @IBOutlet weak var AnnouncementImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailLabel: UITextView!
    
    weak var delegate:ViewAllCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.foregroundView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.foregroundView.layer.borderWidth = 1
        self.foregroundView.layer.cornerRadius = 3
        self.foregroundView.clipsToBounds = true
        //        self.foregroundView.dropShadow()
        
        self.backView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.backView.layer.borderWidth = 1
        self.backView.layer.cornerRadius = 3
        self.backView.clipsToBounds = true
        //                self.backView.dropShadow()
        
        self.viewAllButton.backgroundColor = AppColors.appThemeColor
        self.viewAllButton.layer.cornerRadius = 3
        self.viewAllButton.clipsToBounds = true
        self.viewAllButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
    }
    
    func populateDataFromAnnouncement(_ announcement:Announcement?) {
        self.nameLabel.text = announcement?.title ?? ""
        self.dateLabel.text = ""
        let dateForm = DateFormatter()
        dateForm.dateFormat = "MM-dd-yyyy HH:mm:ss"
        if let date = dateForm.date(from: (announcement?.date ?? "")) {
            let newdateForm = DateFormatter()
            newdateForm.dateFormat = "dd-MMMM-yyyy HH:mm:ss"
            self.dateLabel.text = newdateForm.string(from: date)
        }else {
            dateForm.dateFormat = "MM-dd-yyyy HH:mm"
            if let date = dateForm.date(from: (announcement?.date ?? "")) {
                let newdateForm = DateFormatter()
                newdateForm.dateFormat = "dd-MMMM-yyyy HH:mm:ss"
                self.dateLabel.text = newdateForm.string(from: date)
            }
        }
        self.detailLabel.text = announcement?.announcementMessage ?? ""
        
        if let url = URL(string: (announcement?.uploadImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")) {
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            self.AnnouncementImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true) { (image) in
                self.AnnouncementImageView.contentMode = self.AnnouncementImageView.image == placeholderImage ? .scaleAspectFit : .scaleAspectFill
            }
        }
    }

    @IBAction func viewAllButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapOnViewAll(type: .announcement)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = self.viewAllButton.backgroundColor
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.viewAllButton.backgroundColor = color
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = self.viewAllButton.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        
        if highlighted {
            self.viewAllButton.backgroundColor = color
        }
    }

    
}
