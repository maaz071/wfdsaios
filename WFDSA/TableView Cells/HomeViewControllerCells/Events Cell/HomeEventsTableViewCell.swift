//
//  HomeEventsTableViewCell.swift
//  WFDSA
//
//  Created by Umer Jabbar on 27/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class HomeEventsTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var foregroundView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var dateBackgroundView: UIView!
    
    @IBOutlet weak var lockImageView: UIImageView!
    @IBOutlet weak var viewAllButton: UIButton!
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    weak var delegate:ViewAllCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.foregroundView?.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.foregroundView?.layer.borderWidth = 1
        self.foregroundView?.layer.cornerRadius = 3
        self.foregroundView?.clipsToBounds = true
        
        self.emptyView?.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.emptyView?.layer.borderWidth = 1
        self.emptyView?.layer.cornerRadius = 3
        self.emptyView?.clipsToBounds = true
//        self.foregroundView.dropShadow()
        
        self.backView?.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.backView?.layer.borderWidth = 1
        self.backView?.layer.cornerRadius = 3
        self.backView?.clipsToBounds = true
//                self.backView.dropShadow()
        
        self.viewAllButton.backgroundColor = AppColors.appThemeColor
        self.viewAllButton.layer.cornerRadius = 3
        self.viewAllButton.clipsToBounds = true
        self.viewAllButton?.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
    }
    
    func populateDataFromEvent(event:Event?) {
        
        self.foregroundView.isHidden = (event == nil)
        self.emptyView.isHidden = !self.foregroundView.isHidden
        
        self.titleLabel.text = event?.title ?? ""
        self.dateTimeLabel.text = event?.startDate ?? ""
        self.locationLabel.text = event?.venueAddress ?? ""
        self.dateLabel.text = ""
        self.monthLabel.text = ""
        
        if (event?.permission.contains("Public") ?? false)! {
            self.lockImageView.isHidden = true
        }else {
            var doesEventHasUsersRole = false
            _ = AppStateManager.sharedInstance.loggedInUser.role.components(separatedBy: ",").map {
                if (event?.permission.contains($0) ?? false)! {
                    doesEventHasUsersRole = true
                }
            }
            self.lockImageView.isHidden = doesEventHasUsersRole
        }

        let dateForm = DateFormatter()
        dateForm.dateFormat = "MM-dd-yyyy HH:mm"
        if let date = dateForm.date(from: (event?.startDate ?? "")) {
            let newdateForm = DateFormatter()
            newdateForm.dateFormat = "dd-yyyy"
            self.dateLabel.text = newdateForm.string(from: date)
            newdateForm.dateFormat = "MMM"
            self.monthLabel.text = newdateForm.string(from: date)
            newdateForm.dateFormat = "dd-MMM-yyyy HH:mm"
            self.dateTimeLabel.text = newdateForm.string(from: date)
        }
    }
    
    @IBAction func viewAllButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapOnViewAll(type: .event)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = self.dateBackgroundView.backgroundColor
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.dateBackgroundView.backgroundColor = color
            self.viewAllButton.backgroundColor = color
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = self.dateBackgroundView.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        
        if highlighted {
            self.dateBackgroundView.backgroundColor = color
            self.viewAllButton.backgroundColor = color

        }
    }

}
