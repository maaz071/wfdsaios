//
//  HomeResourcesTableViewCell.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

protocol HomeResourcesTableViewCellDelegate : class {
    func resourceTappedAt(index: Int)
}


class HomeResourcesTableViewCell: BaseTableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var foregroundView: UIView!
    
    @IBOutlet weak var fileImage: UIImageView!
    @IBOutlet weak var fileImage2: UIImageView!
    @IBOutlet weak var fileImage3: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameLabel2: UILabel!
    @IBOutlet weak var nameLabel3: UILabel!
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    
    
    @IBOutlet weak var viewAllButton: UIButton!
    
    weak var delegate:ViewAllCellDelegate?
    weak var tapdelegate:HomeResourcesTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        self.foregroundView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.foregroundView.layer.borderWidth = 1
        self.foregroundView.layer.cornerRadius = 3
        self.foregroundView.clipsToBounds = true
        //        self.foregroundView.dropShadow()
        
        self.backView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.backView.layer.borderWidth = 1
        self.backView.layer.cornerRadius = 3
        self.backView.clipsToBounds = true
        //                self.backView.dropShadow()
        
        self.viewAllButton.backgroundColor = AppColors.appThemeColor
        self.viewAllButton.layer.cornerRadius = 3
        self.viewAllButton.clipsToBounds = true
        self.viewAllButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleFirstViewTap))
        self.firstView.addGestureRecognizer(tap)
        
        let secondTap = UITapGestureRecognizer(target: self, action: #selector(self.handleSecondViewTap))
        self.secondView.addGestureRecognizer(secondTap)

        let thirdTap = UITapGestureRecognizer(target: self, action: #selector(self.handleThirdViewTap))
        self.thirdView.addGestureRecognizer(thirdTap)
        
    }
    
    @objc func handleFirstViewTap() {
        self.tapdelegate?.resourceTappedAt(index: 0)
    }
    
    @objc func handleSecondViewTap() {
        self.tapdelegate?.resourceTappedAt(index: 1)
    }

    @objc func handleThirdViewTap() {
        self.tapdelegate?.resourceTappedAt(index: 2)
    }
    
    func populateDataWithResourcesArray(_ resourcesArray:[Resource]) {
        if resourcesArray.indices.contains(0) {
            self.nameLabel.text = resourcesArray[0].title2
            self.fileImage.image = self.getFileImage(urlString: (resourcesArray[0].uploadFile ?? ""))
        }
        
        if resourcesArray.indices.contains(1) {
            self.nameLabel2.text = resourcesArray[1].title2
            self.fileImage2.image = self.getFileImage(urlString: (resourcesArray[1].uploadFile ?? ""))
        }
        
        if resourcesArray.indices.contains(2) {
            self.nameLabel3.text = resourcesArray[2].title2
            self.fileImage3.image = self.getFileImage(urlString: (resourcesArray[2].uploadFile ?? ""))
        }
    }
    
    func getFileImage(urlString:String) -> UIImage {
        
        if urlString.contains("pdf") {
            return #imageLiteral(resourceName: "pdf-icon")
        }else if urlString.contains("doc") || urlString.contains("docx") {
            return #imageLiteral(resourceName: "word-icon")
        }else if urlString.contains("xls") || urlString.contains("xlsx") {
            return #imageLiteral(resourceName: "excel-icon")
        }else if urlString.contains("ppt") || urlString.contains("pptx") {
            return #imageLiteral(resourceName: "ppt-icon")
        }else if urlString.contains("txt") {
            return #imageLiteral(resourceName: "txt-icon")
        }else {
            return #imageLiteral(resourceName: "other-icon")
        }
    }
    
    @IBAction func viewAllButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapOnViewAll(type: .resources)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = self.viewAllButton.backgroundColor
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.viewAllButton.backgroundColor = color
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = self.viewAllButton.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        
        if highlighted {
            self.viewAllButton.backgroundColor = color
        }
    }

    
}
