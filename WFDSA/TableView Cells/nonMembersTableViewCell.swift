//
//  nonMembersTableViewCell.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 04/10/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class nonMembersTableViewCell: BaseTableViewCell {

    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var countryImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.backView.layer.borderWidth = 1
        self.backView.layer.cornerRadius = 3
        
    }
    
    
    func populateDataFromEvent(member : DsaMember){
        
        if let url = URL(string: member.companyLogo ?? "") {
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            self.companyImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
        
        if let url = URL(string: member.flagPic ?? "") {
            let placeholderImage = #imageLiteral(resourceName: "map")
            self.countryImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
        
        self.nameLabel.text = member.companyName
        self.addressLabel.text = member.address
        self.webLabel.text = member.website
        self.countryLabel.text = member.country
    }
    
}
