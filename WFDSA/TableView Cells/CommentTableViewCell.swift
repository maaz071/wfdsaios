//
//  CommentTableViewCell.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 8/2/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class CommentTableViewCell: BaseTableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.backView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.backView.layer.borderWidth = 1
        self.backView.layer.cornerRadius = 3
        self.backView.clipsToBounds = true
        
    }
    
    func populateData(_ comment : Comment){
        self.nameLabel.text = comment.name
        self.roleLabel.text = comment.post
        self.commentLabel.text = comment.message
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        
        if let commentDate = dateFormatter.date(from: (comment.date + " " + comment.time)) {
            dateFormatter.dateFormat = "dd-MMM-yyyy\nhh:mm a"
            self.dateLabel.text = dateFormatter.string(from: commentDate)
        }else {
            self.dateLabel.text = "\(comment.time ?? "")\n\(comment.date ?? "")"
        }
        
        if let url = URL(string: comment.imageURL ?? "") {
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            self.profileImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
    }
    
    
    
}
