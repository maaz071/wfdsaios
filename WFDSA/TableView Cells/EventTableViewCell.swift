//
//  EventTableViewCell.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class EventTableViewCell: BaseTableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var foregroundView: UIView!
    
    @IBOutlet weak var lockImageView: UIImageView!
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    
    open class func height() -> CGFloat {
        return CGFloat(120) * Utility.scaleFactor()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.backView.layer.borderWidth = 1
        self.backView.layer.cornerRadius = 3
        
        self.foregroundView.layer.cornerRadius = 3
        
    }
    
    fileprivate func setViewsToDate(_ date: Date) {
        let newdateForm = DateFormatter()
        newdateForm.dateFormat = "dd-MMM-yyyy HH:mm"
        self.dateTimeLabel.text = newdateForm.string(from: date)
        newdateForm.dateFormat = "dd-yyyy"
        self.dateLabel.text = newdateForm.string(from: date)
        newdateForm.dateFormat = "MMM"
        self.monthLabel.text = newdateForm.string(from: date)
    }
    
    func populateDataFromEvent(event:Event?) {
        
        self.titleLabel.text = event?.title ?? ""
        self.dateTimeLabel.text = event?.startDate ?? ""
        self.locationLabel.text = event?.venueAddress ?? ""
        
        if (event?.permission.contains("Public") ?? false)! {
            self.lockImageView.isHidden = true
        }else {
            var doesEventHasUsersRole = false
            _ = AppStateManager.sharedInstance.loggedInUser.role.components(separatedBy: ",").map {
                if (event?.permission.contains($0) ?? false)! {
                    doesEventHasUsersRole = true
                }
            }
            self.lockImageView.isHidden = doesEventHasUsersRole
        }
        
        self.dateLabel.text = ""
        self.monthLabel.text = ""
        
        let dateForm = DateFormatter()
        dateForm.dateFormat = "MM-dd-yyyy HH:mm"
        
        if let date = dateForm.date(from: (event?.startDate ?? "")) {
            setViewsToDate(date)
        }else {
            dateForm.dateFormat = "MM-dd-yyyy HH:mm:ss"
            if let date = dateForm.date(from: (event?.startDate ?? "")) {
                setViewsToDate(date)
            }
        }
    }

}
