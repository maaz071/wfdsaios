//
//  EventMembersTableViewCell.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 8/1/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class EventMembersTableViewCell: BaseTableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.backView.layer.borderWidth = 1
        self.backView.layer.cornerRadius = 3
        self.backView.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
