//
//  CommitteeTableViewCell.swift
//  WFDSA
//
//  Created by Umer Jabbar on 29/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import AlamofireImage

class CommitteeTableViewCell: BaseTableViewCell {

    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var countryImageView: UIImageView!
    
    @IBOutlet weak var mainImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var designationLabel2: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var countryNameLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var faxLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
    }
    
    func populateData(member : RoleMember){
        
        if let url = URL(string: member.uploadImage ?? "") {
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            self.mainImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
        
        if let url = URL(string: member.flagPic ?? "") {
            let placeholderImage = #imageLiteral(resourceName: "map")
            self.countryImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
        
        self.nameLabel.text = member.company
        self.addressLabel.text = member.address
        self.titleLabel.text = "\(member.title ?? "") \(member.memberName ?? "")"
        self.phoneLabel.text = member.telephone
        self.faxLabel.text = member.fax
        self.emailLabel.text = member.email
        self.addressLabel.text = member.address
        self.countryNameLabel.text = member.name
        self.designationLabel2.text = member.designation
        self.designationLabel.text = member.wfdsaTitle
        
        
    }


}
