//
//  AnnouncementsTableViewCell.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class AnnouncementsTableViewCell: BaseTableViewCell {
    
     @IBOutlet weak var foregroundView: UIView!
    
    @IBOutlet weak var AnnouncementImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailLabel: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.foregroundView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.foregroundView.layer.borderWidth = 1
        self.foregroundView.layer.cornerRadius = 3

        
    }
    
    func populateCell(announcement : Announcement){
        if let url = URL(string: (announcement.uploadImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")) {
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            self.AnnouncementImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
        
        self.nameLabel.text = announcement.title
        self.dateLabel.text = announcement.date
        
        let dateForm = DateFormatter()
        dateForm.dateFormat = "MM-dd-yyyy HH:mm"
        if let date = dateForm.date(from: (announcement.date ?? "")) {
            
            dateForm.dateFormat = "dd-MMM-yyyy HH:mm"
            self.dateLabel.text = dateForm.string(from: date)
        }else {
            // in case of seconds in date string
            dateForm.dateFormat = "MM-dd-yyyy HH:mm:ss"
            if let date = dateForm.date(from: (announcement.date ?? "")) {
                
                dateForm.dateFormat = "dd-MMM-yyyy HH:mm"
                self.dateLabel.text = dateForm.string(from: date)
            }
        }

        self.detailLabel.text = announcement.announcementMessage
    }



}
