//
//  InvoicePaidTableViewCell.swift
//  WFDSA
//
//  Created by Umer Jabbar on 31/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class InvoicePaidTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
