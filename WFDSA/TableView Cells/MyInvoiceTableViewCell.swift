//
//  MyInvoiceTableViewCell.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/31/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

protocol MyInvoicesPaymentViewCellDelegate : class {
    func didTapOnPayButton(index:Int)
}

class MyInvoiceTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var payButton: UIButton!
    weak var delegate:MyInvoicesPaymentViewCellDelegate?
    
    var index = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.borderView.layer.cornerRadius = 3
        self.borderView.layer.borderColor = UIColor.black.cgColor
        self.borderView.layer.borderWidth = 0
    }
    
    open class func height() -> CGFloat {
        return CGFloat(60) * Utility.scaleFactor()
    }
    
    func populateDataFromUserGuide(userGuide:Invoice) {
        self.nameLabel.text = userGuide.title.capitalized
        if userGuide.paymentStatus == "1" {
            self.payButton.setTitle("Paid", for: .normal)
            self.payButton.isEnabled = false
        }else {
            self.payButton.setTitle("Pay Now\n $\(userGuide.grandTotal ?? "")", for: .normal)
            self.payButton.titleLabel?.numberOfLines = 2
            self.payButton.isEnabled = true
        }
    }
    
    @IBAction func payButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapOnPayButton(index: self.index)
    }
}
