//
//  EventMembersViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 8/1/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON

class EventMembersViewController: BaseController {
    
    var eventId:String = ""
    var eventMembers = [EventMember]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        
        
        
        self.getEventMembers()
    }
    
    func getEventMembers() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ EventMember(fromJson: $0)})
                self.eventMembers = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getEventMembers(parameters: ["event_id": "\(self.eventId)"], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    
    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
//        self.dismiss(animated: true)
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}


extension EventMembersViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.eventMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventMembersTableViewCell", for: indexPath) as! EventMembersTableViewCell
        
        cell.nameLabel.text = "\(self.eventMembers[indexPath.row].firstName ?? "" ) \(self.eventMembers[indexPath.row].lastName ?? "" )"
        
        if let url = URL(string: self.eventMembers[indexPath.row].uploadImage ?? "") {
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            cell.profileImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }

        return cell
    }
}
