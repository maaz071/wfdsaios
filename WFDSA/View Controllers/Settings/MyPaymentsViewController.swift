//
//  MyPaymentsViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/31/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift
import AVFoundation

class MyPaymentsViewController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    var myPaymentsArray = [Payment]()
    var invoicesVC:MyInvoicesViewController? = nil //MyInvoicesViewController.instantiate(fromAppStoryboard: .Settings)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = .init(top: 0, left: 0, bottom: 10, right: 0)
        self.getMyPayments()
        self.title = "My Payments"
    }
    
    func getMyPayments() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["payment_data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Payment(fromJson: $0)})
                self.myPaymentsArray = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getMyPayments(parameters: ["signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType ?? "2")!, "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "")], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    
    //MARK:- IBACtions
    
    fileprivate func selectPaymentTab() {
        
    }
    
    @IBAction func paymentButtonTapped(_ sender: UIButton) {
        selectPaymentTab()
    }
    
    @IBAction func paymentsTabViewTapped(_ sender: UITapGestureRecognizer) {
        selectPaymentTab()
    }
    
    
    fileprivate func selectInvoicesTab() {
        if (self.invoicesVC == nil) {
            self.invoicesVC = MyInvoicesViewController.instantiate(fromAppStoryboard: .Settings)
        }
        self.navigationController?.hero.navigationAnimationType = .slide(direction: .left)
        self.invoicesVC?.paymentsVC = self;
        self.hero.replaceViewController(with: self.invoicesVC!)
    }
    
    @IBAction func invoiceButtonTapped(_ sender: UIButton) {
        selectInvoicesTab()
    }
    @IBAction func invoicesTabViewTapped(_ sender: UITapGestureRecognizer) {
        selectInvoicesTab()
    }
}

extension MyPaymentsViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myPaymentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyPaymentTableViewCell") as! MyPaymentTableViewCell
        cell.index = indexPath.row
        cell.populateDataFrom(payment: self.myPaymentsArray[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension MyPaymentsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MyInvoiceTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return MyInvoiceTableViewCell.height()
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = PaymentINvoiceDetalViewController.instantiate(fromAppStoryboard: .Settings)
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.amount = self.myPaymentsArray[indexPath.row].paymentAmount
        vc.eventName = self.myPaymentsArray[indexPath.row].title
        vc.paymentDate = self.myPaymentsArray[indexPath.row].paymentDate
        self.slideMenuController()?.present(vc, animated: true)
    }
}

extension MyPaymentsViewController : MyInvoicesPaymentViewCellDelegate {
    func didTapOnPayButton(index: Int) {
        
    }
}

