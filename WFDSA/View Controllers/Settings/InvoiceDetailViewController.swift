//
//  InvoiceDetailViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 31/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//
import UIKit
import SwiftyJSON


class InvoiceDetailViewController: BaseController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var paynowButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    
    var amount:String = "0"
    var myInvoiceDetail : InvoiceDetail?
    var invoice : Invoice?
    var invoiceId = "0"
    
    weak var delegate:MyInvoicesPaymentViewCellDelegate?
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.getMyInvoices()
        
        if (self.invoice?.paymentStatus == "1") {
            self.paynowButtonWidthConstraint.constant = 0;
            self.updateViewConstraints()
        }else {
            self.backButton.setTitle("LATER", for: .normal)
        }
        self.view.dropShadow()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tableViewHeightConstraint.constant = min(self.tableView.contentSize.height, UIScreen.main.bounds.size.height*0.5)
        self.tableView.isScrollEnabled = (self.tableView.contentSize.height > UIScreen.main.bounds.size.height*0.5)
    }
    
    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
//        self.dismiss(animated: true)
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func paynowButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.didTapOnPayButton(index: self.index)
        }
    }
    
    func getMyInvoices() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [String : AnyObject] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let json = JSON(rawValue: responseData) {
                let dataObject = InvoiceDetail(fromJson: json)
                self.myInvoiceDetail = dataObject
                self.totalAmountLabel.text = "\(self.myInvoiceDetail?.amount ?? "")"
                self.titleLabel.text = self.myInvoiceDetail?.title.uppercased() ?? ""
                self.nameLabel.text = self.myInvoiceDetail?.name.capitalized ?? ""
                
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getInvoicesDetails(parameters: ["invoice_id": "\(self.invoiceId)"], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension InvoiceDetailViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myInvoiceDetail?.item.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoicePaidTableViewCell", for: indexPath) as! InvoicePaidTableViewCell
        
        cell.nameLabel.text = self.myInvoiceDetail?.item[indexPath.row].name
        cell.priceLabel.text = self.myInvoiceDetail?.item[indexPath.row].amount
        cell.unitLabel.text = self.myInvoiceDetail?.item[indexPath.row].quantity
        return cell
    }
    
}

extension InvoiceDetailViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40 * Utility.scaleFactor()
    }
}
