//
//  PaymentINvoiceDetalViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/31/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class PaymentINvoiceDetalViewController: BaseController {

    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    var amount:String = "0"
    var eventName:String = ""
    var paymentDate:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.totalAmountLabel.text = "Amount : \(amount)"
        self.eventNameLabel.text = self.eventName
        self.paymentLabel.text = self.paymentDate
    }
    
    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
//        self.dismiss(animated: true)
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
}
