//
//  AboutViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class AboutViewController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    var headerView: UIView!
    
    private let kTableHeaderHeight: CGFloat = UIScreen.main.bounds.height*0.9
    var aboutUs:AboutUS?
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var bottomLabel: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = .init(top: 0, left: 0, bottom: 30, right: 0)
        self.title = "ABOUT US"
//        self.setupTableView()
        self.tableView.hero.modifiers = [.cascade]
        for cell in tableView.subviews {
            cell.hero.modifiers = [.fade, .translate(CGPoint(x: 0, y: 200*(Utility.scaleFactor())))]
        }
        self.tableView.delegate = self
        self.getAboutUsData()
        self.descriptionLabel.text = ""
    }
    
    func setupTableView() {
        
        headerView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.addSubview(headerView)
        
        tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0 )
        tableView.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        updateHeaderView()
    }
    
    
    func updateHeaderView() {
        
        var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableView.bounds.width, height: kTableHeaderHeight)
        if tableView.contentOffset.y < -kTableHeaderHeight {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
        }
        
        headerView.frame = headerRect
        self.view.layoutIfNeeded()
    }
    
    func populateFields() {
        self.descriptionLabel.text = (self.aboutUs?.aboutText ?? "")!
        if let url = URL(string: (self.aboutUs?.uploadPic ?? "")) {
            self.coverImageView.af_setImage(withURL: url, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
    }
    
    func getAboutUsData() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            self.stopLoading()
            print(result)
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ AboutUS(fromJson: $0)})
                self.aboutUs = dataArray.first
                self.populateFields()
            }
        }
        
        APIManager.sharedInstance.getAboutUs(parameters: [:], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension AboutViewController : UITableViewDelegate, UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        updateHeaderView()
    }
}

