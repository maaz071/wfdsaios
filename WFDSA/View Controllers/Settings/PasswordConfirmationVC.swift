//
//  PasswordConfirmationVC.swift
//  
//
//  Created by Ali Abdul Jabbar on 09/08/2018.
//

import UIKit
import AnimatedTextInput
import IQKeyboardManagerSwift
import SwiftValidator

protocol PasswordConfirmationVCDelegate : class {
    func didSuccessfullyComfirmed()
}

class PasswordConfirmationVC: BaseController {
    
    @IBOutlet weak var passwordField: AnimatedTextInput!
    let validator = Validator()
    weak var delegate:PasswordConfirmationVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passwordField.delegate = self
        self.passwordField.type = .password(toggleable: true)
        self.passwordField.placeHolderText = "Password"
        self.passwordField.style = AppTextInputStyle(lineHeight: 1.5*Utility.scaleFactor(), yPlaceholderPositionOffset: 0.0, textAttributes: [:])
    }
    
    func startConfirmation() {
        IQKeyboardManager.sharedManager().resignFirstResponder()
        self.resetValidators()
        validator.validate(self)
    }
    
    func setupValidators(){
        validator.registerField(self.passwordField,errorLabel: nil,rules: [RequiredRule(),MinLengthRule(length: 6)])
    }
    
    func resetValidators(){
        self.passwordField.clearError()
    }
    
    func processConfirmation() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            self.delegate?.didSuccessfullyComfirmed()
            self.dismiss(animated: true)
        }
        
        var parameters = [String:String]()
        parameters["password"] = self.passwordField.text
        parameters["signin_type"] = (AppStateManager.sharedInstance.loggedInUser.signinType == "" ? "2" : AppStateManager.sharedInstance.loggedInUser.signinType)!
        parameters["member_id"] = (AppStateManager.sharedInstance.loggedInUser.id )!
        
        APIManager.sharedInstance.passwordConfirmation(parameters: parameters, success: successClosure) { (error) in
            self.stopLoading()
            if error.localizedFailureReason != nil {
                self.passwordField.show(error: error.localizedFailureReason!, placeholderText: self.passwordField.placeHolderText)
            }else {
                self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            }
        }
    }
    
    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
        //        self.dismiss(animated: true)
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true)
    }

}

extension PasswordConfirmationVC : ValidationDelegate {
    
    func validationSuccessful() {
        
        if (CEReachabilityManager.isReachable()){
            processConfirmation()
        }else{
            IQKeyboardManager.sharedManager().resignFirstResponder()
            self.showErrorWith(message: "Please check your internet connection")
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        
        for (field, error) in errors {
            if let field = field as? AnimatedTextInput {
                field.show(error: error.errorMessage, placeholderText: field.placeHolderText)
            }
        }
    }
}

extension PasswordConfirmationVC : AnimatedTextInputDelegate {
    func animatedTextInputShouldReturn(animatedTextInput: AnimatedTextInput) -> Bool {
        IQKeyboardManager.sharedManager().resignFirstResponder()
        return false
    }
    
    func animatedTextInputDidEndEditing(animatedTextInput: AnimatedTextInput) {
        startConfirmation()
    }
    
}
