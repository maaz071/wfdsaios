//
//  ProfileViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/28/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Gallery
import Lightbox
import RealmSwift
import AlamofireImage
import SwiftyJSON

class ProfileViewController: BaseController {
    
    @IBOutlet var imageTapGesture: UITapGestureRecognizer!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var phoneNumberField: UITextField!
    
    lazy var editBarButtonItem:UIBarButtonItem = {
        return UIBarButtonItem(title: "EDIT", style: .plain, target: self, action: #selector(editButtonTapped))
    }()
    
    lazy var saveBarButtonItem:UIBarButtonItem = {
        return UIBarButtonItem(title: "SAVE", style: .plain, target: self, action: #selector(saveButtonTapped))
    }()
    var gallery: GalleryController!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Profile"
        self.navigationItem.rightBarButtonItem = editBarButtonItem
        self.setFields(interactive: false)
        self.fillupFields()
        self.setupImageView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2
        self.borderView.layer.cornerRadius = self.borderView.frame.size.width/2
    }
    
    @objc func editButtonTapped() {
        
//        guard AppStateManager.sharedInstance.loggedInUser.signinType == "2" else {
        
            let vc = PasswordConfirmationVC.instantiate(fromAppStoryboard: .Settings)
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.delegate = self
            self.slideMenuController()?.present(vc, animated: true)
            return
//        }

//        self.setFields(interactive: true)
//        self.navigationItem.rightBarButtonItem = saveBarButtonItem
    }
    
    @objc func saveButtonTapped() {
        IQKeyboardManager.sharedManager().resignFirstResponder()
        self.setFields(interactive: false)
        self.processSubmit()
        self.navigationItem.rightBarButtonItem = editBarButtonItem
    }
    
    func setupImageView() {
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2
        self.borderView.layer.cornerRadius = self.borderView.frame.size.width/2
        self.borderView.layer.borderColor = UIColor.black.cgColor
        self.borderView.layer.borderWidth = 2
        self.profileImageView.contentMode = .scaleAspectFill
        self.profileImageView.clipsToBounds = true
    }
    
    func fillupFields() {
        self.firstNameField.text = AppStateManager.sharedInstance.loggedInUser.firstName
        self.lastNameField.text = AppStateManager.sharedInstance.loggedInUser.lastName
        self.emailField.text = AppStateManager.sharedInstance.loggedInUser.email
        self.phoneNumberField.text = AppStateManager.sharedInstance.loggedInUser.contactNo
        if let url = URL(string: (AppStateManager.sharedInstance.loggedInUser.uploadImage ?? "")) {
            let placeholderImage = #imageLiteral(resourceName: "ic_profile_pic") 
            self.profileImageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
    }
    
    func setFields(interactive:Bool) {
        self.firstNameField.isUserInteractionEnabled = interactive
        self.lastNameField.isUserInteractionEnabled = interactive
        self.phoneNumberField.isUserInteractionEnabled = interactive
        self.imageTapGesture.isEnabled = interactive
        if interactive {
            self.firstNameField.becomeFirstResponder()
        }
    }
    
    @IBAction func ProfilePictureTapped(_ sender: UITapGestureRecognizer) {
        
//        guard AppStateManager.sharedInstance.loggedInUser.signinType == "1" else {
//            return
//        }
        
        gallery = GalleryController()
        Config.Camera.recordLocation = false
        Config.Camera.imageLimit = 1
        Config.tabsToShow = [.imageTab, .cameraTab]
        gallery.delegate = self
        self.slideMenuController()?.present(gallery, animated: true, completion: nil)
    }
    
    @IBAction func signoutButtonTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to Signout?", preferredStyle: .alert)
        
        let actionYes = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            self.processLogout()
        })
        
        let actionNo = UIAlertAction(title: "NO", style: .cancel, handler: { (action) in
        })
        
        alertController.addAction(actionYes)
        alertController.addAction(actionNo)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func changeRootViewController() {
        if !AppStateManager.sharedInstance.isUserLoggedIn() {
            let vc = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "LoginViewControllerNav_ID")
            self.navigationController?.hero.replaceViewController(with: vc)
        }
        else {
            let homeVC:HomeViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            let homeNavVC:BaseNavigationController = BaseNavigationController(rootViewController: homeVC)
            let sideMenuController:SideMenuController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
            let slideMenuController = SlideMenuController(mainViewController: homeNavVC, leftMenuViewController: sideMenuController, rightMenuViewController: UIViewController())
            
            self.navigationController?.hero.replaceViewController(with: slideMenuController)
        }
    }
    
    func processLogout() {
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            self.stopLoading()
            AppStateManager.sharedInstance.logOutUser()
            self.changeRootViewController()
            self.showSuccessWithMessage("Logged Out Successfully!", layout: .statusLine)
        }
        
        let parameters: [String: String] = ["signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType)!, "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "")]
        
        APIManager.sharedInstance.logout(parameters: parameters, success: successClosure){ (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func processSubmit() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }

        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            let json  = JSON(responseData)
            let dataArray = json.arrayValue.map({ User(fromJson: $0)})
            let userObject = dataArray.first
            let realm = try! Realm()
            
            try! realm.write(){
                AppStateManager.sharedInstance.loggedInUser.contactNo = self.phoneNumberField.text!
                AppStateManager.sharedInstance.loggedInUser.firstName = self.firstNameField.text!
                AppStateManager.sharedInstance.loggedInUser.lastName = self.lastNameField.text!
                if let imageURL = userObject?.uploadImage {
                    AppStateManager.sharedInstance.loggedInUser.uploadImage = imageURL
                }
                realm.add(AppStateManager.sharedInstance.loggedInUser, update: true)
            }
            self.showSuccessWithMessage("Profile Updated Successfully!", layout: .statusLine)
        }
        
        var parameters: [String: AnyObject] = [
            "member_id":String(AppStateManager.sharedInstance.loggedInUser.id) as AnyObject,
            "first_name":self.firstNameField.text! as AnyObject,
            "last_name": self.lastNameField.text! as AnyObject,
            "cell":self.phoneNumberField.text! as AnyObject,
            "type":(AppStateManager.sharedInstance.loggedInUser.signinType ?? "2") as AnyObject
            ]
        
            if self.profileImageView.image != nil && self.profileImageView.image != Constants.PLACEHOLDER_USER {
                let imageData = UIImageJPEGRepresentation(self.profileImageView.image!, 0.4)! as NSData as AnyObject
                parameters["image"] = imageData.base64EncodedString(options: .lineLength64Characters) as AnyObject
            }
        
        APIManager.sharedInstance.updateUserInfo(parameters: parameters, success: successClosure){ (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension ProfileViewController : PasswordConfirmationVCDelegate {
    func didSuccessfullyComfirmed() {
        self.setFields(interactive: true)
        self.navigationItem.rightBarButtonItem = saveBarButtonItem
    }
}

extension ProfileViewController : GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true)
    }
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true)
        images.first?.resolve(completion: { (image) in
            
            let controller = CropViewController()
            controller.delegate = self
            controller.image = image
            
            let navController = UINavigationController(rootViewController: controller)
            self.present(navController, animated: true, completion: nil)
        })
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) {
        self.startLoading()
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self?.stopLoading()
            self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
        })
    }
    
    func showLightbox(images: [UIImage]) {
        guard images.count > 0 else {
            return
        }
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.dismissalDelegate = self
        lightbox.dynamicBackground = true
        gallery.present(lightbox, animated: true, completion: nil)
    }
}

extension ProfileViewController : CropViewControllerDelegate {
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
        self.profileImageView.image = image
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        controller.dismiss(animated: true, completion: nil)
        self.profileImageView.image = image
    }
}

extension ProfileViewController : LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) { }
}
/*
extension GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        
    }
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {}
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {}
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true)
    }
}
*/



