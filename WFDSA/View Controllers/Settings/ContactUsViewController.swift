//
//  contactUsViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import MapKit
import CoreLocation
import Localide

enum ContactUsInquiryType : Int {
    case membership = 0
    case general
}

class ContactUsViewController: BaseController {

    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var dropDownLabel: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    
    var selectedInquiryType:ContactUsInquiryType!
    var dropDown:DropDown!
    
    @IBOutlet weak var mapViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    
    var contactUs:ContactUS?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.messageButton.imageView?.contentMode = .scaleAspectFill
        self.phoneButton.imageView?.contentMode = .scaleAspectFill
        self.locationButton.imageView?.contentMode = .scaleAspectFill
        
        self.messageButton.backgroundColor = AppColors.appThemeSecondaryColor
        self.phoneButton.backgroundColor = AppColors.appThemeColor
        self.locationButton.backgroundColor = AppColors.appThemeColor
        
        self.textView.layer.cornerRadius = 3
        self.textView.layer.borderColor = AppColors.appThemeSecondaryColor.cgColor
        self.textView.layer.borderWidth = 1
        
        self.selectedInquiryType = .membership
        
        self.dropDown = DropDown(anchorView: self.dropDownView)
        dropDown.dataSource = ["Membership Inquiry", "General Inquiry"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.dropDownLabel.text = item
            self.selectedInquiryType = ContactUsInquiryType(rawValue: index)
        }
        
        self.mapViewLeadingConstraint.constant = -UIScreen.main.bounds.width
        self.mapView.delegate = self
        getContactUsData()
    }

    func getContactUsData() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            self.stopLoading()
            print(result)
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ ContactUS(fromJson: $0)})
                self.contactUs = dataArray.first
                self.populateFields()
            }
        }
        
        APIManager.sharedInstance.getContactUs(parameters: [:], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func populateFields() {
        self.addressLabel.text = self.contactUs?.address
        let lat = CLLocationDegrees(Double((self.contactUs?.lat ?? ""))!)
        let lng = CLLocationDegrees(Double((self.contactUs?.lng ?? ""))!)
        
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = "WFDSA"
        annotation.subtitle = "World Federation Of Direct Selling Associations"
        
        self.mapView.addAnnotation(annotation)
    }
    
    func showMapView() {
        self.mapViewLeadingConstraint.constant = 0
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        self.mapView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)

    }
    
    func hideMapView() {
        self.mapViewLeadingConstraint.constant = -UIScreen.main.bounds.width
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func messageButtonTapped(_ sender: UIButton) {
        self.hideMapView()
        self.messageButton.backgroundColor = AppColors.appThemeSecondaryColor
        self.phoneButton.backgroundColor = AppColors.appThemeColor
        self.locationButton.backgroundColor = AppColors.appThemeColor
    }
    
    @IBAction func phoneButtonTapped(_ sender: UIButton) {
        guard let number = URL(string: "tel://\(String(describing: (self.contactUs?.contactNo ?? "")!))") else {
            self.showErrorBar(message: "Cannot make this call at the moment")
            return
        }
        UIApplication.shared.open(number)
    }
    
    @IBAction func locationButtonTapped(_ sender: UIButton) {
        self.showMapView()
        self.messageButton.backgroundColor = AppColors.appThemeColor
        self.phoneButton.backgroundColor = AppColors.appThemeColor
        self.locationButton.backgroundColor = AppColors.appThemeSecondaryColor
    }
    
    @IBAction func dropdownTapped(_ sender: UITapGestureRecognizer) {
        self.dropDown.show()
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        
        guard textView.text != "" else {
            self.showErrorWith(message: "Please Write a Message First")
            return
        }
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            self.stopLoading()
            print(result)
            self.showSuccessWithMessage("Message Send To WFDSA", layout: .statusLine)
            self.textView.text = ""
        }
        
        APIManager.sharedInstance.submitContactUs(parameters: ["frm_name":(AppStateManager.sharedInstance.loggedInUser.firstName + AppStateManager.sharedInstance.loggedInUser.lastName), "frm_email":AppStateManager.sharedInstance.loggedInUser.email, "message":textView.text], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension ContactUsViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            return nil;
        }else{
            let pinIdent = "Pin";
            var pinView: MKPinAnnotationView;
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation;
                pinView = dequeuedView;
                let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                        size: CGSize(width: 20, height: 20)))
                mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControlState())
                pinView.rightCalloutAccessoryView = mapsButton
                pinView.canShowCallout = true
            }else{
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: pinIdent);
                let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                        size: CGSize(width: 20, height: 20)))
                mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControlState())
                pinView.rightCalloutAccessoryView = mapsButton
                pinView.canShowCallout = true
            }
            return pinView;
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        let lat = CLLocationDegrees(Double((self.contactUs?.lat ?? ""))!)
        let lng = CLLocationDegrees(Double((self.contactUs?.lng ?? ""))!)
        
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        
        Localide.sharedManager.promptForDirections(toLocation: location) { (usedApp, fromMemory, openedLinkSuccessfully) in
        }
    }
}
