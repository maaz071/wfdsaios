//
//  MyInvoicesViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/31/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift
import AVFoundation
import Sheeeeeeeeet

enum InvoiceFilterType : String {
    case paid = "Paid"
    case unpaid = "Unpaid"
    case all = "All"
}

class MyInvoicesViewController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    var myInvoicesArray = [Invoice]()
    var myAllInvoicesArray = [Invoice]()
    var paymentsVC:MyPaymentsViewController? = nil//MyPaymentsViewController.instantiate(fromAppStoryboard: .Settings)

    
    lazy var filterBarButtonItem:UIBarButtonItem = {
        return UIBarButtonItem(image: #imageLiteral(resourceName: "icons8filter"), style: .plain, target: self, action: #selector(filterData))
    }()
    
    var filterType = InvoiceFilterType.all

    var actionSheet : ActionSheet?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.getMyInvoices()
        self.title = "My Payments"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8filter"), style: .plain, target: self, action: #selector(filterData))
        self.tableView.contentInset = .init(top: 0, left: 0, bottom: 10, right: 0)
        setupFilterSheet()
    }
    
    func setupFilterSheet(){
        
        var sheetItemArray:[ActionSheetItem] = [ActionSheetSingleSelectItem.init(title: "Paid", isSelected: false), ActionSheetSingleSelectItem.init(title: "Unpaid", isSelected: false), ActionSheetSingleSelectItem.init(title: "All", isSelected: true)]
        sheetItemArray.append(ActionSheetButton.init(title: "Cancel", value: true))
        
        self.actionSheet = ActionSheet(items: sheetItemArray) { (sheet, item) in
            print(item.title)
            
            if let item = item as? ActionSheetSingleSelectItem{
                let key = item.title
                self.filterType = InvoiceFilterType(rawValue: key)!
                switch self.filterType {
                case .all:
                    self.myInvoicesArray = self.myAllInvoicesArray
                    break
                case .paid:
                    self.myInvoicesArray = self.myAllInvoicesArray.filter({ (invoice) -> Bool in
                        return invoice.paymentStatus == "1"
                    })
                    break
                case .unpaid:
                    self.myInvoicesArray = self.myAllInvoicesArray.filter({ (invoice) -> Bool in
                        return invoice.paymentStatus == "0" || invoice.paymentStatus == ""
                    })
                    break
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func getMyInvoices() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["payment_data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Invoice(fromJson: $0)})
                self.myInvoicesArray = dataArray
                self.myAllInvoicesArray = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getMyInvoices(parameters: ["user_id":(AppStateManager.sharedInstance.loggedInUser.id)!], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    @objc func filterData(){
        actionSheet?.present(in: self, from: self.filterBarButtonItem)
    }
    
    //MARK:- IBACtions
    
    fileprivate func selectPaymentTab() {
        if (self.paymentsVC == nil) {
            self.paymentsVC = MyPaymentsViewController.instantiate(fromAppStoryboard: .Settings)
        }
        self.navigationController?.hero.navigationAnimationType = .slide(direction: .right)
        self.paymentsVC?.invoicesVC = self;
        self.hero.replaceViewController(with: self.paymentsVC!)
    }
    
    @IBAction func paymentButtonTapped(_ sender: UIButton) {
        selectPaymentTab()
    }
    
    @IBAction func paymentsTabViewTapped(_ sender: UITapGestureRecognizer) {
        selectPaymentTab()
    }
    
    
    fileprivate func selectInvoicesTab() {
    }
    
    @IBAction func invoiceButtonTapped(_ sender: UIButton) {
        selectInvoicesTab()
    }
    @IBAction func invoicesTabViewTapped(_ sender: UITapGestureRecognizer) {
        selectInvoicesTab()
    }


}


extension MyInvoicesViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myInvoicesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyInvoiceTableViewCell") as! MyInvoiceTableViewCell
        cell.index = indexPath.row
        cell.populateDataFromUserGuide(userGuide: self.myInvoicesArray[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension MyInvoicesViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MyInvoiceTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return MyInvoiceTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = InvoiceDetailViewController.instantiate(fromAppStoryboard: .Settings)
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.invoiceId = self.myInvoicesArray[indexPath.row].invoiceId
        vc.invoice = self.myInvoicesArray[indexPath.row]
        vc.index = indexPath.row
        vc.delegate = self;
        self.slideMenuController()?.present(vc, animated: true)
    }
}

extension MyInvoicesViewController : MyInvoicesPaymentViewCellDelegate {
    func didTapOnPayButton(index: Int) {
        let vc = RegistrationViewController.instantiate(fromAppStoryboard: .Home)
        vc.invoice_id = self.myInvoicesArray[index].invoiceId
        vc.amount = self.myInvoicesArray[index].grandTotal
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

