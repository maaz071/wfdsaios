//
//  CommentsViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 8/2/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SwiftyJSON

class CommentsViewController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var commentsArray = [Comment]()
    
    var eventId = ""
    
    @IBAction func commentButtonAction(_ sender: UIButton) {
        
        guard !self.textView.text.isEmpty else{
            return
        }
        self.startLoading()
        let ref = Database.database().reference().child("Messages").child(self.eventId).childByAutoId()
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: Date())
        dateFormatter.dateFormat = "HH:mm"
        let time = dateFormatter.string(from: Date())
        let user = AppStateManager.sharedInstance.loggedInUser
        let dict = ["name" : user?.firstName ?? "", "message": self.textView.text, "post": user?.role ?? "", "date": "\(date)", "time": "\(time)", "imageURL": user?.uploadImage ?? ""] as [String : Any]
        
        ref.setValue(dict) { (error, reff) in
            self.textView.text = ""
            self.stopLoading()
            if error != nil {
                self.showErrorBar(message: (error?.localizedDescription ?? "Something Went Wrong! Please try again"))
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.startLoading()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.textView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.textView.layer.borderWidth = 1
        self.textView.layer.cornerRadius = 3
        self.textView.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
        let ref = Database.database().reference().child("Messages").child("\(self.eventId)")
        ref.observe(DataEventType.value) { (snapShot) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            self.commentsArray = JSON(snapShot.value as Any).dictionaryValue.values.map({ Comment(fromJson: $0)})
            // sorting
            self.commentsArray = self.commentsArray.sorted(by: { (a, b) -> Bool in
                guard let aDate = dateFormatter.date(from: (a.date + " " + a.time)) else { return false}
                guard let bDate = dateFormatter.date(from: (b.date + " " + b.time)) else { return false}
                return aDate < bDate
            })
            self.tableView.reloadData()
            self.stopLoading()
        }
    }
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        //handle appearing of keyboard here
    }
    
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        //handle dismiss of keyboard here
    }
    
    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
//        self.dismiss(animated: true)
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
}

extension CommentsViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        cell.populateData(self.commentsArray[indexPath.row])
        return cell
    }
}

extension CommentsViewController : UITableViewDelegate {
    
}
