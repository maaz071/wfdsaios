//
//  AppUserGuideViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift
import Lightbox
import AVFoundation

class AppUserGuideViewController: BaseController {

    @IBOutlet weak var tableView: UITableView!
    var userGuidesArray = [UserGuide]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getAppUserGuides()
        self.tableView.contentInset = .init(top: 20, left: 0, bottom: 10, right: 0)
    }
    
    func getAppUserGuides() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ UserGuide(fromJson: $0)})
                self.userGuidesArray = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getUserGuides(parameters: [:], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}


extension AppUserGuideViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userGuidesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppUserGuideTableViewCell") as! AppUserGuideTableViewCell
        cell.populateDataFromUserGuide(userGuide: self.userGuidesArray[indexPath.row])
        return cell
    }
}

extension AppUserGuideViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AppUserGuideTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var images = [LightboxImage]()
        
        for userGuide in self.userGuidesArray {
            if let url = URL(string: (userGuide.uploadVideo ?? "")) {
                images.append(LightboxImage(image: UIColor.black.toImage()!, text: userGuide.title, videoURL: url))
            }
        }

        let controller = LightboxController(images: images)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        controller.goTo(indexPath.row)
        self.present(controller, animated: true, completion: nil)
    }
}

extension AppUserGuideViewController : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}
