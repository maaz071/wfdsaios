//
//  ImageGalleryViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 8/1/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import AlamofireImage
import SwiftyJSON
import Gallery
import Lightbox

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var greyView: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}

extension ImageCollectionViewCell : UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        self.activityIndicator.startAnimating()
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.activityIndicator.stopAnimating()
    }
}

class ImageGalleryViewController: BaseController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var gallery: GalleryController!
    var images = [String]()
    var eventId = ""
    var imageList = [UIImage?]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.getEvents()
    }
    
    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
//        self.dismiss(animated: true)
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func uploadButtonAction(_ sender: UIButton) {
        self.uploadButtonAction()
    }
    
    
    func getEvents() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Event(fromJson: $0)})
                self.images = dataArray.map({ (event) -> String in
                    return event.galleryImages
                })
                self.collectionView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getEventGallery(parameters: ["event_id": "\(self.eventId)", "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "")], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func uploadButtonAction(){
        gallery = GalleryController()
        Config.Camera.recordLocation = false
        Config.Camera.imageLimit = 5
        Config.tabsToShow = [.imageTab, .cameraTab]
        gallery.delegate = self
        self.present(gallery, animated: true, completion: nil)
//        self.navigationController?.present(gallery, animated: true, completion: nil)
    }
    
    func processImages() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            self.showSuccessWithMessage("Once Approved your uploaded will be shown to this gallery", layout: .statusLine)
        }
        
        var parameters: [String: AnyObject] = [
            "user_id":String(AppStateManager.sharedInstance.loggedInUser.id) as AnyObject,
            "event_id":self.eventId as AnyObject,
            ]
        var images = [String]()
        for i in 0...self.imageList.count-1 {
            if let image = self.imageList[i] {
                let imageData = UIImageJPEGRepresentation(image, 0.4)! as NSData
                images.append(imageData.base64EncodedString(options: .lineLength64Characters))
            }
        }
        parameters["imageList"] = images as AnyObject
        APIManager.sharedInstance.uploadGalleryImages(parameters: parameters, success: successClosure){ (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension ImageGalleryViewController : UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell)!
        if let url = URL(string: self.images[indexPath.row]){
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            imageCell.imageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
        imageCell.imageView.hero.id = "image_\(indexPath.item)"
        imageCell.imageView.hero.modifiers = [.fade, .scale(0.8)]
        imageCell.imageView.isOpaque = true
        return imageCell
    }
}

extension ImageGalleryViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var images = [LightboxImage]()
        
        for urlString in self.images {
            if let url = URL(string: urlString){
                images.append(LightboxImage(imageURL: url))
            }
        }
        
        let controller = LightboxController(images: images)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        controller.goTo(indexPath.row)
        self.present(controller, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10);
    }
}


extension ImageGalleryViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = ((collectionView.frame.width-15-20)/3);
        return CGSize(width: size, height: size)
    }
}

extension ImageGalleryViewController : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}


extension ImageGalleryViewController : GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true)
    }
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true)
        var imList =  [UIImage?]()
        var index = 0
        images.forEach { (ima) in
            ima.resolve(completion: { (image) in
                imList.append(image)
                if index >= (images.count-1) {
                    self.imageList = imList
                    self.processImages()
                    return
                }
                index = index+1
            })
        }
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) {
        self.startLoading()
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self?.stopLoading()
            self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
        })
    }
    
    func showLightbox(images: [UIImage]) {
        guard images.count > 0 else {
            return
        }
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.dismissalDelegate = self
        lightbox.dynamicBackground = true
        gallery.present(lightbox, animated: true, completion: nil)
    }
}

