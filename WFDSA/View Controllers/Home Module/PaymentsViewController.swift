//
//  PaymentsViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class PaymentsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var paymentOptions = ["My Payments", "My Invoices"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.contentInset = .init(top: 10, left: 0, bottom: 10, right: 0)
        
        tableView.register(UINib(nibName: "TextOnlyTableViewCell", bundle: nil), forCellReuseIdentifier: "TextOnlyTableViewCell")
        
    }
    
}


extension PaymentsViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paymentOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextOnlyTableViewCell", for: indexPath) as! TextOnlyTableViewCell
        cell.titleLabel.text = self.paymentOptions[indexPath.row]
        return cell
    }
}

extension PaymentsViewController : UITableViewDelegate {
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.row == 0 {
            let vc = MyPaymentsViewController.instantiate(fromAppStoryboard: .Settings)
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1 {
            let vc = MyInvoicesViewController.instantiate(fromAppStoryboard: .Settings)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
