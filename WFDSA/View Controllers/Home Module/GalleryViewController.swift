//
//  GalleryViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 17/09/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON

enum AlbumTableViewType : Int {
    case photo = 0
    case video = 1
}

class GalleryViewController: BaseController {

    @IBOutlet weak var selectedSegmentView: UIView!
    @IBOutlet weak var selectedSegmentCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    
    @IBOutlet weak var photoTableView: UITableView!
    @IBOutlet weak var photoTableViewLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var videoTableView: UITableView!
    @IBOutlet weak var videoTableViewLeadingConstraint: NSLayoutConstraint!
    
    var photoAlbums = [Album]()
    var videoAlbums = [Album]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        photoTableView.register(UINib(nibName: "AlbumTableViewCell", bundle: nil), forCellReuseIdentifier: "AlbumTableViewCell")
        videoTableView.register(UINib(nibName: "AlbumTableViewCell", bundle: nil), forCellReuseIdentifier: "AlbumTableViewCell")
        
        self.photoTableView.tableFooterView = UIView()
        self.videoTableView.tableFooterView = UIView()
        
        self.videoTableViewLeadingConstraint.constant = UIScreen.main.bounds.width
        
        self.getPhotosAlbums()
        self.getVideoAlbums()
    }
    
    
    func getPhotosAlbums() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Album(fromJson: $0)})
                self.photoAlbums = dataArray
                self.photoTableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getPhotoAlbums(parameters: ["role":(AppStateManager.sharedInstance.loggedInUser.role ?? "")!], success: successClosure) { (error) in
//            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func getVideoAlbums() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Album(fromJson: $0)})
                self.videoAlbums = dataArray
                self.videoTableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getVideoAlbums(parameters: ["role":(AppStateManager.sharedInstance.loggedInUser.role ?? "")!], success: successClosure) { (error) in
//            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
//MARK:- IBACtions
    
    fileprivate func selectPhotoTab() {
        self.selectedSegmentCenterXConstraint = self.selectedSegmentCenterXConstraint.changeMultiplier(multiplier: 0.5)
        self.photoTableViewLeadingConstraint.constant = 0
        self.videoTableViewLeadingConstraint.constant = UIScreen.main.bounds.width
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.photoButton.setTitleColor(UIColor(hex: "5C5670"), for: .normal)
            self.videoButton.setTitleColor(UIColor.white, for: .normal)
            self.view!.layoutIfNeeded()
        }, completion: { (_) in
        })
    }
    
    @IBAction func photoButtonTapped(_ sender: UIButton) {
        selectPhotoTab()
    }
    
    @IBAction func photosTabViewTapped(_ sender: UITapGestureRecognizer) {
        selectPhotoTab()
    }
    
    
    fileprivate func selectVideosTab() {
        self.selectedSegmentCenterXConstraint = self.selectedSegmentCenterXConstraint.changeMultiplier(multiplier: 1.5)
        self.videoTableViewLeadingConstraint.constant = 0
        self.photoTableViewLeadingConstraint.constant = -UIScreen.main.bounds.width
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.videoButton.setTitleColor(UIColor(hex: "5C5670"), for: .normal)
            self.photoButton.setTitleColor(UIColor.white, for: .normal)
            self.view.layoutIfNeeded()
        }, completion: { (_) in
        })
    }
    
    @IBAction func videoButtonTapped(_ sender: UIButton) {
        selectVideosTab()
    }
    @IBAction func videosTabViewTapped(_ sender: UITapGestureRecognizer) {
        selectVideosTab()
    }
}


extension GalleryViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let type = AlbumTableViewType(rawValue: tableView.tag) else {return 0}
        switch type {
        case .photo:
            
            if self.photoAlbums.count > 0
            {
                self.photoTableView.separatorStyle = .singleLine
                self.photoTableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No Image Album Found"
                noDataLabel.textColor     = UIColor.black
                noDataLabel.font = UIFont(name: "Roboto", size: 17 * Utility.scaleFactor())
                noDataLabel.textAlignment = .center
                self.photoTableView.backgroundView  = noDataLabel
                self.photoTableView.separatorStyle  = .none
            }
            
            return self.photoAlbums.count
        case .video:
            
            if self.videoAlbums.count > 0
            {
                self.videoTableView.separatorStyle = .singleLine
                self.videoTableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No Video Album Found"
                noDataLabel.textColor     = UIColor.black
                noDataLabel.font = UIFont(name: "Roboto", size: 17 * Utility.scaleFactor())
                noDataLabel.textAlignment = .center
                self.videoTableView.backgroundView  = noDataLabel
                self.videoTableView.separatorStyle  = .none
            }

            return self.videoAlbums.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTableViewCell", for: indexPath) as! AlbumTableViewCell
        guard let type = AlbumTableViewType(rawValue: tableView.tag) else {return cell}
        switch type {
        case .photo:
            cell.populateCellWith(self.photoAlbums[indexPath.row])
            break
        case .video:
            cell.populateCellWith(self.videoAlbums[indexPath.row])
            break
        }
        return cell
    }
}

extension GalleryViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0 * Utility.scaleFactor()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = AlbumGalleryViewController.instantiate(fromAppStoryboard: .Home)
        guard let type = AlbumTableViewType(rawValue: tableView.tag) else {return}
        switch type {
        case .photo:
            vc.albumID = self.photoAlbums[indexPath.row].albumId
            vc.title = self.photoAlbums[indexPath.row].albumTitle.capitalized
            break
        case .video:
            vc.albumID = self.videoAlbums[indexPath.row].albumId
            vc.title = self.videoAlbums[indexPath.row].albumTitle.capitalized
            break
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

