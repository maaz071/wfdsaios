//
//  MemberDetailsViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/30/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON

class MemberDetailsViewController: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    var members = [RoleMember]()
    var roleTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.roleTitle.uppercased()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.contentInset = .init(top: 10, left: 0, bottom: 10, right: 0)
        
        tableView.register(UINib(nibName: "CommitteeTableViewCell", bundle: nil), forCellReuseIdentifier: "CommitteeTableViewCell")
        tableView.register(UINib(nibName: "NonMemberCommitteeTableViewCell", bundle: nil), forCellReuseIdentifier: "NonMemberCommitteeTableViewCell")
        tableView.estimatedRowHeight = 250.0
        self.getMembers()

    }
    
    
    func getMembers() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["response"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ RoleMember(fromJson: $0)})
                self.members = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getRoleMembers(parameters: ["role_id": self.roleTitle], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }

}


extension MemberDetailsViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (AppStateManager.sharedInstance.loggedInUser.signinType ?? "2")! == "2" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NonMemberCommitteeTableViewCell", for: indexPath) as! NonMemberCommitteeTableViewCell
            cell.populateData(member: self.members[indexPath.row])
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommitteeTableViewCell", for: indexPath) as! CommitteeTableViewCell
        cell.populateData(member: self.members[indexPath.row])
        return cell
    }
}


extension MemberDetailsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
