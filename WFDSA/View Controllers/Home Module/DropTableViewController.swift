//
//  TableViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 30/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import Foundation
import DropDownTableView
import Alamofire
import SwiftyJSON
import SwiftWebVC

class DropTableViewController: BaseTableViewController {
    
    var categoryId = ""
    var categories = [ResourcesCategory]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "earth"))
        (self.tableView.backgroundView as? UIImageView)?.contentMode = .scaleAspectFill
    
        self.tableView.register(UINib(nibName: "TextOnlyTableViewCell", bundle: nil), forCellReuseIdentifier: "TextOnlyTableViewCell")
        self.tableView.register(UINib(nibName: "ResourcesTableViewCell", bundle: nil), forCellReuseIdentifier: "ResourcesTableViewCell")
        
        getResourcesCategories()
        
    }
    
    override func numberOfRows(in tableView: UITableView) -> Int {
        return self.categories.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfSubrowsInRow row: Int) -> Int {
        if self.categories.count > 0 {
            return self.categories[row].resources?.count ?? 0
        }
        return 0
    }
    
    override var showSubrowsInRow: Int? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRow row: Int, indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextOnlyTableViewCell", for: indexPath) as! TextOnlyTableViewCell
        
        cell.titleLabel.text = self.categories[row].name
        
        if row == self.nsk_selectedRow {
            cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "icons8-collapse2"))
        } else {
            cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "icons8-expand2"))
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, cellForSubrow subrow: Int, inRow row: Int, indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResourcesTableViewCell", for: indexPath) as! ResourcesTableViewCell
        
        let resource = self.categories[row].resources[subrow]
        
        cell.titleLabel.text = resource.title2
        cell.iconImage.image = self.getFileImage(urlString: (resource.uploadFile ?? ""))
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRow row: Int) {
        switch (self.nsk_selectedRow, row) {
        case (let sr?, _) where row == sr:
            tableView.cellForRow(at: row)?.accessoryView =  UIImageView(image: #imageLiteral(resourceName: "icons8-expand2"))
            tableView.deselect(row: row, animated: true)
            
            print("\(row) case 1")
            break
        case (let sr?, _) where row != sr:
            tableView.cellForRow(at: row)?.accessoryView = UIImageView(image: #imageLiteral(resourceName: "icons8-collapse2"))
            tableView.cellForRow(at: sr)?.accessoryView = UIImageView(image: #imageLiteral(resourceName: "icons8-expand2"))
            
            print("\(row) case 2")
            break
        case (nil, _):
            tableView.cellForRow(at: row)?.accessoryView = UIImageView(image: #imageLiteral(resourceName: "icons8-collapse2"))
            
            print("\(row) case 3")
            break
        default:
            print("\(row) case default")
            break
        }
        super.tableView(tableView, didSelectRow: row)
    }
    
    override func tableView(_ tableView: UITableView, didSelectSubrow subrow: Int, inRow row: Int) {
        
        let resource = self.categories[row].resources[subrow]
        if let urlS = resource.uploadFile{
            let urlString = urlS
            if let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                let webVC = SwiftWebVC(pageURL: url, sharingEnabled: true)
                self.navigationController?.pushViewController(webVC, animated: true)
            }
        }
    }
    
    func getResourcesCategories() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ ResourcesCategory(fromJson: $0)})
                self.categories = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getResourcesByCategories(parameters: ["category_id": self.categoryId, "role":(AppStateManager.sharedInstance.loggedInUser.role ?? "")!], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func getFileImage(urlString:String) -> UIImage {
        
        if urlString.contains("pdf") {
            return #imageLiteral(resourceName: "pdf-icon")
        }else if urlString.contains("doc") || urlString.contains("docx") {
            return #imageLiteral(resourceName: "word-icon")
        }else if urlString.contains("xls") || urlString.contains("xlsx") {
            return #imageLiteral(resourceName: "excel-icon")
        }else if urlString.contains("ppt") || urlString.contains("pptx") {
            return #imageLiteral(resourceName: "ppt-icon")
        }else if urlString.contains("txt") {
            return #imageLiteral(resourceName: "txt-icon")
        }else {
            return #imageLiteral(resourceName: "other-icon")
        }
    }
    
}
