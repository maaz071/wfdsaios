//
//  HomeViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/27/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift
import SwiftWebVC
import Firebase
import UserNotifications


enum HomeCellType {
    case event
    case resources
    case announcement
}

protocol ViewAllCellDelegate : class {
    func didTapOnViewAll(type:HomeCellType)
}

class HomeViewController: BaseController {
    
    
    @IBOutlet weak var tableView: UITableView!
    var dashboardEvent:Event?
    var resourcesArray:[Resource]?
    var announcementData:Announcement?
    var headerView: UIView!
    
    private let kTableHeaderHeight: CGFloat = CGFloat(300.0) * Utility.scaleFactor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.contentInset = .init(top: 0, left: 0, bottom: 30, right: 0)

        self.title = "Welcome To WFDSA"
        self.slideMenuController()?.delegate = self
        self.setNavigationBarItem()
        
        self.setupTableView()
        self.registerCellsNib()
        
        self.tableView.hero.modifiers = [.cascade]
        for cell in tableView.visibleCells {
            cell.hero.modifiers = [.fade, .translate(CGPoint(x: 0, y: 200*(Utility.scaleFactor()))), .scale(1.5)]
        }
    }
    
    func registerCellsNib(){
        
        tableView.register(UINib(nibName: "HomeEventsTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeEventsTableViewCell")
        tableView.register(UINib(nibName: "HomeResourcesTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeResourcesTableViewCell")
        tableView.register(UINib(nibName: "HomeAnnouncementsTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeAnnouncementsTableViewCell")
        
    }
    
    func setupTableView() {
        
        headerView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.addSubview(headerView)
        
        tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0 )
        tableView.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateHeaderView()
        self.slideMenuController()?.addLeftGestures()
        
        self.getSingleEvent()
        self.getRecentThreeResources()
        self.getLatestAnnouncement()
    }

    
    func updateHeaderView() {
        
        var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableView.bounds.width, height: kTableHeaderHeight)
        if tableView.contentOffset.y < -kTableHeaderHeight {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
        }
        
        headerView.frame = headerRect
    }
    
    func getSingleEvent() {
        
        if self.dashboardEvent == nil {
            startLoading()
        }
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Event(fromJson: $0)})
                self.dashboardEvent = dataArray.first
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getSingleEvent(parameters: [:], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func getRecentThreeResources() {
        
        if self.resourcesArray == nil {
            startLoading()
        }
        
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: "No Network Connection")
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Resource(fromJson: $0)})
                self.resourcesArray = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getRecentThreeResources(parameters: ["role_id":(AppStateManager.sharedInstance.loggedInUser.role ?? "")!], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func getLatestAnnouncement() {
        if self.announcementData == nil {
            startLoading()
        }
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Announcement(fromJson: $0)})
                self.announcementData = dataArray.first
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getLatestAnnouncement(parameters: ["signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType)!, "role":(AppStateManager.sharedInstance.loggedInUser.role ?? "")!], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension HomeViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear

        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeEventsTableViewCell", for: indexPath) as! HomeEventsTableViewCell
            cell.populateDataFromEvent(event: self.dashboardEvent)
            cell.delegate = self
            cell.selectedBackgroundView = backgroundView
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeResourcesTableViewCell", for: indexPath) as! HomeResourcesTableViewCell
            cell.populateDataWithResourcesArray(self.resourcesArray ?? [])
            cell.delegate = self
            cell.tapdelegate = self
            cell.selectedBackgroundView = backgroundView
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeAnnouncementsTableViewCell", for: indexPath) as! HomeAnnouncementsTableViewCell
            cell.populateDataFromAnnouncement(self.announcementData)
            cell.delegate = self
            cell.selectedBackgroundView = backgroundView
            return cell
        }
    }
}

extension HomeViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        switch indexPath.row {
        case 0:
            
            if (self.dashboardEvent?.permission.contains("Public") ?? false) {
                self.moveToEventsVCwithEventID(self.dashboardEvent?.eventId ?? "")
            }else {
                _ = AppStateManager.sharedInstance.loggedInUser.role.components(separatedBy: ",").map {
                    if (self.dashboardEvent?.permission.contains($0))! {
                        self.moveToEventsVCwithEventID(self.dashboardEvent?.eventId ?? "")
                    }
                    }
            }
            break
        case 1:
            break
        case 2:
            let vc = AnnouncementsViewController.instantiate(fromAppStoryboard: .Home)
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            break
        }
    }
    
    func moveToEventsVCwithEventID(_ eventId:String) {
        let vc = EventDetailViewController.instantiate(fromAppStoryboard: .Home)
        vc.event_id = eventId
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension HomeViewController : ViewAllCellDelegate {
    func didTapOnViewAll(type: HomeCellType) {
        var vc = UIViewController()
        switch type {
        case .event:
            vc = EventsViewController.instantiate(fromAppStoryboard: .Home)
            break
        case .resources:
            vc = ResourcesViewController.instantiate(fromAppStoryboard: .Home)
            break
        case .announcement:
            vc = AnnouncementsViewController.instantiate(fromAppStoryboard: .Home)
            break
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeViewController : HomeResourcesTableViewCellDelegate {
    func resourceTappedAt(index: Int) {
        
        guard (self.resourcesArray?.indices.contains(index) ?? false)! else {
            return
        }
        guard let resource = self.resourcesArray?[index] else { return }
        
        if let urlS = resource.uploadFile{
            let urlString = urlS
            if let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                let webVC = SwiftWebVC(pageURL: url, sharingEnabled: true)
                self.navigationController?.pushViewController(webVC, animated: true)
            }
        }
    }
}


extension HomeViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }
}


extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
