//
//  RegistrationViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/30/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import AnimatedTextInput
import SwiftValidator
import IQKeyboardManagerSwift
import NKVPhonePicker
import RealmSwift
import SwiftyJSON
import DropDown
import Stripe

class RegistrationViewController: BaseController {
    
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var feesLabel: UILabel!
    @IBOutlet weak var nameField: AnimatedTextInput!
    @IBOutlet weak var cardNumberField: AnimatedTextInput!
    @IBOutlet weak var cvcCodeField: AnimatedTextInput!
    @IBOutlet weak var addressField: AnimatedTextInput!
    @IBOutlet weak var contactField: AnimatedTextInput!
    @IBOutlet weak var countryField: AnimatedTextInput!
    @IBOutlet weak var emailField: AnimatedTextInput!
    @IBOutlet weak var monthField: AnimatedTextInput!
    @IBOutlet weak var yearField: AnimatedTextInput!
    
    let validator = Validator()
    var monthDropDown:DropDown!
    var yearsDropDown:DropDown!
    var event : EventDetail?
    var invoice_id : String?
    var amount : String?
    var StripeToken : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "REGISTER EVENT"
        
        if self.event == nil{
            self.title = "INVOICE PAYMENT"
        }
        
        self.setupFields()
        self.setupValidators()
        self.setupEventDetails()
    }
    
    @IBAction func payButtonAction(_ sender: UIButton) {
        
        IQKeyboardManager.sharedManager().resignFirstResponder()
        self.resetValidators()
        validator.validate(self)
        
        
        
    }
    
    func setupEventDetails(){
        self.eventNameLabel.text = self.event?.title
        self.feesLabel.text = self.event?.fee
    }
    
    func openCardController(){
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        self.navigationController?.pushViewController(addCardViewController, animated: true)
    }
    
    
    func setupFields() {
        
        let appInputStyle = AppTextInputStyle(lineHeight: 1.5*Utility.scaleFactor(), yPlaceholderPositionOffset: 0.0, textAttributes: [:])
        
        self.nameField.placeHolderText = "NAME(AS APPEARS ON CARD)"
        self.nameField.type = .standard
        self.nameField.style = appInputStyle
        
        self.monthField.placeHolderText = "MONTH"
        self.monthField.type = .selection
        self.monthField.style = appInputStyle
        self.monthField.tapAction = { [weak self] in
            guard let strongself = self else { return }
            strongself.openCardController()
            } as (() -> Void)
        
        self.yearField.placeHolderText = "YEAR"
        self.yearField.type = .selection
        self.yearField.style = appInputStyle
        self.yearField.tapAction = { [weak self] in
            guard let strongself = self else { return }
            strongself.openCardController()
            } as (() -> Void)
        
        self.contactField.placeHolderText = "CONTACT NUMBER"
        self.contactField.type = .phone
        self.contactField.delegate = self
        self.contactField.style = appInputStyle
        
        self.countryField.placeHolderText = "COUNTRY"
        self.countryField.type = .selection
        self.countryField.tapAction = { [weak self] in
            guard let strongself = self else { return }
            strongself.showCountryPicker()
            } as (() -> Void)
        self.countryField.style = appInputStyle
        
        self.emailField.placeHolderText = "EMAIL"
        self.emailField.type = .email
        self.emailField.style = appInputStyle
        
        self.cvcCodeField.placeHolderText = "CVC CODE"
        self.cvcCodeField.type = .selection
        self.cvcCodeField.style = appInputStyle
        self.cvcCodeField.tapAction = { [weak self] in
            guard let strongself = self else { return }
            strongself.openCardController()
            } as (() -> Void)
        
        self.cardNumberField.placeHolderText = "CARD NUMBER"
        self.cardNumberField.type = .selection
        self.cardNumberField.style = appInputStyle
        self.cardNumberField.tapAction = { [weak self] in
            guard let strongself = self else { return }
            strongself.openCardController()
            } as (() -> Void)
        
        self.addressField.placeHolderText = "ADDRESS"
        self.addressField.type = .standard
        self.addressField.style = appInputStyle
        
    }
    
    func setupValidators(){
        validator.registerField(self.nameField,errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(self.monthField,errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(self.yearField,errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(self.cardNumberField,errorLabel: nil, rules: [RequiredRule(),MinLengthRule(length: 12)])
        validator.registerField(self.contactField,errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(self.countryField,errorLabel: nil, rules: [RequiredRule(message: "Please Select a Country")])
        validator.registerField(self.emailField,errorLabel: nil, rules: [RequiredRule(),EmailRule(message: "Please Enter a valid email Address")])
        validator.registerField(self.addressField,errorLabel: nil,rules: [RequiredRule()])
        validator.registerField(self.cvcCodeField,errorLabel: nil,rules: [RequiredRule(), MinLengthRule(length: 3), MaxLengthRule(length: 4)])
    }
    
    //MARK: - Helper Methods
    
    func resetValidators() {
        self.nameField.clearError()
        self.cardNumberField.clearError()
        self.contactField.clearError()
        self.countryField.clearError()
        self.emailField.clearError()
        self.cvcCodeField.clearError()
        self.addressField.clearError()
    }
    
    func showCountryPicker() {
        let countriesViewController:CountriesViewController = CountriesViewController.standardController()
        countriesViewController.delegate = self
        let navVC = UINavigationController(rootViewController: countriesViewController)
        self.present(navVC, animated: true)
    }
    
    func setupMonthsDropDown(){
        self.monthDropDown = DropDown(anchorView: self.monthField)
        monthDropDown.dataSource = ["January", "Febuary", "March", "April", "May", "June", "July", "Augest", "September", "October", "November", "December"]
        monthDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.monthField.text = item
        }
    }
    
    func setupYearsDropDown(){
        self.yearsDropDown = DropDown(anchorView: self.monthField)
        yearsDropDown.dataSource = ["2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"]
        yearsDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.yearField.text = item
        }
    }
    
    func showMonthDropDown(){
        self.monthDropDown.show()
        self.yearsDropDown.hide()
    }
    
    func showYearsDropDown(){
        self.yearsDropDown.show()
        self.monthDropDown.hide()
    }
    
    func processRegister(){
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            if let parent = self.parent as? EventDetailViewController {
                parent.registerButton.setTitle("Registered", for: .normal)
                parent.registerButton.isUserInteractionEnabled = false
            }
            
            self.navigationController?.popViewController(animated: true)
        }
        
        if (self.event?.fee.isEmpty ?? false) {
            APIManager.sharedInstance.registerForEvent(parameters: ["api_secret":AppStateManager.sharedInstance.loggedInUser.token, "stripe_token":self.StripeToken ?? "0", "amount":"0", "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? ""), "signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType ?? "2"), "event_id":self.event?.eventId ?? ""], success: successClosure) { (error) in
                self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
                self.stopLoading()
            }
        }else{
            APIManager.sharedInstance.registerWithAmountForEvent(parameters: ["api_secret":AppStateManager.sharedInstance.loggedInUser.token, "stripe_token":self.StripeToken ?? "0", "amount":self.event?.fee ?? "0", "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? ""), "signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType ?? "2"), "event_id":self.event?.eventId ?? ""], success: successClosure) { (error) in
                self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
                self.stopLoading()
            }
        }        
    }
    
    func processPayment(){
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            self.navigationController?.popViewController(animated: true)
        }
        
        APIManager.sharedInstance.invoicePayment(parameters: ["api_secret":AppStateManager.sharedInstance.loggedInUser.token, "stripe_token":self.StripeToken ?? "", "amount":self.amount ?? "0", "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "0"), "signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType ?? "2"), "invoice_id":self.invoice_id ?? ""], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

//MARK: -

extension RegistrationViewController: ValidationDelegate {
    
    func validationSuccessful() {
        
        if (CEReachabilityManager.isReachable()){
            if self.event == nil {
                self.processPayment()
                return
            }
            processRegister()
        }else{
            IQKeyboardManager.sharedManager().resignFirstResponder()
            self.showErrorWith(message: "Please check your internet connection")
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        
        for (field, error) in errors {
            if let field = field as? AnimatedTextInput {
                field.show(error: error.errorMessage, placeholderText: field.placeHolderText)
            }
        }
    }
}

extension RegistrationViewController : CountriesViewControllerDelegate {
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country) {
        self.countryField.text = country.name
    }
    
    func countriesViewControllerDidCancel(_ sender: CountriesViewController) { }
}

extension RegistrationViewController : AnimatedTextInputDelegate {
    func animatedTextInput(animatedTextInput: AnimatedTextInput, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        animatedTextInput.text = Utility.formatPhoneNumber(animatedTextInput.text, in: range, replacementString: string)
        return false
    }
}

extension RegistrationViewController : STPAddCardViewControllerDelegate{
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        
        self.navigationController?.popViewController(animated: true)
        
        self.cardNumberField.text = "**** **** **** \(token.card?.last4 ?? "")"
        self.monthField.text = self.getDayOfWeek(month: Int(token.card?.expMonth ?? 0))
        self.yearField.text = "\(token.card?.expYear ?? 0)"
        self.cvcCodeField.text = "****"
        
    }
    
    func getDayOfWeek(month : Int) -> String? {
        let formatter  = DateFormatter()
        let monthName = formatter.monthSymbols[month]
        return monthName
    }
    
}


