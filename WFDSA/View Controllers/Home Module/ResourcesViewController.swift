//
//  ResourcesViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON

class ResourcesViewController: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    var categories = [ResourcesCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = Menu.resources.rawValue.uppercased()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.contentInset = .init(top: 10, left: 0, bottom: 10, right: 0)
        tableView.register(UINib(nibName: "ResourcesTableViewCell", bundle: nil), forCellReuseIdentifier: "ResourcesTableViewCell")
        self.getResourcesCategories()
    }
    
    func getResourcesCategories() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ ResourcesCategory(fromJson: $0)})
                self.categories = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getResourcesCategories(parameters: ["role_id":(AppStateManager.sharedInstance.loggedInUser.role ?? "")!], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}


extension ResourcesViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResourcesTableViewCell", for: indexPath) as! ResourcesTableViewCell
        cell.titleLabel.text = self.categories[indexPath.row].name
        return cell
    }
}


extension ResourcesViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = DropTableViewController.instantiate(fromAppStoryboard: .Home)
            vc.categoryId = self.categories[indexPath.row].categoriesId
            vc.title = self.categories[indexPath.row].name
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
