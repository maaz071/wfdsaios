//
//  MembersViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Sheeeeeeeeet

class MembersViewController: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    var members = [DsaMember]()
    var allMembers = [DsaMember]()
    
    lazy var filterBarButtonItem:UIBarButtonItem = {
    return UIBarButtonItem(image: #imageLiteral(resourceName: "icons8filter"), style: .plain, target: self, action: #selector(filterData))
    }()
    
    var actionSheet : ActionSheet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = filterBarButtonItem
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.contentInset = .init(top: 10, left: 0, bottom: 10, right: 0)
        self.tableView.estimatedRowHeight = 250.0
        self.getDsaMembers()
    }
    
    func setupFilterSheet(){
        
        
        var titles = self.members.map { (member) in
            return member.region
        }
        titles = Array(Set(titles))
        
        var sheetItemArray = titles.map { (member) -> ActionSheetItem in
            return ActionSheetSingleSelectItem.init(title: member ?? "", isSelected: false)
        }
        
        sheetItemArray.insert(ActionSheetSingleSelectItem.init(title: "All", isSelected: false), at: 0)
        sheetItemArray.append(ActionSheetButton.init(title: "Cancel", value: true))
        
        self.actionSheet = ActionSheet(items: sheetItemArray) { (sheet, item) in
            print(item.title)
            
            if let item = item as? ActionSheetSingleSelectItem{
                let key = item.title
                if key == "All"{
                    self.members = self.allMembers
                }else{
                    self.members = self.allMembers.filter({ (member) -> Bool in
                        return member.region == key
                    })
                }
                self.tableView.reloadData()
            }
        }
    }
    
    
    func getDsaMembers() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ DsaMember(fromJson: $0)})
                self.members = dataArray
                self.allMembers = dataArray
                self.setupFilterSheet()
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getDsaMembers(parameters: [:], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    @objc func filterData(){
        actionSheet?.present(in: self, from: self.filterBarButtonItem)
    }
}

extension MembersViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*
        if AppStateManager.sharedInstance.loggedInUser.signinType != "\(UserType.member.rawValue)"  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "nonMembersTableViewCell", for: indexPath) as! nonMembersTableViewCell
            cell.populateDataFromEvent(member: self.members[indexPath.row])
            return cell
        }else {
            */
            let cell = tableView.dequeueReusableCell(withIdentifier: "MembersTableViewCell", for: indexPath) as! MembersTableViewCell
            cell.populateDataFromEvent(member: self.members[indexPath.row])
            return cell
//        }
    }
}

extension MembersViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
