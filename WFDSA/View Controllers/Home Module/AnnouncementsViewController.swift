//
//  AnnouncementsViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON

class AnnouncementsViewController: BaseController {

    @IBOutlet weak var tableView: UITableView!
    var announcementData = [Announcement]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = Menu.announcements.rawValue.uppercased()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.contentInset = .init(top: 10, left: 0, bottom: 10, right: 0)
        self.getLatestAnnouncement()
        
    }
    
    func getLatestAnnouncement() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Announcement(fromJson: $0)})
                self.announcementData = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getAllAnnouncement(parameters: ["signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType)!, "role":(AppStateManager.sharedInstance.loggedInUser.role ?? "")!], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension AnnouncementsViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.announcementData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnnouncementsTableViewCell", for: indexPath) as! AnnouncementsTableViewCell
        
        cell.populateCell(announcement: self.announcementData[indexPath.row])
        
        return cell
    }
}


extension AnnouncementsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
