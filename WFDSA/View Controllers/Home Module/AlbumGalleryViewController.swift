//
//  AlbumGalleryViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 21/09/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import AlamofireImage
import SwiftyJSON
import Gallery
import Lightbox
import SwiftWebVC

class AlbumGalleryViewController: BaseController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var gallery: GalleryController!
    var images = [AlbumObject]()
    var albumID = ""
    var imageList = [UIImage?]()
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.getGallery()
    }
    
    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
        //        self.dismiss(animated: true)
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    func getGallery() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ AlbumObject(fromJson: $0)})
                self.images = dataArray
                self.collectionView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getAlbumGallery(parameters: ["album_id": "\(self.albumID)"], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension AlbumGalleryViewController : UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell)!
        imageCell.imageView.image = #imageLiteral(resourceName: "noimage")
        let album = self.images[indexPath.item]
        if let url = URL(string: album.uploadLink){
            let placeholderImage = #imageLiteral(resourceName: "noimage")
            imageCell.imageView.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
        imageCell.imageView.hero.id = "image_\(indexPath.item)"
        imageCell.imageView.hero.modifiers = [.fade, .scale(0.8)]
        imageCell.imageView.isOpaque = true
        
        switch album.type {
        case "2":
            let urlString = (self.images[indexPath.row].uploadLink.replacingOccurrences(of: "\"", with: "").components(separatedBy: "src=").last?.components(separatedBy: " ").first)
            
            imageCell.greyView.isHidden = true
            imageCell.iconView.image = UIImage(named: "youtube-play-icon")
            imageCell.iconView.isHidden = true
            imageCell.webView.delegate = imageCell
            imageCell.webView.isHidden = false
            imageCell.webView.isOpaque = false
            imageCell.webView.backgroundColor = UIColor.black
            imageCell.webView.scalesPageToFit = true
            imageCell.webView.allowsInlineMediaPlayback = true
            imageCell.webView.scrollView.isScrollEnabled = false
            if let url = URL(string: urlString?.removingWhitespacesAndNewlines ?? "") {
                imageCell.webView.loadRequest(URLRequest(url: url))
            }else {
                imageCell.webView.loadHTMLString(self.images[indexPath.row].uploadLink, baseURL: nil)
            }
            break
        case "1":
            imageCell.greyView.isHidden = false
            imageCell.iconView.image = UIImage(named: "video-play-icon")
            imageCell.iconView.isHidden = false
            imageCell.webView.delegate = imageCell
            imageCell.webView.isOpaque = false
            imageCell.webView.backgroundColor = UIColor.black
            imageCell.webView.isHidden = false
            imageCell.webView.scalesPageToFit = true
            imageCell.webView.allowsInlineMediaPlayback = false
            imageCell.webView.mediaPlaybackRequiresUserAction = true
            if let url = URL(string: self.images[indexPath.row].uploadLink.removingWhitespacesAndNewlines + "?playsinline=1" ) {
                imageCell.webView.loadRequest(URLRequest(url: url))
            }else {
                
            }
            break
        default:
            imageCell.greyView.isHidden = false
            imageCell.iconView.isHidden = false
        }
        
        return imageCell
    }
}

extension AlbumGalleryViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var images = [LightboxImage]()
        
        guard self.images[indexPath.row].type != "2" else {
            let urlString = (self.images[indexPath.row].uploadLink.replacingOccurrences(of: "\"", with: "").components(separatedBy: "src=").last?.components(separatedBy: " ").first)
            if let url = URL(string: urlString?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! ?? "") {
                UIApplication.shared.open(url)
            }
            return
        }
        
        for album in self.images {
            
            switch album.type {
            case "1":
                if let albumURL = URL(string: album.uploadLink) {
                    let lightboxObject = LightboxImage(image: #imageLiteral(resourceName: "noimage"), text: "", videoURL: albumURL)
                    images.append(lightboxObject)
                }
                break
            case "0":
                if let albumURL = URL(string: album.uploadLink) {
                    let lightboxObject = LightboxImage(imageURL: albumURL)
                    images.append(lightboxObject)
                }
                break
            default:
                break
            }
        }
        
        let controller = LightboxController(images: images)
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        controller.dynamicBackground = true
        controller.goTo(indexPath.row)
        self.present(controller, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
    }
}


extension AlbumGalleryViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = ((collectionView.frame.width-20));
        return CGSize(width: size, height: size*3/4)
    }
}

extension AlbumGalleryViewController : LightboxControllerPageDelegate, LightboxControllerDismissalDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

extension String {
    
    func deleteHTMLTag(tag:String) -> String {
        return self.replacingOccurrences(of: "(?i)</?\(tag)\\b[^<]*>", with: "", options: .regularExpression, range: nil)
    }
    
    func getHTMLTag(tag:String) -> String {
        return self.replacingOccurrences(of: "(?i)</?\(tag)\\b[^<]*>", with: "", options: .regularExpression, range: nil)
    }
    
    func deleteHTMLTags(tags:[String]) -> String {
        var mutableString = self
        for tag in tags {
            mutableString = mutableString.deleteHTMLTag(tag: tag)
        }
        return mutableString
    }
    
    var removingWhitespacesAndNewlines: String {
        return components(separatedBy: .whitespacesAndNewlines).joined()
    }

}
