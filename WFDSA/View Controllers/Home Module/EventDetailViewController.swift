//
//  EventDetailViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 29/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftMessages
import MapKit
import Sheeeeeeeeet
import Localide
import EventKit

class EventDetailViewController: BaseController {

    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var timingView: UIView!
    @IBOutlet weak var speakerView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var speakerNameLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var floatingButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var mapImageView: UIImageView!
    @IBOutlet weak var mkMapView: MKMapView!
    
    let locationManager = CLLocationManager()
    var eventLocation : CLLocation?
    var myLocation : CLLocation?
    var actionSheet : ActionSheet?
    
    var event_id = ""
    var event : EventDetail? {
        didSet{
            if let event = self.event {
            self.populateDataFromEvent(event: event)
            }
        }
    }
    
    @IBAction func likeButtonAction(_ sender: UIButton) {
        if self.event?.isLike != 1 {
            addLike()
            sender.setImage(#imageLiteral(resourceName: "thumbs_up"), for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.eventView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.eventView.layer.borderWidth = 1
        self.eventView.layer.cornerRadius = 3
        self.eventView.clipsToBounds = true
        self.eventView.backgroundColor = UIColor.clear
        
        self.timingView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.timingView.layer.borderWidth = 1
        self.timingView.layer.cornerRadius = 3
        self.timingView.clipsToBounds = true
        self.timingView.backgroundColor = UIColor.clear
        
        self.speakerView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        self.speakerView.layer.borderWidth = 1
        self.speakerView.layer.cornerRadius = 3
        self.speakerView.clipsToBounds = true
        self.speakerView.backgroundColor = UIColor.clear
        
//        self.floatingButton.addShadow(color: #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1), cornerRadius: 25)
        self.floatingButton.backgroundColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        self.floatingButton.layer.cornerRadius = 25
        self.floatingButton.clipsToBounds = true
        self.floatingButton.setImage(UIImage(named: "poll-icon"), for: .normal)
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        self.mkMapView.delegate = self
        
        self.getEventDetails()
    }
    
    @objc func addEventToDefaultCalendar() {
        
        let dateForm = DateFormatter()
        dateForm.dateFormat = "MM-dd-yyyy HH:mm"
        guard let startDate = dateForm.date(from: (event?.startDate ?? "")) else { return }
        guard let endDate = dateForm.date(from: (event?.endDate ?? "")) else { return }
        
        self.addEventToCalendar(title: (self.event?.title ?? "" )!, description: (self.event?.agenda ?? "")!, startDate: startDate, endDate: endDate) { (success, error) in
            if success {
                DispatchQueue.main.async {
                    self.navigationItem.rightBarButtonItems = []
                }

                var config = SwiftMessages.Config()
                config.presentationStyle = .top
                config.presentationContext = .viewController(self)
                let messageView = MessageView.viewFromNib(layout: .tabView)
                messageView.configureTheme(.info)
                messageView.configureContent(title: "", body: "Event is added in Calendar")
                messageView.button?.isHidden = false
                messageView.button?.setTitle("Open", for: .normal)
                messageView.buttonTapHandler = { _ in
                    let interval = startDate.timeIntervalSinceReferenceDate
                    if let url = URL(string: "calshow:\(interval)") {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
                SwiftMessages.show(config: config, view: messageView)
                
            }else {
                self.showErrorBar(message: "Event could not be added in Calendar")
            }
        }
    }
    
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }

    
    func populateDataFromEvent(event : EventDetail){
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "calendar-icon"), style: .plain, target: self, action: #selector(addEventToDefaultCalendar))

        self.title = event.title ?? ""
        self.titleLabel.text = event.title ?? ""
        self.detailsLabel.text = event.agenda ?? ""
        let dateForm = DateFormatter()
        dateForm.dateFormat = "MM-dd-yyyy HH:mm"
        if let date = dateForm.date(from: (event.startDate ?? "")) {
            let newdateForm = DateFormatter()
            newdateForm.dateFormat = "dd-MMM-yyyy"
            self.dateLabel.text = newdateForm.string(from: date)
            newdateForm.dateFormat = "HH:mm"
            self.timeLabel.text = newdateForm.string(from: date)
        }
        self.locationLabel.text = event.venueAddress ?? ""
        self.speakerNameLabel.text = event.speaker ?? ""
        if let likes = Int((event.totalLikes ?? "0")) {
            self.likeButton.setTitle("\(event.totalLikes ?? "0") \((likes > 1) ? "LIKES" : "LIKE") ", for: .normal)
        }
        
        if event.isLike == 1 {
            self.likeButton.setImage(#imageLiteral(resourceName: "thumbs_up"), for: .normal)
        }else{
            self.likeButton.setImage(#imageLiteral(resourceName: "thumbs_up"), for: .normal)
        }
        self.setupMapSnapShot()
        
    }
    
    func setupMapSnapShot(){
        
        let longlat = self.event?.latLng.split(separator: ",")
        let latitude = Double(String(longlat?.first ?? "0"))
        let longitude = Double(String(longlat?.last ?? "0"))

        self.eventLocation = CLLocation(latitude: latitude ?? 0.0, longitude: longitude ?? 0.0)
        
        let location = CLLocationCoordinate2DMake(latitude ?? 0.0, longitude ?? 0.0)
        let region = MKCoordinateRegionMakeWithDistance(location, 1000, 1000)
        mkMapView.region = region
        
        self.populateFields()
    }
    
    
    func getEventDetails() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ EventDetail(fromJson: $0)})
                self.event = dataArray.first
                if self.event?.isRegistered == 1{
                    self.registerButton.isUserInteractionEnabled = false
                    self.registerButton.setTitle("You are already registered for the Event", for: .normal)
                }
            }
        }
        
        APIManager.sharedInstance.getEventDetails(parameters: ["signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType)!, "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "")!, "event_id" : self.event_id], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func addLike() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            if let likes = Int((self.event?.totalLikes ?? "0")!) {
                self.likeButton.setTitle("\(likes+1) \((likes+1) <= 1 ? " Like " :" Likes ")", for: .normal)
                self.likeButton.isUserInteractionEnabled = false
            }
        }
        
        APIManager.sharedInstance.addLike(parameters: ["signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType)!, "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "")!, "event_id" : self.event_id], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func processPayment(){
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            self.showSuccessWithMessage("You are registered to this event!", layout: .statusLine)
            self.getEventDetails()
        }
        let params = ["event_id": self.event_id, "api_secret":(AppStateManager.sharedInstance.loggedInUser.token ?? "")!, "stripe_token":"0", "amount":"0", "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "0"), "signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType ?? "2")] as [String : Any]
        APIManager.sharedInstance.registerForEvent(parameters: params, success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    @IBAction func registerButtonAction(_ sender: UIButton) {
        guard (self.event?.fee != "") else {
            self.processPayment()
            return
        }
        
        guard (self.event?.fee != "0") else {
            self.processPayment()
            return
        }
        
        let vc = RegistrationViewController.instantiate(fromAppStoryboard: .Home)
        vc.event = self.event
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func shareButtonAction(_ sender: UIButton) {
        
        let vc = UIActivityViewController(activityItems: ["\(self.event?.title ?? "")\n \(self.event?.startDate ?? "")\n \(self.event?.agenda ?? "")", URL(string: "https://wfdsa.org")!], applicationActivities: nil)
        vc.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList, UIActivityType.assignToContact, UIActivityType.saveToCameraRoll, UIActivityType.openInIBooks]
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func peopleButtonAction(_ sender: UIButton) {
    
        guard self.event?.isRegistered == 1 else {
            self.showInfoWithMessage("Members can only be seen by registered users", layout: .statusLine)
            return
        }

        let vc = EventMembersViewController.instantiate(fromAppStoryboard: .Settings)
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.eventId = self.event_id
        self.slideMenuController()?.present(vc, animated: true)
    }
    
    @IBAction func commentButtonAction(_ sender: UIButton) {
        
        guard self.event?.isRegistered == 1 else {
            self.showInfoWithMessage("Discussions can only be seen by registered users", layout: .statusLine)
            return
        }
        
        let vc = CommentsViewController.instantiate(fromAppStoryboard: .Settings)
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.eventId = self.event_id
        self.slideMenuController()?.present(vc, animated: true)
    }
    
    @IBAction func pictureButtonAction(_ sender: UIButton) {
        guard self.event?.isRegistered == 1 else {
            self.showInfoWithMessage("Gallery can only be viewed by registered users", layout: .statusLine)
            return
        }
        
        let vc = ImageGalleryViewController.instantiate(fromAppStoryboard: .Settings)
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.eventId = self.event_id
        self.slideMenuController()?.present(vc, animated: true)
    }
    
    @IBAction func checkButtonAction(_ sender: UIButton) {
        
        guard self.event?.isRegistered == 1 else {
            self.showInfoWithMessage("Checkins can only be performed by registered users", layout: .statusLine)
            return
        }
        
        let status  = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
            return
        }
        
        if status == .denied || status == .restricted {
            showLocationError()
            return
        }
        
        if let eventLocation = self.eventLocation {
            if Int(self.myLocation?.distance(from: eventLocation ) ?? 5000) < 5000 {
                self.checkIn()
            }else{
                self.showErrorBar(message: "You are not at event Location")
            }
        }
    }
    
    func showLocationError(){
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .viewController(self)
        let error = MessageView.viewFromNib(layout: .tabView)
        error.configureTheme(.info)
        error.configureContent(title: "Location Services Disabled", body: "Please enable Location Services in Settings")
        error.button?.setTitle("Settings", for: .normal)
        error.buttonTapHandler = { _ in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        
        error.button?.isHidden = false
        SwiftMessages.show(config: config, view: error)
    }
    
    @IBAction func pollButtonAction(_ sender: UIButton) {
        
        guard self.event?.isRegistered == 1 else {
            self.showInfoWithMessage("Polls can only be seen by registered users", layout: .statusLine)
            return
        }
        
        getPolls()
    }
    
    
    func checkIn() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"]?.boolValue else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if responseData {
                self.showErrorWith(message: "You have already checked in")
                return
            } else {
                self.showErrorWith(message: "You have succesfully Checked in")
                return
            }
        }
        
        APIManager.sharedInstance.checkIn(parameters: ["signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType)!, "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "")!, "event_id" : self.event_id], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func getPolls() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [String : AnyObject] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = Poll(fromJson: jsonArray)
                self.setupSheet(poll: dataArray)
                self.actionSheet?.present(in: self, from: self.view)
            }
        }
        
        APIManager.sharedInstance.getPolls(parameters: ["event_id" : self.event_id], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func addPollAnswer(pollId : String, pollAnswerId : String) {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            guard let responseData = result["response"] as? String else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            self.showSuccessMessage(message: responseData)
        }
        
        APIManager.sharedInstance.addPollAnswer(parameters: ["user_id":AppStateManager.sharedInstance.loggedInUser.id ?? "","poll_id": pollId,"poll_answer_id": pollAnswerId,"member_type":AppStateManager.sharedInstance.loggedInUser.signinType ?? ""], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func setupSheet(poll : Poll){
        
        var sheetItemArray = poll.pollAnswer.map { (member) -> ActionSheetItem in
            return ActionSheetSingleSelectItem.init(title: member, isSelected: false)
        }
        sheetItemArray.insert(ActionSheetTitle.init(title: poll.pollData.pollQuestion), at: 0)
        sheetItemArray.append(ActionSheetButton.init(title: "Cancel", value: true))
        
        self.actionSheet = ActionSheet(items: sheetItemArray) { (sheet, item) in
            print(item.title)
            
            if let item = item as? ActionSheetSingleSelectItem{
                let key = item.title
                let index = poll.pollAnswer.index(of: key)
                self.addPollAnswer(pollId: poll.pollData.pollId ?? "", pollAnswerId: "\(index?.hashValue ?? 0)")
            }
        }
    }
    
    func populateFields() {
        
        let coordinate = self.eventLocation?.coordinate
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate!
        annotation.title = self.event?.title
        annotation.subtitle = self.event?.agenda
        self.mkMapView.addAnnotation(annotation)
    }
}

extension EventDetailViewController : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let _: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.myLocation = manager.location
    }
    
}

extension EventDetailViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            return nil;
        }else{
            let pinIdent = "Pin";
            var pinView: MKPinAnnotationView;
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation;
                pinView = dequeuedView;
                let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                        size: CGSize(width: 20, height: 20)))
                mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControlState())
                pinView.rightCalloutAccessoryView = mapsButton
                pinView.canShowCallout = true
            }else{
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: pinIdent);
                let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                        size: CGSize(width: 20, height: 20)))
                mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControlState())
                pinView.rightCalloutAccessoryView = mapsButton
                pinView.canShowCallout = true
            }
            return pinView;
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {

        let location = self.eventLocation?.coordinate ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        
        Localide.sharedManager.promptForDirections(toLocation: location) { (usedApp, fromMemory, openedLinkSuccessfully) in
        }
    }
}

