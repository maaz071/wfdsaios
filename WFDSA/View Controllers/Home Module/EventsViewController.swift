//
//  EventsViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON

class EventsViewController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var events = [Event]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Menu.events.rawValue.uppercased()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.contentInset = .init(top: 10, left: 0, bottom: 10, right: 0)
        
        self.getEvents()
    }
    
    
    func getEvents() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ Event(fromJson: $0)})
                self.events = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getAllEvents(parameters: [:], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }

}


extension EventsViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell", for: indexPath) as! EventTableViewCell
        
        cell.populateDataFromEvent(event: self.events[indexPath.row])
        return cell
    }
}


extension EventsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return EventTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let event = self.events[indexPath.row]
        
        if (event.permission.contains("Public")) {
            self.moveToEventsVCwithEventID(self.events[indexPath.row].eventId ?? "")
        }else {
            _ = AppStateManager.sharedInstance.loggedInUser.role.components(separatedBy: ",").map {
                if (event.permission.contains($0)) {
                    self.moveToEventsVCwithEventID(self.events[indexPath.row].eventId ?? "")
                }
            }
        }
    }
    
    func moveToEventsVCwithEventID(_ eventId:String) {
        let vc = EventDetailViewController.instantiate(fromAppStoryboard: .Home)
        vc.event_id = eventId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
