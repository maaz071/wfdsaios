//
//  SideMenuController.swift
//  Template
//
//  Created by alijabbar on 3/18/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import AlamofireImage
import DTPhotoViewerController

enum Menu: String {
    case events = "events"
    case resources = "resources"
    case payments = "payments"
    case announcements = "announcements"
    case wfdas_leadership = "wfdsa leadership"
    case committees = "committees"
    case dsa_members = "dsa members"
    case about_wfdsa = "about wfdsa"
    case contact_us = "contact us"
    case app_user_guide = "app user guide"
    case logout = "logout"
    case settings = "settings"
    case gallery = "gallery"
}


class SideMenuController: BaseController {
    
    private let kTableHeaderHeight: CGFloat = 100
    
    @IBOutlet var tableView: UITableView!
    
    var headerView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var userImageview: UIImageView!
    @IBOutlet weak var borderView: UIView!
    
    var menuArray = [
        ["icon": #imageLiteral(resourceName: "ic_events"), "title": Menu.events.rawValue.uppercased()],
        ["icon": #imageLiteral(resourceName: "ic_announcement"), "title": Menu.announcements.rawValue.uppercased()],
        ["icon": #imageLiteral(resourceName: "ic_resources"), "title": Menu.resources.rawValue.uppercased()],
        ["icon": #imageLiteral(resourceName: "leader"), "title": Menu.wfdas_leadership.rawValue.uppercased()],
        ["icon": #imageLiteral(resourceName: "ic_member"), "title": Menu.dsa_members.rawValue.uppercased()],
        ["icon": #imageLiteral(resourceName: "ic_about"), "title": Menu.about_wfdsa.rawValue.uppercased()],
        ["icon": UIImage(named: "ic_gengallery")!, "title": Menu.gallery.rawValue.uppercased()],
        ["icon": #imageLiteral(resourceName: "ic_contact"), "title": Menu.contact_us.rawValue.uppercased()],
        ["icon": #imageLiteral(resourceName: "ic_payment"), "title": Menu.payments.rawValue.uppercased()],
        ["icon": #imageLiteral(resourceName: "ic_user_guide"), "title": Menu.app_user_guide.rawValue.uppercased()]
//        ["icon": #imageLiteral(resourceName: "ic_committees"), "title": Menu.committees.rawValue.uppercased()]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        
        tableView.hero.modifiers = [.cascade]
        for cell in tableView.visibleCells {
            cell.hero.modifiers = [.fade, .scale(0.5)]
        }
        /*
        if (AppStateManager.sharedInstance.loggedInUser.signinType ?? "2")! == "2" {
            menuArray = [
                ["icon": #imageLiteral(resourceName: "ic_events"), "title": Menu.events.rawValue.uppercased()],
                ["icon": #imageLiteral(resourceName: "ic_resources"), "title": Menu.resources.rawValue.uppercased()],
                ["icon": #imageLiteral(resourceName: "ic_payment"), "title": Menu.payments.rawValue.uppercased()],
                ["icon": #imageLiteral(resourceName: "ic_announcement"), "title": Menu.announcements.rawValue.uppercased()],
                ["icon": #imageLiteral(resourceName: "leader"), "title": Menu.wfdas_leadership.rawValue.uppercased()],
                ["icon": #imageLiteral(resourceName: "ic_member"), "title": Menu.dsa_members.rawValue.uppercased()],
                ["icon": #imageLiteral(resourceName: "ic_about"), "title": Menu.about_wfdsa.rawValue.uppercased()],
                ["icon": #imageLiteral(resourceName: "ic_contact"), "title": Menu.contact_us.rawValue.uppercased()],
                ["icon": #imageLiteral(resourceName: "ic_user_guide"), "title": Menu.app_user_guide.rawValue.uppercased()]
            ]
        }
 */
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupUserDetails()
        self.updateHeaderView()
        self.setupImageView()
        if let url = URL(string: (AppStateManager.sharedInstance.loggedInUser.uploadImage ?? "")) {
            let placeholderImage = #imageLiteral(resourceName: "ic_profile_pic")
            self.userImageview.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
    }
    
    
    func setupUserDetails() {
        
        self.nameLabel.text = (AppStateManager.sharedInstance.loggedInUser.firstName + " " + AppStateManager.sharedInstance.loggedInUser.lastName).capitalized
        self.emailLabel.text = AppStateManager.sharedInstance.loggedInUser.email
        if let url = URL(string: (AppStateManager.sharedInstance.loggedInUser.uploadImage ?? "")) {
            let placeholderImage = #imageLiteral(resourceName: "ic_profile_pic")
            self.userImageview.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        }
    }
    
    func setupImageView() {
        self.userImageview.layer.cornerRadius = self.userImageview.frame.size.width/2
        self.borderView.layer.cornerRadius = self.borderView.frame.size.width/2
        self.borderView.layer.borderColor = UIColor.black.cgColor
        self.borderView.layer.borderWidth = 2
        self.userImageview.contentMode = .scaleAspectFill
        self.userImageview.clipsToBounds = true
    }
    
    func setupTableView() {
        
        headerView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.addSubview(headerView)
        
        tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0 )
        tableView.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
        
    }
    
    
    func updateHeaderView() {
        
        var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableView.bounds.width, height: kTableHeaderHeight)
        if tableView.contentOffset.y < -kTableHeaderHeight {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
        }
        
        headerView.frame = headerRect
    }
    
    
    @IBAction func profilePictureTapped(_ sender: Any) {
        
        let profilePicture:UIImageView = (sender as! UITapGestureRecognizer).view as! UIImageView
        guard profilePicture.image != Constants.PLACEHOLDER_USER else {return}
        if let viewController = SimplePhotoViewerController(referencedView: profilePicture, image: profilePicture.image) {
            self.present(viewController, animated: true, completion: nil)
        }
        
    }
    
    func pushViewController(vc : UIViewController){
        
        self.slideMenuController()?.closeLeft()
        (self.slideMenuController()?.mainViewController as? UINavigationController)?.pushViewController(vc, animated: true)
    }
    
    
    func changeViewController(_ menu: Menu) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        
        
        switch menu {
        case .events:
            let vc = storyboard.instantiateViewController(withIdentifier: "EventsViewController")
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .resources:
            let vc = storyboard.instantiateViewController(withIdentifier: "ResourcesViewController")
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .payments:
            
            let vc = MyPaymentsViewController.instantiate(fromAppStoryboard: .Settings)
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .announcements:
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController")
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .wfdas_leadership:
            let vc = storyboard.instantiateViewController(withIdentifier: "LeadershipViewController")
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .committees:
            let vc = storyboard.instantiateViewController(withIdentifier: "CommitteesViewController")
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .dsa_members:
            let vc = storyboard.instantiateViewController(withIdentifier: "MembersViewController")
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .about_wfdsa:
            let vc = AboutViewController.instantiate(fromAppStoryboard: .Settings)
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .contact_us:
            let vc = ContactUsViewController.instantiate(fromAppStoryboard: .Settings)
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .app_user_guide:
            let vc = AppUserGuideViewController.instantiate(fromAppStoryboard: .Settings)
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .settings:
            let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController")
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
        case .gallery:
            let vc = GalleryViewController.instantiate(fromAppStoryboard: .Home)
            vc.title = menu.rawValue.uppercased()
            pushViewController(vc: vc)
            break
            
        case .logout:
            self.slideMenuController()?.closeLeft()
            break
            //            HelperMethods.delay(delay: 0.5, closure: {
            //
            //                let alertController = UIAlertController(title: nil, message: "Are you sure you want to logout?", preferredStyle: .alert)
            //
            //                let actionYes = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            //                    let navController = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "SignInController_NavID")
            //
            //                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            //
            //                    UIView.transition(with: appDelegate.window!, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            //                        appDelegate.window?.rootViewController = navController
            //                        appDelegate.window?.makeKeyAndVisible()
            //                    }, completion: nil)
            //
            //                    //TODO: - work on it later on
            ////                    AppStateManager.sharedInstance.logOutUser()
            //                })
            //
            //                let actionNo = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            //                    self.slideMenuController()?.closeRight()
            //                })
            //
            //                alertController.addAction(actionYes)
            //                alertController.addAction(actionNo)
            //                self.present(alertController, animated: true, completion: nil)
            //
            //            })
            
            
        }
    }
    
    @IBAction func settingsButtonTapped(_ sender: UIButton) {
        let vc = ProfileViewController.instantiate(fromAppStoryboard: .Settings)
        pushViewController(vc: vc)
    }
    
    @IBAction func logoutButtonTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to Signout?", preferredStyle: .alert)
        
        let actionYes = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            self.slideMenuController()?.closeLeft()
            self.processLogout()
        })
        
        let actionNo = UIAlertAction(title: "NO", style: .cancel, handler: { (action) in
        })
        
        alertController.addAction(actionYes)
        alertController.addAction(actionNo)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func changeRootViewController() {
        if !AppStateManager.sharedInstance.isUserLoggedIn() {
            let vc = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "LoginViewControllerNav_ID")
            self.slideMenuController()?.hero.replaceViewController(with: vc)
        }
        else {
            let homeVC:HomeViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            let homeNavVC:BaseNavigationController = BaseNavigationController(rootViewController: homeVC)
            let sideMenuController:SideMenuController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
            let slideMenuController = SlideMenuController(mainViewController: homeNavVC, leftMenuViewController: sideMenuController, rightMenuViewController: UIViewController())
            
            self.slideMenuController()?.hero.replaceViewController(with: slideMenuController)
        }
    }
    
    func processLogout() {
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            self.stopLoading()
            AppStateManager.sharedInstance.logOutUser()
            self.changeRootViewController()
            self.showSuccessWithMessage("Logged Out Successfully!", layout: .statusLine)
        }
        
        let parameters: [String: String] = ["signin_type":(AppStateManager.sharedInstance.loggedInUser.signinType)!, "user_id":(AppStateManager.sharedInstance.loggedInUser.id ?? "")]
        
        APIManager.sharedInstance.logout(parameters: parameters, success: successClosure){ (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }

    
}

extension SideMenuController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }
}

extension SideMenuController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SideMenuTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cellType = (self.menuArray[indexPath.row] as NSDictionary)["title"] as! String
        
        if let menu = Menu(rawValue: cellType.lowercased()){
            self.changeViewController(menu)
        }
    }
}

extension SideMenuController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        
        let image = menuArray[indexPath.row]["icon"] as? UIImage
        image?.withRenderingMode(.alwaysTemplate)
        cell.iconImageView.tintColor = #colorLiteral(red: 0.3607843137, green: 0.337254902, blue: 0.4392156863, alpha: 1)
        cell.iconImageView.image = image
        cell.titleLabel.text = (menuArray[indexPath.row]["title"] as! String)
        
        return cell
    }
    
    
}







