//
//  LeadershipViewController.swift
//  WFDSA
//
//  Created by Umer Jabbar on 28/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import SwiftyJSON


class LeadershipViewController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var members = [LeaderShipMember]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.contentInset = .init(top: 10, left: 0, bottom: 10, right: 0)
        tableView.register(UINib(nibName: "TextOnlyTableViewCell", bundle: nil), forCellReuseIdentifier: "TextOnlyTableViewCell")
        self.getLeaderShipMembers()
    }
    
    
    func getLeaderShipMembers() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            
            guard let responseData = result["data"] as? [[String : AnyObject]] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            if let jsonArray = JSON(rawValue: responseData) {
                let dataArray = jsonArray.arrayValue.map({ LeaderShipMember(fromJson: $0)})
                self.members = dataArray
                self.tableView.reloadData()
            }
        }
        
        APIManager.sharedInstance.getLeaderShipMembers(parameters: [:], success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

extension LeadershipViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextOnlyTableViewCell", for: indexPath) as! TextOnlyTableViewCell
        cell.titleLabel.text = self.members[indexPath.row].name
        return cell
    }
}

extension LeadershipViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = MemberDetailsViewController.instantiate(fromAppStoryboard: .Home)
        vc.roleTitle = self.members[indexPath.row].name
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
