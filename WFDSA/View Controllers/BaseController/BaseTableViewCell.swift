//
//  BaseTableViewCell.swift
//  WFDSA
//
//  Created by Umer Jabbar on 27/07/2018.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    @IBOutlet var adjustableLabels: [UILabel]?
    @IBOutlet var adjustableButtons: [UIButton]?
    @IBOutlet var paddingConstraints: [NSLayoutConstraint]?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        for constraint in paddingConstraints ?? [] {
            constraint.constant *= Utility.scaleFactor()
        }
        
        for label in adjustableLabels ?? [] {
            label.font = UIFont(name: label.font.fontName, size: CGFloat(FontManager.getAdjustedFontSize(fromSize: Float(label.font.pointSize))))
        }
        
        for button in adjustableButtons ?? [] {
            button.titleLabel?.font = UIFont(name: button.titleLabel!.font.fontName, size: CGFloat(FontManager.getAdjustedFontSize(fromSize: Float(button.titleLabel!.font.pointSize))))
        }
        
        
    }
    
    
}
