//
//  BaseController.swift
//  Template
//
//  Created by Muzamil Hassan on 02/01/2017.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import SwiftMessages
import SwiftyJSON
import Hero
import Crashlytics
import SwiftValidator
import IQKeyboardManagerSwift

class BaseController: UIViewController {

    @IBOutlet var adjustableLabels: [UILabel]?
    @IBOutlet var adjustableButtons: [UIButton]?
    @IBOutlet var paddingConstraints: [NSLayoutConstraint]?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    private  lazy var panGR: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(leftSwipeDismiss(gestureRecognizer:)))

    public var enableScreenPanGesture: Bool = true {
        didSet {
            if(enableScreenPanGesture) {
                self.view.addGestureRecognizer(self.panGR)
                return
            }
            if self.view.gestureRecognizers?.contains(panGR) ?? false {
                self.view.removeGestureRecognizer(panGR)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.navigationController?.hero.navigationAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        for constraint in paddingConstraints ?? [] {
            constraint.constant *= Utility.scaleFactor()
        }
        
        for label in adjustableLabels ?? [] {
            label.font = UIFont(name: label.font.fontName, size: CGFloat(FontManager.getAdjustedFontSize(fromSize: Float(label.font.pointSize))))
        }
        
        for button in adjustableButtons ?? [] {
            button.titleLabel?.font = UIFont(name: button.titleLabel!.font.fontName, size: CGFloat(FontManager.getAdjustedFontSize(fromSize: Float(button.titleLabel!.font.pointSize))))
        }
        
        enableScreenPanGesture = false//navigationController?.viewControllers.count ?? 0 > 1 && navigationController?.viewControllers.last == self
        hero.isEnabled = true
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
    }
    
    @objc func back(sender: UIBarButtonItem) {
        IQKeyboardManager.sharedManager().resignFirstResponder()
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func leftSwipeDismiss(gestureRecognizer: UIPanGestureRecognizer) {
        
        let translation = gestureRecognizer.translation(in: nil)
        let progress = translation.x / 2 / view.bounds.width
        let gestureView = gestureRecognizer.location(in: self.view)
        
        switch gestureRecognizer.state {
        case .began:
            if gestureView.x <= 30 {
                hero.dismissViewController()
            }
        case .changed:
            let translation = gestureRecognizer.translation(in: nil)
            let progress = translation.x / 2 / view.bounds.width
            Hero.shared.update(progress)
        default:
            if progress + gestureRecognizer.velocity(in: nil).x / view.bounds.width > 0.3 {
                Hero.shared.finish()
                return
            }
            Hero.shared.cancel()
        }
    }

    func changeNavBarColor(with: UIColor) {
        UIView.transition(with: self.navigationController!.navigationBar, duration: 0.2, options: .transitionCrossDissolve, animations: { () -> Void in
            
            self.navigationController!.navigationBar.barTintColor = with
            self.navigationController!.navigationBar.barTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.navigationController!.navigationBar.setBackgroundImage(UIImage.imageWithColor(color: with), for: .default)
            self.navigationController!.navigationBar.shadowImage = UIImage.imageWithColor(color: with)
        }, completion: nil)
    }
    
    func startLoading(){
        LoadingOverlay.shared.showOverlay()
    }
    
    func stopLoading(){
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func showErrorWith(message: String){
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .viewController(self)
        let error = MessageView.viewFromNib(layout: .tabView)
        error.configureContent(title: "", body: message)
        error.configureTheme(.info)
        error.button?.isHidden = true
        SwiftMessages.show(config: config, view: error)
    }
    
    func showErrorBar(message: String){
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        let error = MessageView.viewFromNib(layout: .tabView)
        error.configureTheme(.info)
        error.configureContent(title: "", body: message)
        error.button?.isHidden = true
        SwiftMessages.show(config: config, view: error)
    }
    
    func showSuccessMessage(message:String){
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .viewController(self)
        let messageView = MessageView.viewFromNib(layout: .tabView)
        messageView.configureTheme(.info)
        messageView.configureContent(title: "", body: message)
        messageView.button?.isHidden = true
        SwiftMessages.show(config: config, view: messageView)
    }
    
    func showSuccessWithMessage(_ message:String, layout:MessageView.Layout) {
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .viewController(self)
        let messageView = MessageView.viewFromNib(layout: .tabView)
        messageView.configureTheme(.info)
        messageView.configureContent(title: "", body: message)
        messageView.button?.isHidden = true
        SwiftMessages.show(config: config, view: messageView)
    }
    
    func showInfoWithMessage(_ message:String, layout:MessageView.Layout) {
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .viewController(self)
        let messageView = MessageView.viewFromNib(layout: .tabView)
        messageView.configureTheme(.info)
        messageView.configureContent(title: "", body: message)
        messageView.button?.isHidden = true
        SwiftMessages.show(config: config, view: messageView)
    }
    
    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "menu_icon")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
//        self.slideMenuController()?.addLeftGestures()
//        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
}
