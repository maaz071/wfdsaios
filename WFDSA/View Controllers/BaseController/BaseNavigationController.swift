//
//  BaseNavigationController.swift
//  Versole
//
//  Created by Soomro Shahid on 2/20/16.
//  Copyright © 2016 Muzamil Hassan. All rights reserved.
//

import UIKit


class BaseNavigationController: UINavigationController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.barTintColor = AppColors.appThemeColor
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [kCTForegroundColorAttributeName : UIColor.white] as [NSAttributedStringKey : Any]
        self.navigationBar.setBackgroundImage(UIImage.imageWithColor(color: AppColors.appThemeColor), for: .default)
        self.navigationBar.shadowImage = UIImage.imageWithColor(color: AppColors.appThemeColor)
        self.hero.isEnabled = true
        self.navigationBar.barStyle = .blackOpaque
        self.navigationBar.isTranslucent = false
    }
}
