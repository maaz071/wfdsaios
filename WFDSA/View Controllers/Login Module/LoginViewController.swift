//
//  LoginViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/27/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import AnimatedTextInput
import SwiftValidator
import IQKeyboardManagerSwift
import RealmSwift
import SwiftyJSON
import FirebaseInstanceID

enum UserType : Int {
    case member = 1
    case nonMember = 2
}

extension AnimatedTextInput: Validatable {
    
    public var validationText: String {
        return text ?? ""
    }
}


class LoginViewController: BaseController {
    
    @IBOutlet weak var memberSegmentSelectedViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupButtonLeadingConstraint: NSLayoutConstraint!
    
    var userType: UserType = .member
    @IBOutlet weak var emailField: AnimatedTextInput!
    @IBOutlet weak var passwordField: AnimatedTextInput!
    
    @IBOutlet weak var signupButton: UIButton!
    let validator = Validator()
    
    var fcm_token = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.setupNavBar()
        self.setupFields()
        self.setupValidators()
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.fcm_token = result.token
            }
        }
    }
    
    //MARK: - Setup Methods
    
    func setupNavBar() {
        self.navigationController?.navigationBar.barTintColor = AppColors.appThemeColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    func setupFields() {
        self.emailField.placeHolderText = "Email"
        self.emailField.type = .email
        self.emailField.style = AppTextInputStyle(lineHeight: 1.5*Utility.scaleFactor(), yPlaceholderPositionOffset: 0.0, textAttributes: [:])
        
        self.passwordField.placeHolderText = "Password"
        self.passwordField.type = .password(toggleable: true)
        self.passwordField.style = AppTextInputStyle(lineHeight: 1.5*Utility.scaleFactor(), yPlaceholderPositionOffset: 0.0, textAttributes: [:])
        
        self.signupButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.signupButton.isHidden = true
    }
    
    func setupValidators(){
        validator.registerField(self.emailField,errorLabel: nil, rules: [RequiredRule(),EmailRule(message: "Please Enter a valid email Address")])
        validator.registerField(self.passwordField,errorLabel: nil,rules: [RequiredRule(),MinLengthRule(length: 6)])
    }
    
    func resetValidators(){
        self.emailField.clearError()
        self.passwordField.clearError()
    }
    
    
    //MARK: - IBActions
    
    @IBAction func memberButtonTapped(_ sender: UIButton) {
        self.userType = .member
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.memberSegmentSelectedViewLeadingConstraint.constant = 0
            self.view.layoutIfNeeded()
            self.signupButton.transform = CGAffineTransform(scaleX: 0.05, y: 0.05)
        }, completion: { (_) in
            self.signupButton.isHidden = true
        })
    }
    
    @IBAction func nonMemberButtonTapped(_ sender: UIButton) {
        self.userType = .nonMember
        self.signupButton.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.memberSegmentSelectedViewLeadingConstraint.constant = sender.frame.width
            self.signupButtonLeadingConstraint.constant = 0
            self.signupButton.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.view.layoutIfNeeded()
        }, completion: { (_) in
        })
    }
    
    @IBAction func signinButtonTapped(_ sender: UIButton) {
        IQKeyboardManager.sharedManager().resignFirstResponder()
        self.resetValidators()
        validator.validate(self)
    }
    
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        
    }
    
    //MARK: - Service
    
    func processSignin() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            AppStateManager.sharedInstance.loggedInUser = nil
            
            guard var responseData = result["user_data"] as? [String : AnyObject] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }

            responseData["token"] = result["api_secret"] as AnyObject?
            if let memeberID = responseData["member_id"] as? String {
                responseData["id"] = memeberID as AnyObject?
            }else if let nonMemberID = responseData["non_member_id"] as? String {
                responseData["id"] = nonMemberID as AnyObject?
            }
            let json  = JSON(responseData)
            AppStateManager.sharedInstance.loggedInUser = User(fromJson: json)
            
            let realm = try! Realm()
            try! realm.write(){
                realm.add(AppStateManager.sharedInstance.loggedInUser, update: true)
            }
            if AppStateManager.sharedInstance.loggedInUser != nil {
                print(AppStateManager.sharedInstance.loggedInUser)
                self.moveToHome()
            }
        }
        
        var parameters = [String:String]()
        parameters["email"] = self.emailField.text
        parameters["password"] = self.passwordField.text
        parameters["signin_type"] = String(describing: self.userType.rawValue)
        parameters["device_token"] = self.fcm_token
        
        APIManager.sharedInstance.authenticateUserWith(parameters: parameters, success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
    
    func moveToHome() {
        let homeVC:HomeViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let homeNavVC:BaseNavigationController = BaseNavigationController(rootViewController: homeVC)
        let sideMenuController:SideMenuController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
        let slideMenuController = SlideMenuController(mainViewController: homeNavVC, leftMenuViewController: sideMenuController, rightMenuViewController: UIViewController())
        
        self.navigationController?.hero.replaceViewController(with: slideMenuController)

    }
}

//MARK: -

extension LoginViewController: ValidationDelegate {
    
    func validationSuccessful() {
        
        if (CEReachabilityManager.isReachable()){
            processSignin()
        }else{
            IQKeyboardManager.sharedManager().resignFirstResponder()
            self.showErrorWith(message: "Please check your internet connection")
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        
        for (field, error) in errors {
             if let field = field as? AnimatedTextInput {
                field.show(error: error.errorMessage, placeholderText: field.placeHolderText)
             }
        }
    }
}
