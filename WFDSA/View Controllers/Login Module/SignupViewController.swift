//
//  SignupViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/27/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import AnimatedTextInput
import SwiftValidator
import IQKeyboardManagerSwift
import NKVPhonePicker
import RealmSwift
import SwiftyJSON

class SignupViewController: BaseController {

    @IBOutlet weak var firstNameField: AnimatedTextInput!
    @IBOutlet weak var lastNameField: AnimatedTextInput!
    @IBOutlet weak var phoneNumberField: AnimatedTextInput!
    @IBOutlet weak var countryField: AnimatedTextInput!
    @IBOutlet weak var emailField: AnimatedTextInput!
    @IBOutlet weak var passwordField: AnimatedTextInput!
    @IBOutlet weak var confirmPasswordField: AnimatedTextInput!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var termsAndConditionsButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupFields()
        self.setupValidators()
        self.setFacebookCallback()
    }
    
    //MARK: - Setup Methods
        
    func setupFields() {
        
        let appInputStyle = AppTextInputStyle(lineHeight: 1.5*Utility.scaleFactor(), yPlaceholderPositionOffset: 0.0, textAttributes: [:])
        
        self.firstNameField.placeHolderText = "First Name"
        self.firstNameField.type = .standard
        self.firstNameField.style = appInputStyle
        
        self.lastNameField.placeHolderText = "Last Name"
        self.lastNameField.type = .standard
        self.lastNameField.style = appInputStyle
        
        self.phoneNumberField.placeHolderText = "Contact Number"
        self.phoneNumberField.type = .phone
        self.phoneNumberField.delegate = self
        self.phoneNumberField.style = appInputStyle
        
        self.countryField.placeHolderText = "Country"
        self.countryField.type = .selection
        self.countryField.tapAction = { [weak self] in
            guard let strongself = self else { return }
            strongself.showCountryPicker()
        } as (() -> Void)
        self.countryField.style = appInputStyle
        
        self.emailField.placeHolderText = "Email"
        self.emailField.type = .email
        self.emailField.style = appInputStyle
        
        self.passwordField.placeHolderText = "Password"
        self.passwordField.type = .password(toggleable: true)
        self.passwordField.style = appInputStyle
        
        self.confirmPasswordField.placeHolderText = "Confirm Password"
        self.confirmPasswordField.type = .password(toggleable: true)
        self.confirmPasswordField.style = appInputStyle
    }
    
    func setupValidators(){
        validator.registerField(self.firstNameField,errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(self.lastNameField,errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(self.phoneNumberField,errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(self.countryField,errorLabel: nil, rules: [RequiredRule(message: "Please Select a Country")])
        validator.registerField(self.emailField,errorLabel: nil, rules: [RequiredRule(),EmailRule(message: "Please Enter a valid email Address")])
        validator.registerField(self.passwordField,errorLabel: nil,rules: [RequiredRule(),MinLengthRule(length: 6)])
        validator.registerField(self.confirmPasswordField,errorLabel: nil,rules: [RequiredRule(),MinLengthRule(length: 6), ConfirmationRule(confirmField: self.passwordField)])
    }
    func setFacebookCallback() {
        FaceBookUserProfile.getUserProfileInfoCallBack = { (firtsName: String,lastName: String,email: String,imageUrl: String,userId:String) in
            
            var parameters = [String:String]()
            parameters["social_media_id"] = "\(userId)"
            parameters["social_media_platform"] = "facebook"
            parameters["full_name"] = "\(firtsName) \(lastName)"
            parameters["email"] = "\(email)"
            parameters["profile_picture"] = "\(imageUrl)"
            parameters["device_type"] = "ios"
            parameters["device_token"] = "1234"
            self.firstNameField.text = firtsName
            self.lastNameField.text = lastName
            self.emailField.text = email
        }
    }
    
    //MARK: - Helper Methods
    
    func resetValidators() {
        self.firstNameField.clearError()
        self.lastNameField.clearError()
        self.phoneNumberField.clearError()
        self.countryField.clearError()
        self.emailField.clearError()
        self.passwordField.clearError()
        self.confirmPasswordField.clearError()
    }
    
    func showCountryPicker() {
        let countriesViewController:CountriesViewController = CountriesViewController.standardController()
        countriesViewController.delegate = self
        let navVC = UINavigationController(rootViewController: countriesViewController)
        self.navigationController?.present(navVC, animated: true)
        
    }
    
    
    //MARK: - IBActions
    
    
    @IBAction func termsAndConditionsButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func facebookButtonTapped(_ sender: UIButton) {
        FaceBookUserProfile.getUserFacebookProfile(from: self)
    }
    
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        IQKeyboardManager.sharedManager().resignFirstResponder()
        self.resetValidators()
        validator.validate(self)
    }
    
    //MARK: - Service
    
    func processSignup() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            print(result)
            AppStateManager.sharedInstance.loggedInUser = nil
            /*
            guard var responseData = result["user_details"] as? [String : AnyObject] else {
                self.showErrorWith(message: "An Unhandled Data Response has Received")
                return
            }
            
            responseData["token"] = result["api_secret"] as AnyObject?
            if let memeberID = responseData["member_id"] as? String {
                responseData["id"] = memeberID as AnyObject?
            }else if let nonMemberID = responseData["non_member_id"] as? String {
                responseData["id"] = nonMemberID as AnyObject?
            }
            
            let json  = JSON(responseData)
            AppStateManager.sharedInstance.loggedInUser = User(fromJson: json)
            
            let realm = try! Realm()
            try! realm.write(){
                realm.add(AppStateManager.sharedInstance.loggedInUser, update: true)
            }
            if AppStateManager.sharedInstance.loggedInUser != nil {
                self.showSuccessWithMessage("Successfully Signed Up!", layout: .statusLine)
                print(AppStateManager.sharedInstance.loggedInUser)
                Constants.APP_DELEGATE.changeRootViewController()
            }
 */
            
//            Constants.APP_DELEGATE.changeRootViewController()
            self.showSuccessWithMessage("You are successfully signed up!", layout: .statusLine)
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        var parameters = [String:String]()
        parameters["first_name"] = self.firstNameField.text
        parameters["last_name"] = self.lastNameField.text
        parameters["email"] = self.emailField.text
        parameters["password"] = self.passwordField.text
        parameters["confirm_password"] = self.confirmPasswordField.text
        parameters["contact"] = self.phoneNumberField.text?.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: " ", with: "")
        parameters["country"] = self.countryField.text
        //TODO: - work with Firebase token
        parameters["firebase_token"] = "Unkown Device Token"
        
        APIManager.sharedInstance.userSignupWith(parameters: parameters, success: successClosure) { (error) in
            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            self.stopLoading()
        }
    }
}

//MARK: -

extension SignupViewController: ValidationDelegate {
    
    func validationSuccessful() {
        
        if (CEReachabilityManager.isReachable()){
            if self.termsAndConditionsButton.isSelected {
                processSignup()
            }else {
                self.termsAndConditionsButton.transform = CGAffineTransform(translationX: 20, y: 0);
                UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                    self.termsAndConditionsButton.transform = CGAffineTransform.identity
                }, completion: nil)
            }
        }else{
            IQKeyboardManager.sharedManager().resignFirstResponder()
            self.showErrorWith(message: "Please check your internet connection")
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        
        for (field, error) in errors {
            if let field = field as? AnimatedTextInput {
                field.show(error: error.errorMessage, placeholderText: field.placeHolderText)
            }
        }
    }
}

extension SignupViewController : CountriesViewControllerDelegate {
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country) {
        self.countryField.text = country.name
    }
    
    func countriesViewControllerDidCancel(_ sender: CountriesViewController) { }
}

extension SignupViewController : AnimatedTextInputDelegate {
    func animatedTextInput(animatedTextInput: AnimatedTextInput, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        animatedTextInput.text = Utility.formatPhoneNumber(animatedTextInput.text, in: range, replacementString: string)
        return false
    }
}
