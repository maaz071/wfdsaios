//
//  ForgotPasswordViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/27/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit
import AnimatedTextInput
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON

class ForgotPasswordViewController: BaseController {

    @IBOutlet weak var emailField: AnimatedTextInput!
    
    let validator = Validator()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupValidators()
        self.setupFields()
    }
    
    //MARK: - Setup Methods

    func setupFields() {
        self.emailField.placeHolderText = "Email"
        self.emailField.type = .email
        self.emailField.style = AppTextInputStyle(lineHeight: 1.5*Utility.scaleFactor(), yPlaceholderPositionOffset: 0.0, textAttributes: [:])
    }
    
    func setupValidators(){
        validator.registerField(self.emailField,errorLabel: nil, rules: [RequiredRule(),EmailRule(message: "Please Enter a valid email Address")])
    }
    
    func resetValidators(){
        self.emailField.clearError()
    }
    
    //MARK: - IBActions
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        IQKeyboardManager.sharedManager().resignFirstResponder()
        self.resetValidators()
        validator.validate(self)
    }
    
    
    //MARK: - Service
    
    func processSignin() {
        
        startLoading()
        if(!CEReachabilityManager.isReachable()){
            self.showErrorWith(message: Constants.noNetworkConnection)
            self.stopLoading()
            return
        }
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (result) in
            
            self.stopLoading()
            var messageString = "Your New Password has been sent at \(String(describing: self.emailField.text))"
            if let responseData = result["response"] as? String { messageString = responseData }
            self.showSuccessWithMessage(messageString, layout: .centeredView)
            self.emailField.text = ""
            self.hero.dismissViewController()
        }
        
        var parameters = [String:String]()
        parameters["email"] = self.emailField.text
        
        APIManager.sharedInstance.forgotUserPassword(parameters: parameters, success: successClosure) { (error) in
//            self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            if let errorMessage = error.localizedFailureReason {
                self.emailField.show(error: errorMessage, placeholderText: self.emailField.placeHolderText)
            }else {
                self.showErrorWith(message: (error.localizedFailureReason ?? error.localizedDescription))
            }
            self.stopLoading()
        }
    }
}

//MARK: -

extension ForgotPasswordViewController: ValidationDelegate {
    
    func validationSuccessful() {
        
        if (CEReachabilityManager.isReachable()){
            processSignin()
        }else{
            IQKeyboardManager.sharedManager().resignFirstResponder()
            self.showErrorWith(message: "Please check your internet connection")
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        
        for (field, error) in errors {
            if let field = field as? AnimatedTextInput {
                field.show(error: error.errorMessage, placeholderText: field.placeHolderText)
            }
        }
    }
}

