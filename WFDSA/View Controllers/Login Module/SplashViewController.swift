//
//  SplashViewController.swift
//  WFDSA
//
//  Created by Ali Abdul Jabbar on 7/28/18.
//  Copyright © 2018 Ali Abdul Jabbar. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    var hideStatusBar = true
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UILabel.appearance().substituteFontName = "Roboto"; // USE YOUR FONT NAME INSTEAD
        UITextView.appearance().substituteFontName = "Roboto"; // USE YOUR FONT NAME INSTEAD
        UITextField.appearance().substituteFontName = "Roboto"; // USE YOUR FONT NAME INSTEAD
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideStatusBar = false
        UIView.animate(withDuration: 0.3) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
        changeRootViewController()
    }
    
    func changeRootViewController() {
        if !AppStateManager.sharedInstance.isUserLoggedIn() {
            let vc = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "LoginViewControllerNav_ID")
            self.hero.replaceViewController(with: vc)
        }
        else {
            let homeVC:HomeViewController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            let homeNavVC:BaseNavigationController = BaseNavigationController(rootViewController: homeVC)
            let sideMenuController:SideMenuController = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
            let slideMenuController = SlideMenuController(mainViewController: homeNavVC, leftMenuViewController: sideMenuController, rightMenuViewController: UIViewController())
            
            self.hero.replaceViewController(with: slideMenuController)
        }
    }
}
